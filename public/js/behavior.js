var subtotal = 0;
var results = new Array();
var dataPackage = 0;
var test = 0;
var pay_methode;
var personalData = new Array();
var errorData = new Array();
var g_cc = new Object();
var form;

jQuery(document).ready(function () {
    jQuery('.scrollspy').scrollSpy();
    $(".button-collapse").sideNav();
    jQuery(".counter").change(function () {
        jQuery('.container_before_cart').slideDown("slow");
    });
    $(window).scroll(function (event) {
        var scroll = $(window).scrollTop();
        //console.log(scroll);
        if (scroll > 621) {
            //$("#right_nav_package").css("display", "block");
            $(".col.hide-on-small-only.s2.m2.l2.fixed_nav_right").css("display", "block");
        }
        else if (scroll < 620) {
            //$("#right_nav_package").css("display", "none");
            $(".col.hide-on-small-only.s2.m2.l2.fixed_nav_right").css("display", "none");
        }
    });
    /* FORMULARIO QUE HACE LA PETICION A LA API. CUANDO ESE FORMULARIO HAGA SUBMIT SE EJECUTARA ESTE EVENTO. PERO ANTES DE QUE SE EJECUTE EL SUBMIT, PRIMERO SE EJECUTARA EL EVENTO CLICK DEL BOTON "CONTINUAR" QUE SE CLICKEA CUANDO SE VA A HACER EL PAGO. EL EVENTO DEL BOTON ESTA EN LA LINEA 180 */
    $('form#api-form').submit(function(event) {
        /* CON EL PREVENTDEFAULT EVITO QUE EL FORMULARIO HAGA LA FUNCION DEL SUBMIT. ESTO LO HAGO PARA QUE ANTES DE QUE SE VAYAN LOS DATOS A LA API DEBO GUARDAR LA ORDEN Y OBTENER EL ORDERID Y EL AMOUNT*/
        event.preventDefault();
        /* EN LA CONSOLA APARECERA ESTE MENSAJE INDICANDO QUE EL SUBMIT DEL FORMULARIO HA PAUSADO */
        console.log("Form Paused");
        /* GUARDO EL FORMULARIO EN UNA VARIABLE GLOBAL PARA ACCEDERLA DESDE OTRO EVENTO.*/
        form = this;
        
    });
});

function calculate(id) {
    jQuery("#" + id + " option:selected").each(function () {
        var gifts = jQuery("#" + id + " option:selected").val();
        var g = parseInt(gifts);
        var generalName = jQuery("#id_component_general_name_" + id.substr(13)).val();
        var description = jQuery("#id_component_general_descrption_" + id.substr(13)).val();
        var id_source_ = jQuery("#id_component_source_" + id.substr(13)).val();
        var v = jQuery("#id_component_value_" + id.substr(13)).val();
        var result = g * parseInt(v);
        jQuery("#id_result_" + id.substr(13)).text("Regalo de: $" + result + " ");
        jQuery("#id_component_result_" + id.substr(13)).val(result);
        var totalRow = new Object();
        totalRow = {
            name: id,
            value: result,
            general_name: generalName,
            description: description,
            gifts: g,
            id_source: id_source_
        };
        var flag = false;


        if (results.length > 0) {
            for (j = 0; j < results.length; j++) {
                if (results[j].name == id) {
                    flag = true;
                }
            }
            if (flag == true) {
                for (i = 0; i < results.length; i++) {
                    if (results[i].name == id) {
//                            alert("source: " + results[i].name + " id: " + id);
                        results[i].value = result;
                    }
                }
            } else {
                results.push(totalRow);
            }
        } else {
            results.push(totalRow);
        }
        calculateSubTotal();
    });
}
//funcion callback del arreglo de resultados
function actionOverArrayResults(value, index, ar) {
    alert(" index: " + index + " value nombre: " + value.name + " value result: " + value.value);
}
//funcion callback del arreglo que acumula los el valor de cada row
function calculateSubtotal(value, index, ar) {
//        subtotal += value.value;
}
function feedtableCart(value, index, ar) {
    jQuery("#table_cart tbody").append("<tr><td colspan='3'>" + value.general_name + "</td><td class='first_col'>" + value.gifts + "</td><td class='second_col'>" + "$" + value.value + ".00 </td></tr>");
    //jQuery(".first_col").addClass("totals_row_1");
    //jQuery(".second_col").addClass("totals_row_2");
    jQuery(".first_col").addClass("right");
    jQuery(".second_col").addClass("center");
}

//function desablePackage(value, index, ar) {
//    jQuery("#"+value.name).prop("disabled",true);
//}
function desabledPackage() {
    var n = parseInt(jQuery("#id_components").val());
    for (i = 1; i <= n; i++) {
        jQuery("#sl_component_" + i).prop("disabled", true);
    }
}
function enablePackage() {
    var n = parseInt(jQuery("#id_components").val());
    for (i = 1; i <= n; i++) {
        jQuery("#sl_component_" + i).prop("disabled", false);
    }
}
function subtotalAndTotal111() {
    jQuery("#table_cart_subtotal tbody").text("");
    jQuery("#table_cart_subtotal tbody").append("<tr><th class='right'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Subtotal</th>");
    jQuery("#table_cart_subtotal tbody").append("<td class='right'></td>");
    jQuery("#table_cart_subtotal tbody").append("<td class='value_total' id='col_subtotal'>" + subtotal + " $</td></tr>");
    jQuery("#table_cart_subtotal tbody").append("<tr><th class='right'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total</th>");
    jQuery("#table_cart_subtotal tbody").append("<td class='right'></td>");
    jQuery("#table_cart_subtotal tbody").append("<td class='value_total' id='col_subtotal'>" + subtotal + " $</td></tr>");
    //jQuery("#col_subtotal").text(subtotal + " $");
    //jQuery("#col_total").text(subtotal + " $");
    jQuery("table#table_cart_subtotal").addClass("subtotal");
    jQuery(".value_total").addClass("value_total_1");
}

function subtotalAndTotal() {

    jQuery("#col_subtotal").text( "$" + subtotal + ".00");
    jQuery("#col_total").text("$" + subtotal + ".00");
    jQuery("table#table_cart_subtotal").addClass("subtotal");
    jQuery(".value_total").addClass("value_total_1");
}

function calculateSubTotal() {
    var row = jQuery("#id_result_1").val();
    var cont = jQuery("#id_components").val();
//        results.forEach(calculateSubtotal);
    var total = 0;
    for (i = 0; i < results.length; i++) {
        total += results[i].value;
    }
    subtotal = total;

    jQuery("#spn_subtotal").text("Total: " + "$" + subtotal );
//        jQuery("#spn_subtotal").text("Total: " + results.length + " $");
    jQuery(".subtotal").css("display", "block");
}

//jQuery("#go_cart").click(function () {
//    jQuery('.container_before_cart').slideUp("fast");
//results.forEach(feedtableCart);
//subtotalAndTotal();
//});

jQuery("#go_cart").click(function () {
    jQuery('#right_nav_cart').css("display", "block");
    jQuery('#cart').css("display", "block");
    jQuery('.container_before_cart').slideUp("fast");
    //results.forEach(desablePackage);
    jQuery("#table_cart tbody").text("");
    desabledPackage();
    results.forEach(feedtableCart);
    subtotalAndTotal();
});
jQuery("#return_edit_package").click(function () {
    enablePackage();
});
jQuery("#go_make_process_pay").click(function () {
    jQuery("#checkout_cart").css("display", "block");
    jQuery("#checkout").css("display", "block");

});
var names = new Array();
jQuery("#go_make_pay").click(function () {

});
/* ESTE EVENTO SE EJECUTA UNA VEZ LE DAS CLICK AL BOTON "CONTINUAR", JUSTO ANTES DE QUE EL FORMULARIO SE "SUBMITEE" */
$(".group-go_make_pay").click(function (event) {
    console.log("Button Click");
    
    var id_boton = $(this).attr("id");
    if (id_boton == "go_make_pay") {
        if (validateDataPersonal()) {
            if (validateFieldsCreditCard()) {
                activeAndDisactiveControls();
            } else {
                showErrorsFielsCreditCard();
            }
        }
        else {
            /*mostrar errores solo de formulario de tarjeta de credito*/
            showErrorsFielsPersonalData();
        }
    } else {
        if (validateDataPersonal()) {
            activeAndDisactiveControls();
        } else {
            showErrorsFielsPersonalData();
        }
    }
    names.length = 0;
    errorData.length = 0;
});
function showErrorsFielsCreditCard() {
    for (i = 0; i < names.length; i++) {
        $time = (i + 1) * 5000;
        var $toastContent = $('<span class="malert" style="font-size: 14px; width: 100%; text-align-last: center;">El ' + names[i].name + ' es requido.</span>');
        Materialize.toast($toastContent, $time);
    }
}
function showErrorsFielsPersonalData() {
    for (i = 0; i < errorData.length; i++) {
        $time = (i + 1) * 5000;
        var $toastContent = $('<span class="malert" style="font-size: 14px; width: 100%; text-align-last: center;"> ' + errorData[i].name + '</span>');
        Materialize.toast($toastContent, $time);
    }
}
function activeAndDisactiveControls() {
    jQuery(".preloader-wrapper").addClass("active");
    setTimeout(function () {
        sendData();
    }, 1000);
}
function validateFieldsCreditCard() {
    var flag = true;
    if ($("#icon_name_credit_card").val() == "") {
        flag = false;
        label = {name: "nombre de la tarjeta"};
        names.push(label);
    }
    if ($("#icon_credit_card").val() == "") {
        flag = false;
        label = {name: "numero de tarjeta"};
        names.push(label);
    }

    jQuery("#id_sl_month option:selected").each(function () {
        var month = jQuery("#id_sl_month option:selected").val();
        if (month == "") {
            flag = false;
            label = {name: "mes de expiracion"};
            names.push(label);
        }
    });

    jQuery("#id_sl_year option:selected").each(function () {
        var year = jQuery("#id_sl_year option:selected").val();
        if (year == "") {
            flag = false;
            label = {name: "a&ntilde;o de expiracion"};
            names.push(label);
        }
    });

    if ($("#icon_csv").val() == "") {
        flag = false;
        label = {name: "codigo CVV"};
        names.push(label);
    }
    return flag;
}
function validateDataPersonal() {
    var flag = true;
    if ($("#icon_name").val() == "") {
        flag = false;
        label = {name: "Ingrese un nombre en Datos personales"};
        errorData.push(label);
    }
    if ($("#icon_lastname").val() == "") {
        flag = false;
        label = {name: "Ingrese un apellido en Datos personales"};
        errorData.push(label);
    }
    if ($("#icon_telephone").val() == "") {
        flag = false;
        label = {name: "Ingrese un telefono en Datos personales"};
        errorData.push(label);
    }
    if ($("#icon_email").val() == "") {
        flag = false;
        label = {name: "Ingrese un email en Datos personales"};
        errorData.push(label);
    }
    if ($("#icon_address").val() == "") {
        flag = false;
        label = {name: "Ingrese una direccion en Datos personales"};
        errorData.push(label);
    }
    return flag;
}

jQuery(".img_marcas").click(function () {
    //showMArcas();
});
jQuery(".img_visa").click(function () {
    showVisa();
});
jQuery(".img_secure").click(function () {
    show3dsecure();
});

jQuery(".terms").click(function () {
    PopupCenterDual("/DreamGiftsSoyServidor/public/terminos-y-condiciones", "Terminos y condiciones", '800', '700');
});


function showVisa() 
{
    window.open("vbv.html", "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, top=200, left=200, width=500, height=400");
}
function show3dsecure() 
{
    window.open("http://www.mastercard.com/us/business/en/corporate/securecode/sc_popup.html?template=/orphans&content=securecodepopup,", "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, top=200, left=200, width=500, height=400");
}

function PopupCenterDual(url, title, w, h) 
{
    // Fixes dual-screen position Most browsers Firefox
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;
    width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

    // Puts focus on the newWindow
    if (window.focus) {
    newWindow.focus();
    }
}


/*funcion que envia los datos cuando se recibio un a respuesta positiva de bac*/

function sendData() {
    /**
     * @ id_package id del paquete de donde se origunan los componentes
     * @ dataPackage hola mundo
     * @type {*|jQuery}
     */

    var id_package = $("#id_package").val();
    var data_credit_card;
    var personael_data;
    if(pay_methode == 1){
        data_credit_card = {type_methode:pay_methode,
            data:[{name:$("#icon_name_credit_card").val()},
                {num_card:$("#icon_credit_card").val()},
                {month:$("#id_sl_month").val()},
                {year:$("#id_sl_year").val()},
                {cvv:$("#icon_csv").val()}]
        };

        g_cc = data_credit_card.data;
        var cc_number = g_cc[1].num_card
        var last4 = cc_number.substring(cc_number.length-4);
        //console.log(last4);
    }else{
        data_credit_card = {type_methode:pay_methode,
            data:false
        };
    }
    personael_data = {
        name:$("#icon_name").val(),
            lastname:$("#icon_lastname").val(),
            telephone:$("#icon_telephone").val(),
            email:$("#icon_email").val(),
            address:$("#icon_address").val()
    };
    dataPackage = {data: results, id_package: id_package, pay:data_credit_card, general_total:subtotal, data_personal:personael_data, _token: $('meta[name=_token]').attr('content')};

    $.ajax({
        url: '../process-data',
        type: 'POST',
        data: dataPackage,
        success: function (data) {
            //console.log(data);
            if(data != ""){
                var json = JSON.parse(data);
                console.log(json)
                /* CUANDO OBTENGO LOS DATOS QUE ME DEVUELVE EL SERVIDOR, PROCEDO A AGREGAR DINAMICAMENTE LOS DATOS AL FORMULARIO QUE POR EL MOMENTO ESTA PAUSADO. */

                /* AGREGAR ORDERID*/
                $(form).append("<input type='hidden' name='orderid' value='" + json.order + "'>");
                /* AGREGAR AMOUNT */
                $(form).append("<input type='hidden' name='amount' value='" + json.amount + "'>");
                /* AGREGAR HASH */
                $(form).append("<input type='hidden' name='hash' value='" + json.hash + "'>");
                /* AGREGAR TIME */
                $(form).append("<input type='hidden' name='time' value='" + json.time + "'>");
                /* AGREGAR KEY ID */
                $(form).append("<input type='hidden' name='key_id' value='" + json.key_id + "'>");
                /* AGREGAR CC EXP */
                $(form).append("<input type='hidden' name='ccexp' value='" + g_cc[2].month + g_cc[3].year + "'>");


                console.log("AJAX FINISHED");

                /* CUANDO TERMINÉ DE AGREGAR LOS DATOS QUE HACIAN FALTA PARA ENVIAR LOS DATOS A LA API DE CREDOMATIC, EL FORMULARIO ESTARA LISTO PARA SER SUBMITEADO. Y LISTO (FACIL Y RAPIDO Y SIN DOLOR ) */
                form.submit();
                /**/
                //console.log(json);
                /*test = {type: 'auth', hash: json.hash, key_id: json.key_id, time: json.time, amount: json.amount, orderid:json.order,  redirect: 'http://166.63.35.26/DreamGiftsSoyServidor/public/callback', HashDigest: json.hash, last4: last4, ccnumber: g_cc[1].num_card, cvv: g_cc[4].cvv, CardName: g_cc[0].name, ccexp: g_cc[2].month + g_cc[3].year, _token: $('meta[name=_token]').attr('content')};
                $.ajax({
                    url: 'https://credomatic.compassmerchantsolutions.com/api/transact.php',
                    type: 'POST',
                    data: test,
                    success: function (request){
                        console.log(request);
                    }
                })*/
                
                jQuery("#success_confirmed").css("display", "block");
                jQuery("#success").css("display", "block");
                jQuery(".preloader-wrapper").removeClass("active");
                jQuery("#checkout_cart").removeClass("active");
                //jQuery("#success_confirmed").addClass("active");
                //jQuery("#return_edit_package").addClass("disabled");
                //jQuery("#go_make_process_pay").addClass("disabled");
                jQuery("#return_edit_package").css("display", "none");
                jQuery("#go_make_process_pay").css("display", "none");
                jQuery("#go_make_pay").css("display", "none");
                jQuery("#message_confirmed").text("Gracias por tu regalo!");
                jQuery("#message_confirmed_1").text("Tu regalo es de " + subtotal + ".00 $");

                $("#success_confirmed").trigger("click");
            }else{
                jQuery("#success_confirmed").css("display", "block");
                jQuery("#success").css("display", "block");
                jQuery(".preloader-wrapper").removeClass("active");
                jQuery("#checkout_cart").removeClass("active");
                //jQuery("#success_confirmed").addClass("active");
                //jQuery("#return_edit_package").addClass("disabled");
                //jQuery("#go_make_process_pay").addClass("disabled");
                jQuery("#return_edit_package").css("display", "none");
                jQuery("#go_make_process_pay").css("display", "none");
                jQuery("#go_make_pay").css("display", "none");
                jQuery("#message_confirmed").text("Lo sentimos, el proceso fallo!");
                jQuery("#message_confirmed_1").text("intenta recargar tu pagina y repetir el proceso... ");
                $("#success_confirmed").trigger("click");
            }
        }
    });
}
/*mascaras de el contenedor de tarjetas de credito, usando plugin de mascaras*/
jQuery("#icon_csv").mask("999");
/* fin mascaras de el contenedor de tarjetas de credito*/


$("#mp-credit-card").click(function () {
    pay_methode = 1;
    $("#pay-agency-pay").css("display", "none");
    $("#pay-bank-transfer").css("display", "none");
    $("#pay-credit-card").css("display", "block");

});

$("#mp-agency-pay").click(function () {
    pay_methode = 3;
    $("#pay-bank-transfer").css("display", "none");
    $("#pay-credit-card").css("display", "none");
    $("#pay-agency-pay").css("display", "block");

});

$("#mp-transfer-bank").click(function () {
    pay_methode = 2;
    $("#pay-credit-card").css("display", "none");
    $("#pay-agency-pay").css("display", "none");
    $("#pay-bank-transfer").css("display", "block");

});

$(".group-form-buyer").click(function () {
    $(".data-personal-buyer").css("display", "none");
    alert("programar validacion de campos vacios en formulario de datos personales");
});
/*fin pay methode*/