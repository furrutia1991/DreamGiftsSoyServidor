-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 22-01-2016 a las 09:34:33
-- Versión del servidor: 5.5.46-0ubuntu0.14.04.2
-- Versión de PHP: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `dreamgifts`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `buyer`
--

CREATE TABLE IF NOT EXISTS `buyer` (
  `buyer_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`buyer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=56 ;

--
-- Volcado de datos para la tabla `buyer`
--

INSERT INTO `buyer` (`buyer_id`, `email`, `name`, `phone_number`) VALUES
(1, 'carlos@soyservidor.com', 'Carlos Martinez', '98991212'),
(2, 'jose@soyservidor.com', 'Jose Hernandez', '97991514'),
(3, 'eduardo@soyservidor.com', 'Eduardo Cabos', '95891518'),
(4, 'hugo@soyservidor.com', 'Hugo Ramirez', '33891517'),
(5, 'sofia@soyservidor.com', 'Sofia Suazo', '93891913'),
(6, 'elizabeth@soyservidor.com', 'Elizabeth Avila', '32891119'),
(7, 'maría@soyservidor.com', 'María Torres', '98891216'),
(8, 'nahun@soyservidor.com', 'Nahun Sanchez', '33891314'),
(9, 'caleb@soyservidor.com', 'Caleb Rodriguez', '33891415'),
(10, 'desarrollojr@soyservidor.com', 'Francisco Urrutia', '31921974'),
(12, 'jacobo@soyservidor.com', 'Jacovo vasquez', '99894523'),
(13, 'm_orellana_123@hotmail.com', 'Mariela Orellana', '94782315'),
(14, 'pablo@soyservidor.com', 'Pablo', '0005555000'),
(15, 'pablo@gmail.com', 'Pablo ', '555444777'),
(16, 'jvasquez8704@gmail.com', 'JAcobo', '111222333666'),
(17, 'jvasquez@yahoo.es', 'Hugo  Boss', '5465432132'),
(18, 'navas@yahoo.com', 'Beto Navas', '32146546547985'),
(19, 'er', 'er er', 'rer'),
(20, 'francisco.urrutia.1991@gmail.com', 'Francisco Urrutia', '31921974'),
(21, 'jacobo@soyservidor.com', 'Jacobo  Vasquez', '22242337'),
(22, 'abner.zuniga@gmail.com', 'Abner Vasquez', '99955665'),
(23, 'erick.canelas@gmail.com', 'Erick Canelas', '222455896'),
(24, 'fr@gmail.com', 'Francisco Rovelo', '414651651'),
(25, 'jmartinez@gmail.com', 'Javier Martinez', '22256654'),
(26, 'bianca.rosales@yahoo.es', 'Bianca Rosales', '456874321'),
(27, 'galtamirano@yahoo.com', 'Gerson  Altamirano', '87954321'),
(28, 'iSanchez@gmail.com', 'Irma Sanchez', '5487812316'),
(29, 'rmaradiaga@gmail.com', 'Roberto Maradiaga', '445545'),
(30, 'aizaguirre@gmail.com', 'Alberto Izaguirre', '57454654654'),
(31, 'bgonzalez', 'Bersy  Gonzalez', '8479846541312'),
(32, 'jineztroza@gmail.com', 'Juan  Inestroza', '876544654'),
(33, 'asd', 'asd asd', 'asd'),
(34, 'asd', 'prueba prueba', 'asd'),
(35, 'asd', 'asd asd', 'asd'),
(36, '574as', 'cena romantica', '54'),
(37, 'asd', 'asd asd', 'asd'),
(38, 'm_orellana_123@hotmail.com', 'Mariela  Orellana', '94782315'),
(39, 'desarrollojr@soyservidor.com', 'Francisco  Urrutia', '31921974'),
(40, 'francisco.urrutia.1991@soyservidor.com', 'FRANCISCO  URRUTIA', '31921974'),
(41, 'francisco@lynext.com', 'Francisco Urrutia', '31921974'),
(42, 'desarrollojr@soyservidor.com', 'francisco  urrutia', '31921974'),
(43, 'francisco.urrutia.1991@gmail.com', 'Francisco  Urrutia', '31921974'),
(44, 'francisco.urrutia.1991@gmail.com', 'francisco  urrutia', '31921974'),
(45, '|aasd', 'asd asd', 'ad'),
(46, 'desarrollojr@soyservidor.com', 'pablo marmol', '31921974'),
(47, 'francisco@lynext.com', 'Jacobo Vasquez', '31921974'),
(48, 'desarrollojr@soyservidor.com', 'asd asd', 'asd'),
(49, 'desarrollojr@soyservidor.com', 'Cesar Torres', '31921974'),
(50, 'desarrollojr@soyservidor.com', 'cesar torres', '31921974'),
(51, 'desarrollojr@soyservidor.com', 'pedro torres', '31921974'),
(52, 'asdasd', 'asdasd asdasd', 'asdasd'),
(53, 'jacobo@soyservidor.com', 'Jacobo  zuniga', '22242337'),
(54, 'francisco.urrutia.1991@gmail.com', 'hh g', '31921941'),
(55, 'francisco.urrutia.1991@gmail.com', 'Mariela Orellana', '94782315');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `city`
--

CREATE TABLE IF NOT EXISTS `city` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`city_id`),
  KEY `country_id` (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Volcado de datos para la tabla `city`
--

INSERT INTO `city` (`city_id`, `name`, `country_id`) VALUES
(1, 'Miami', 7),
(2, 'New York', 7),
(3, 'Los Angeles', 7),
(4, 'Washinton D.C.', 7),
(5, 'San Francisco', 7),
(6, 'Chicago', 7),
(7, 'Las Vegas', 7),
(8, 'Cancun', 8),
(9, 'Cabo San Lucas', 8),
(10, 'Mexido D.F.', 8),
(11, 'Playa del Carmen', 8),
(12, 'Punta Cana', 1),
(13, 'Santiago de los Caballeros', 1),
(14, 'Santo Domingo', 1),
(15, 'Santiago de Chile', 4),
(16, 'Buenos Aires', 5),
(17, 'Rosario', 5),
(18, 'Ciudad de Córdoba', 5),
(19, 'Ciudad de Mendoza', 5),
(20, 'La Habana', 2),
(21, 'Cayo Santa María', 2),
(22, 'Rotan', 6),
(23, 'Utila', 6),
(24, 'Roatan', 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `component`
--

CREATE TABLE IF NOT EXISTS `component` (
  `component_id` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` int(11) NOT NULL,
  `tittle` varchar(50) DEFAULT NULL,
  `picture` varchar(200) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `date_created` varchar(140) DEFAULT NULL,
  `date_updated` varchar(140) DEFAULT NULL,
  `total_amount` int(11) NOT NULL,
  `amount` float DEFAULT NULL,
  `balance` float DEFAULT NULL,
  `num_parts` int(11) DEFAULT NULL,
  `num_parts_balance` int(11) DEFAULT NULL,
  PRIMARY KEY (`component_id`),
  KEY `fk_package` (`package_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Volcado de datos para la tabla `component`
--

INSERT INTO `component` (`component_id`, `package_id`, `tittle`, `picture`, `description`, `date_created`, `date_updated`, `total_amount`, `amount`, `balance`, `num_parts`, `num_parts_balance`) VALUES
(1, 2, 'Hotel', '/DreamGiftsSoyServidor/public/images/components/hotel5.jpg', 'Hotel Intercontinental, Cuenta con piscina, Sauna, y masaje', '2015-12-15', '2015-12-15', 250, 50, 0, 5, 5),
(2, 2, 'Kayak', '/DreamGiftsSoyServidor/public/images/components/kayak.jpg', 'Tour en kayak ', '2015-12-15 00:00:00', '0000-00-00 00:00:00', 200, 40, -40, 5, 5),
(3, 2, 'Cena', '/DreamGiftsSoyServidor/public/images/components/diner.jpg', 'Cena Romantica', '2015-12-15 00:00:00', '0000-00-00 00:00:00', 50, 10, -10, 5, 5),
(4, 2, 'Masaje', '/DreamGiftsSoyServidor/public/images/components/masaje2.jpg', 'Sauna, y masaje para dos', '2015-12-15 00:00:00', NULL, 200, 25, 150, 8, 8),
(5, 2, 'Vino', '/DreamGiftsSoyServidor/public/images/components/pack5.jpg', 'Vino fino frances', '2015-12-15 00:00:00', '0000-00-00 00:00:00', 50, 50, 50, 1, 1),
(6, 2, 'Tour', '/DreamGiftsSoyServidor/public/images/components/hotel6.jpg', 'Tour a la isla caimán', '2015-12-15 00:00:00', '0000-00-00 00:00:00', 250, 25, 200, 10, 10),
(7, 3, 'Yate', '/DreamGiftsSoyServidor/public/images/components/crucero6.jpg', 'Paseo en yate por un dia', '2015-12-15 00:00:00', '0000-00-00 00:00:00', 1000, 100, 0, 10, 0),
(8, 3, 'Crucero', '/DreamGiftsSoyServidor/public/images/components/crucero1.jpg', 'Crucero para dos a las bahamas', '2015-12-15 00:00:00', '2016-01-20 13:27:59 ', 3000, 100, 2000, 30, 28),
(9, 3, 'Viaje en globo', '/DreamGiftsSoyServidor/public/images/components/aeroestatico3.jpg', 'Tour por la ciudad de Roma en globo', '2015-12-15 00:00:00', '2016-01-20 13:26:51 ', 500, 50, 350, 10, 9),
(10, 3, 'Canopy', '/DreamGiftsSoyServidor/public/images/components/canopy2.jpg', 'Cinco horas de máxima adrenalina en el mejor canopy del mundo', '2015-12-15 00:00:00', '0000-00-00 00:00:00', 275, 55, 0, 5, 0),
(11, 3, 'Bongie', '/DreamGiftsSoyServidor/public/images/components/bongie.jpg', 'Salto en el bongie mas alto de la ciudad', '2015-12-15 00:00:00', '2016-01-20 10:34:27 ', 175, 35, 0, 5, 3),
(12, 3, 'Paracaidas', '/DreamGiftsSoyServidor/public/images/components/paracaidismo.jpg', 'Tour sobre la ciudad y lanzamiento de paracaidas', '2015-12-15 00:00:00', '2016-01-20 13:24:05 ', 750, 50, 0, 15, 3),
(13, 4, 'Cabaña Privada', '/DreamGiftsSoyServidor/public/images/components/hotel1.jpg', 'Cabaña todo incluido, decoración romantica', '2015-12-15 00:00:00', '0000-00-00 00:00:00', 1250, 125, 875, 10, 10),
(14, 4, 'Buceo', '/DreamGiftsSoyServidor/public/images/components/pack1.jpg', 'Tour acuatico por el arrecife de coral de punta cana', '2015-12-15 00:00:00', '0000-00-00 00:00:00', 210, 10, 170, 21, 10),
(15, 4, 'Almuerzo Especial', '/DreamGiftsSoyServidor/public/images/components/diner1.jpg', 'Comida y bebida ilimitada', '2015-12-15 00:00:00', '2016-01-21 16:32:31 ', 250, 25, 175, 5, 2),
(16, 4, 'Paseo en Camello', '/DreamGiftsSoyServidor/public/images/components/camel.jpg', 'Paseo en camello por la ciudad de siria', '2015-12-15 00:00:00', '0000-00-00 00:00:00', 75, 15, 75, 5, 5),
(17, 4, 'Acuario', '/DreamGiftsSoyServidor/public/images/components/acuario.jpg', 'Visita al acuario de la ciudad', '2015-12-15 00:00:00', '2016-01-21 10:48:01 ', 235, 47, 94, 5, 3),
(18, 4, 'Renta de vehiculo', '/DreamGiftsSoyServidor/public/images/components/car.jpg', 'Alquiler de vehiculo por 6 dias', '2015-12-15 00:00:00', '0000-00-00 00:00:00', 15, 50, 650, 15, 15),
(19, 0, 'COMPONENTE DE PRUEBA', NULL, 'ESTE ES UN COMPONENTE DE PRUEBA', '23 December, 2015', '23 December, 2015', 10, 50, 500, 10, 10),
(20, 0, 'componente A', NULL, 'componente de prueba', '23 December, 2015', '23 December, 2015', 55, 11, 55, 5, 5),
(21, 0, 'componente B', NULL, 'componente de prueba', '23 December, 2015', '23 December, 2015', 55, 11, 55, 5, 5),
(22, 0, 'componente C', NULL, 'a', '23 December, 2015', '23 December, 2015', 1000, 100, 1000, 10, 1),
(23, 0, 'componente D', NULL, 'a', '23 December, 2015', '23 December, 2015', 500, 50, 500, 10, 1),
(24, 0, 'componente E', NULL, 'a', '', '', 300, 30, 300, 10, 1),
(25, 0, 'componente F', NULL, 'a', '', '', 600, 60, 600, 10, 1),
(26, 0, 'componente G', NULL, 'a', '23 December, 2015', '23 December, 2015', 0, 2, 2, 2, 2),
(27, 1, 'componente H', '/DreamGiftsSoyServidor/public/images/components/viaje1.jpg', 'a', '23 December, 2015', NULL, 0, 2, 0, 2, 2),
(28, 1, 'componente I', '/DreamGiftsSoyServidor/public/images/components/hotel4.jpg', 'b', '23 December, 2015', NULL, 0, 30, 0, 10, 5),
(29, 1, 'componente J', '/DreamGiftsSoyServidor/public/images/components/pack6.jpg', 'este es el componente 1', '23 December, 2015', '2016-01-20 09:57:14 ', 0, 20, -760, 40, 1),
(30, 1, 'Luna de miel a cancun', '/DreamGiftsSoyServidor/public/images/components/pack7.jpg', 'VIAJE TODO INCLUIDO A CANCUN', '23 December, 2015', '2016-01-21 16:38:40 ', 0, 60, 0, 100, 0),
(31, 1, 'Cena romantica', '/DreamGiftsSoyServidor/public/images/components/diner3.jpg', 'CENA Y NOCHE ROMANTICA EN EL HOTEL MARRIOT', '23 December, 2015', '2016-01-15 16:51:32 ', 0, 20, -80, 25, 6),
(32, 1, 'Pasajes aereos', '/DreamGiftsSoyServidor/public/images/components/viaje2.jpg', 'Pasajes Aereos', '23 December, 2015', '2016-01-20 15:36:28 ', 0, 50, 250, 10, 9),
(33, 5, 'Hotel salinitas', '/DreamGiftsSoyServidor/public/images/components/hotel1.jpg', 'playa del carmen', '23 December, 2015', NULL, 0, 80, 800, 8, 8),
(34, 0, 'Luna de miel en Hawai', NULL, '111ASD56A4SD65A4SD654', '3 January, 2016', '22 January, 2016', 0, 122, 12200, 100, 100),
(35, 2, 'Paseo turistico', NULL, 'Paseo por la costa del sol', '5 January, 2016', '2016-01-20 10:13:27 ', 0, 50, 350, 10, 0),
(36, 7, 'Componente K', '/DreamGiftsSoyServidor/public/images/components/pack8.jpg', 'descripcion del componente', '5 January, 2016', NULL, 0, 50, 500, 10, 10),
(37, 7, 're', '/DreamGiftsSoyServidor/public/images/components/car.jpg', 'd', '8 January, 2016', '8 January, 2016', 0, 1, 1, 1, 1),
(39, 6, 'Luna de miel a Miami', '/DreamGiftsSoyServidor/public/images/components/hotel6.jpg', 'Paquete todo incluido a miami 7 dias y 6 noches', '8 January, 2016', '2016-01-20 11:59:15 ', 5000, 50, 2100, 100, 46);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`country_id`),
  KEY `country_id` (`country_id`),
  KEY `country_id_2` (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Volcado de datos para la tabla `country`
--

INSERT INTO `country` (`country_id`, `name`) VALUES
(1, 'Republica Dominicana'),
(2, 'Cuba'),
(3, 'Canada'),
(4, 'Chile'),
(5, 'Argentina'),
(6, 'Honduras'),
(7, 'Estados Unidos'),
(8, 'Mexico'),
(9, 'Perú'),
(10, 'Colombia'),
(11, 'Venezuela'),
(12, 'Ecuador'),
(13, 'España'),
(14, 'Italia'),
(15, 'Alemania'),
(16, 'Inglaterra');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(140) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `dni_number` varchar(25) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Volcado de datos para la tabla `customer`
--

INSERT INTO `customer` (`customer_id`, `description`, `email`, `name`, `dni_number`, `address`, `phone_number`) VALUES
(1, '#', 'cliente1@soyservidor.com', 'Cardona Orellana', '080119911234', 'Col.cliente1 casa N°1231', '31989850'),
(2, '#', 'cliente2@soyservidor.com', 'Gonzales Gutierrez', '0801-1991-12342', 'Col.cliente2 casa N°1232', '31989852'),
(3, '#', 'cliente3@soyservidor.com', 'Peres Lopez', '0801-1991-12343', 'Col.cliente3 casa N°1233', '31989853'),
(4, '#', 'cliente3@soyservidor.com', 'Santos Hernandez', '0801-1991-12344', 'Col.cliente3 casa N°1234', '31989854'),
(5, '#', 'cliente4@soyservidor.com', 'Kafie Nuñez', '0801-1991-12345', 'Col.cliente4 casa N°1235', '31989855'),
(6, '#', 'cliente5@soyservidor.com', 'Funez Ordoñez', '0801-1991-12346', 'Col.cliente5 casa N°1236', '31989856'),
(7, '#', 'cliente6@soyservidor.com', 'Amaya Avila', '0801-1991-12347', 'Col.cliente6 casa N°1237', '31989857'),
(11, NULL, 'jacobo@soyservidor.com', 'Jacobo Vasquez', '080119911234', 'Col. Cerro Grande, Honduras', '31989850'),
(12, NULL, 'francisco.urrutia.1991@gmail.com', 'Urrutia Orellana', '080119910826', 'Tegucigalpa Honduras', '31921974');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `destination`
--

CREATE TABLE IF NOT EXISTS `destination` (
  `destination_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `postal_code` varchar(50) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `latitude` varchar(40) DEFAULT NULL,
  `longitude` varchar(40) DEFAULT NULL,
  `phone` varchar(140) NOT NULL,
  `city_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`destination_id`),
  KEY `city_id` (`city_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `destination`
--

INSERT INTO `destination` (`destination_id`, `description`, `postal_code`, `name`, `latitude`, `longitude`, `phone`, `city_id`) VALUES
(1, 'Hotel ubicado en jardines tropicales, con acceso directo a la reconocida playa de West Bay y a solo unos pasos de las aguas turquesas y del mundialmente famoso arrecife de coral. Es un estupendo destino familiar. Con tres restaurantes, 2 piscinas, Centro', '504', 'Paradise Resort', '16.2739', '-86.5905', '2225-2522', 24),
(2, 'Pocos hoteles de lujo de Miami ofrecen tanto estilo y esplendor. Con 641 habitaciones recién redecoradas, ofrece sofisticación y servicio de primera calidad a solo 12 kilómetros del aeropuerto de Miami.', '11542', 'Intercontinetal Hotel', '25.259', '-45.5905', '8879-5269', 1),
(3, 'ESTE ES UNA PRUEBA', '123', 'DESTINO DE PRUEBA', NULL, NULL, '0000-0000', 1),
(7, 'ESTE ES UNA PRUEBA', '132', 'DESTINO DE PRUEBA 2', NULL, NULL, '8875-1203', 15);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_reference` int(11) DEFAULT NULL,
  `name_table` varchar(20) DEFAULT NULL,
  `action` varchar(70) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `place` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notification`
--

CREATE TABLE IF NOT EXISTS `notification` (
  `notification_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_reference` int(11) DEFAULT NULL,
  `name_table` varchar(20) DEFAULT NULL,
  `message` varchar(70) DEFAULT NULL,
  `observation` varchar(70) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `place` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`notification_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `date_created` varchar(140) NOT NULL,
  `status` enum('processing','pending','completed','canceled') NOT NULL,
  `total` int(11) NOT NULL,
  `buyer_id` int(11) NOT NULL,
  `package_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`order_id`),
  KEY `fk_order_buyer` (`buyer_id`),
  KEY `fk_package` (`package_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=45 ;

--
-- Volcado de datos para la tabla `orders`
--

INSERT INTO `orders` (`order_id`, `date_created`, `status`, `total`, `buyer_id`, `package_id`) VALUES
(1, '', 'processing', 0, 1, NULL),
(2, '', 'processing', 0, 1, NULL),
(3, '2016-01-13 11:26:29 ', 'processing', 560, 15, NULL),
(4, '2016-01-13 11:53:40 ', 'processing', 440, 16, 4),
(5, '2016-01-13 11:58:55 ', 'processing', 540, 17, 1),
(6, '2016-01-13 12:16:51 ', 'processing', 640, 18, 1),
(7, '2016-01-13 18:21:30 ', 'processing', 20, 19, 1),
(8, '2016-01-13 20:23:20 ', 'processing', 100, 20, 2),
(9, '2016-01-14 10:34:47 ', 'processing', 1400, 21, 3),
(10, '2016-01-14 11:11:24 ', 'processing', 1100, 22, 3),
(11, '2016-01-14 12:40:10 ', 'processing', 700, 23, 3),
(12, '2016-01-14 12:45:44 ', 'processing', 275, 24, 3),
(13, '2016-01-14 15:00:03 ', 'processing', 205, 25, 3),
(14, '2016-01-14 16:12:56 ', 'processing', 160, 26, 2),
(15, '2016-01-14 16:15:46 ', 'processing', 230, 27, 2),
(16, '2016-01-14 16:36:38 ', 'processing', 160, 28, 2),
(17, '2016-01-14 16:44:35 ', 'processing', 100, 29, 2),
(18, '2016-01-14 17:07:47 ', 'processing', 442, 30, 1),
(19, '2016-01-14 17:09:59 ', 'processing', 165, 31, 4),
(20, '2016-01-14 17:13:07 ', 'processing', 397, 32, 4),
(21, '2016-01-15 16:40:59 ', 'processing', 300, 33, 1),
(22, '2016-01-15 16:42:07 ', 'processing', 400, 34, 1),
(23, '2016-01-15 16:43:30 ', 'processing', 780, 35, 1),
(24, '2016-01-15 16:51:32 ', 'processing', 180, 36, 1),
(25, '2016-01-20 09:57:14 ', 'processing', 6720, 37, 1),
(26, '2016-01-20 10:00:40 ', 'processing', 50, 13, 2),
(27, '2016-01-20 10:09:30 ', 'processing', 50, 38, 2),
(28, '2016-01-20 10:13:27 ', 'processing', 50, 39, 2),
(29, '2016-01-20 10:32:46 ', 'processing', 35, 40, 3),
(30, '2016-01-20 10:34:27 ', 'processing', 35, 41, 3),
(31, '2016-01-20 10:37:58 ', 'processing', 450, 42, 3),
(32, '2016-01-20 10:41:00 ', 'processing', 100, 43, 3),
(33, '2016-01-20 10:44:20 ', 'processing', 5000, 44, 6),
(34, '2016-01-20 10:53:54 ', 'processing', 2500, 45, 6),
(35, '2016-01-20 10:58:47 ', 'processing', 50, 46, 6),
(36, '2016-01-20 11:22:14 ', 'pending', 250, 47, 6),
(37, '2016-01-20 11:59:14 ', 'pending', 100, 48, 6),
(38, '2016-01-20 13:24:05 ', 'pending', 200, 49, 3),
(39, '2016-01-20 13:26:51 ', 'pending', 50, 50, 3),
(40, '2016-01-20 13:27:59 ', 'pending', 200, 51, 3),
(41, '2016-01-20 15:36:28 ', 'pending', 50, 52, 1),
(42, '2016-01-21 10:48:01 ', 'pending', 119, 53, 4),
(43, '2016-01-21 16:32:30 ', 'pending', 50, 54, 4),
(44, '2016-01-21 16:38:40 ', 'pending', 60, 55, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `package`
--

CREATE TABLE IF NOT EXISTS `package` (
  `package_id` int(11) NOT NULL AUTO_INCREMENT,
  `picture` varchar(200) DEFAULT NULL,
  `message` varchar(200) DEFAULT NULL,
  `date_created` varchar(140) DEFAULT NULL,
  `date_finalized` varchar(140) DEFAULT NULL,
  `date_event` varchar(140) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `destination_id` int(11) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  PRIMARY KEY (`package_id`),
  KEY `customer_id` (`customer_id`),
  KEY `destination_id` (`destination_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `package`
--

INSERT INTO `package` (`package_id`, `picture`, `message`, `date_created`, `date_finalized`, `date_event`, `customer_id`, `destination_id`, `amount`) VALUES
(1, '/DreamGiftsSoyServidor/public/images/wedding3.jpg', 'Gracias por ayudarnos a pagar nuestra luna de miel. ', '25 December, 2015', '25 December, 2015', '14 December, 2015', 3, 2, 5550),
(2, '/DreamGiftsSoyServidor/public/images/res_1432419069471.jpg', 'Gracias por ayudarnos a pagar nuestra luna de miel. Es un honor para nosotros compartir esta alegría con tigo.', '25 December, 2015', '24 December, 2015', '26 December, 2015', 1, 1, 7000),
(3, '/DreamGiftsSoyServidor/public/images/wedding2.jpg', 'Gracias por acompañarnos en nuestra boda, Si quieres hacernos un regalo agradecemos que aportes a pagar nuestra luna de miel', '25 December, 2015', '25 December, 2015', '25 December, 2015', 2, 2, 3600),
(4, '/DreamGiftsSoyServidor/public/images/wedding3.jpg', 'Gracias por tu regalo para nuestra luna de miel', '25 December, 2015', '25 December, 2015', '25 December, 2015', 5, 2, 5200),
(5, '/DreamGiftsSoyServidor/public/images/wedding3.jpg', 'asdasdasdasdasdas', '16 December, 2015', '25 December, 2015', '25 December, 2015', 11, 1, 5000),
(6, '/DreamGiftsSoyServidor/public/images/phpDwLfWt.jpg', 'PAQUETE CREADO PARA BODA URRUTIA', '4 January, 2016', '29 June, 2016', '30 June, 2016', 12, 2, 9000),
(7, '/DreamGiftsSoyServidor/public/images/wedding4.jpg', 'Nuestra boda', '5 January, 2016', '22 March, 2016', '23 March, 2016', 7, 3, 9000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `package_component`
--

CREATE TABLE IF NOT EXISTS `package_component` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment` varchar(200) DEFAULT NULL,
  `status` enum('available','disabled') DEFAULT 'available',
  `package_id` int(11) DEFAULT NULL,
  `component_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `package_id` (`package_id`),
  KEY `component_id` (`component_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Volcado de datos para la tabla `package_component`
--

INSERT INTO `package_component` (`id`, `comment`, `status`, `package_id`, `component_id`) VALUES
(1, 'comentario del administrador', 'available', 2, 1),
(2, 'comentario del administrador', 'available', 2, 2),
(3, 'comentario del administrador', 'available', 2, 3),
(4, 'comentario del administrador', 'available', 2, 4),
(5, 'comentario del administrador', 'available', 2, 5),
(6, 'comentario del administrador', 'available', 2, 6),
(7, 'comentario del administrador', 'available', 3, 7),
(8, 'comentario del administrador', 'available', 3, 8),
(9, 'comentario del administrador', 'available', 3, 9),
(10, 'comentario del administrador', 'available', 3, 10),
(11, 'comentario del administrador', 'available', 3, 11),
(12, 'comentario del administrador', 'available', 3, 12),
(13, 'comentario del administrador', 'available', 4, 13),
(14, 'comentario del administrador', 'available', 4, 14),
(15, 'comentario del administrador', 'available', 4, 15),
(16, 'comentario del administrador', 'available', 4, 16),
(17, 'comentario del administrador', 'available', 4, 17),
(18, 'comentario del administrador', 'available', 4, 18),
(19, NULL, 'available', 1, 27),
(20, NULL, 'available', 1, 28),
(21, NULL, 'available', 1, 29),
(22, NULL, 'available', 1, 30),
(23, NULL, 'available', 1, 31),
(24, NULL, 'available', 1, 32),
(25, NULL, 'available', 5, 33),
(26, NULL, 'available', 2, 35),
(27, NULL, 'available', 7, 36),
(28, NULL, 'available', 7, 37),
(30, NULL, 'available', 6, 39);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payment`
--

CREATE TABLE IF NOT EXISTS `payment` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_method_id` int(11) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `component_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`payment_id`),
  KEY `fk_payment_paymentmethod` (`payment_method_id`),
  KEY `component_id` (`component_id`),
  KEY `payment_id` (`payment_id`),
  KEY `fk_order` (`order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=61 ;

--
-- Volcado de datos para la tabla `payment`
--

INSERT INTO `payment` (`payment_id`, `payment_method_id`, `description`, `date_created`, `component_id`, `order_id`) VALUES
(1, 1, 'credict card', '2015-12-16 00:00:00', 1, 1),
(2, 1, 'credict card', '2015-12-17 00:00:00', 3, 1),
(3, 2, 'credict card', '2015-12-18 00:00:00', 3, 1),
(4, 2, 'credict card', '2015-12-16 00:00:00', 1, 1),
(5, 2, 'credict card', '2015-12-17 00:00:00', 1, 1),
(6, 2, 'credict card', '2015-12-18 00:00:00', 9, 1),
(7, 2, 'credict card', '2015-12-19 00:00:00', 6, 1),
(8, 2, 'credict card', '2015-12-16 00:00:00', 13, 1),
(9, 2, 'credict card', '2015-12-21 00:00:00', 4, 1),
(10, 1, 'pago', '2016-01-11 00:00:00', 1, 1),
(11, 1, 'pago de regalo de Cardona Orellana', '2016-01-12 00:00:00', 1, 1),
(12, 1, NULL, NULL, 27, 1),
(13, 2, NULL, NULL, 28, 1),
(15, 1, NULL, NULL, 33, 1),
(16, 1, NULL, NULL, 33, 1),
(17, 1, NULL, NULL, 36, 1),
(18, 1, NULL, NULL, 37, 1),
(19, 2, 'por los momentos ninguna...', '2016-01-13 11:26:29', 1, 3),
(20, 3, 'por los momentos ninguna...', '2016-01-13 11:53:40', 1, 4),
(21, 2, 'por los momentos ninguna...', '2016-01-13 11:58:55', 1, 5),
(22, 2, 'por los momentos ninguna...', '2016-01-13 12:16:51', 1, 6),
(23, 2, 'por los momentos ninguna...', '2016-01-13 18:21:30', 1, 7),
(24, 1, 'por los momentos ninguna...', '2016-01-13 20:23:20', 1, 8),
(25, 3, 'por los momentos ninguna...', '2016-01-14 10:34:47', 1, 9),
(26, 3, 'por los momentos ninguna...', '2016-01-14 11:11:24', 1, 10),
(27, 2, 'por los momentos ninguna...', '2016-01-14 12:40:10', 1, 11),
(28, 3, 'por los momentos ninguna...', '2016-01-14 12:45:44', 1, 12),
(29, 2, 'por los momentos ninguna...', '2016-01-14 15:00:03', 1, 13),
(30, 3, 'por los momentos ninguna...', '2016-01-14 16:12:56', 1, 14),
(31, 3, 'por los momentos ninguna...', '2016-01-14 16:15:46', 1, 15),
(32, 3, 'por los momentos ninguna...', '2016-01-14 16:36:38', 1, 16),
(33, 2, 'por los momentos ninguna...', '2016-01-14 16:44:35', 1, 17),
(34, 3, 'por los momentos ninguna...', '2016-01-14 17:07:47', 1, 18),
(35, 3, 'por los momentos ninguna...', '2016-01-14 17:09:59', 1, 19),
(36, 2, 'por los momentos ninguna...', '2016-01-14 17:13:07', 1, 20),
(37, 3, 'por los momentos ninguna...', '2016-01-15 16:40:59', 1, 21),
(38, 2, 'por los momentos ninguna...', '2016-01-15 16:42:07', 1, 22),
(39, 1, 'por los momentos ninguna...', '2016-01-15 16:43:30', 1, 23),
(40, 3, 'por los momentos ninguna...', '2016-01-15 16:51:32', 1, 24),
(41, 1, 'por los momentos ninguna...', '2016-01-20 09:57:14', 1, 25),
(42, 1, 'por los momentos ninguna...', '2016-01-20 10:00:40', 1, 26),
(43, 3, 'por los momentos ninguna...', '2016-01-20 10:09:30', 1, 27),
(44, 2, 'por los momentos ninguna...', '2016-01-20 10:13:27', 1, 28),
(45, 2, 'por los momentos ninguna...', '2016-01-20 10:32:46', 1, 29),
(46, 2, 'por los momentos ninguna...', '2016-01-20 10:34:27', 1, 30),
(47, 2, 'por los momentos ninguna...', '2016-01-20 10:37:58', 1, 31),
(48, 2, 'por los momentos ninguna...', '2016-01-20 10:41:00', 1, 32),
(49, 2, 'por los momentos ninguna...', '2016-01-20 10:44:20', 1, 33),
(50, 2, 'por los momentos ninguna...', '2016-01-20 10:53:54', 1, 34),
(51, 2, 'por los momentos ninguna...', '2016-01-20 10:58:47', 1, 35),
(52, 2, 'por los momentos ninguna...', '2016-01-20 11:22:14', 1, 36),
(53, 3, 'por los momentos ninguna...', '2016-01-20 11:59:14', 1, 37),
(54, 2, 'por los momentos ninguna...', '2016-01-20 13:24:05', 1, 38),
(55, 2, 'por los momentos ninguna...', '2016-01-20 13:26:51', 1, 39),
(56, 2, 'por los momentos ninguna...', '2016-01-20 13:27:59', 1, 40),
(57, 3, 'por los momentos ninguna...', '2016-01-20 15:36:28', 1, 41),
(58, 3, 'por los momentos ninguna...', '2016-01-21 10:48:01', 1, 42),
(59, 3, 'por los momentos ninguna...', '2016-01-21 16:32:30', 1, 43),
(60, 3, 'por los momentos ninguna...', '2016-01-21 16:38:40', 1, 44);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paymentmethod`
--

CREATE TABLE IF NOT EXISTS `paymentmethod` (
  `paymentMethod_id` int(11) NOT NULL AUTO_INCREMENT,
  `tittle` varchar(40) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`paymentMethod_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `paymentmethod`
--

INSERT INTO `paymentmethod` (`paymentMethod_id`, `tittle`, `description`) VALUES
(1, 'Credit Card', 'payment'),
(2, 'Bank Transfer', 'payment'),
(3, 'Electronic Tranfer', 'payment');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserve`
--

CREATE TABLE IF NOT EXISTS `reserve` (
  `reserve_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `buyer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`reserve_id`),
  KEY `buyer_id` (`buyer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `reserve`
--

INSERT INTO `reserve` (`reserve_id`, `description`, `status`, `date_created`, `buyer_id`) VALUES
(1, 'Reservado por 2 dias', 'reserved', '2015-12-16', 1),
(2, 'Reservado por 2 dias', 'reserved', '2015-12-17', 2),
(3, 'Reservado por 2 dias', 'reserved', '2015-12-18', 3),
(4, 'Reservado por 2 dias', 'reserved', '2015-12-19', 4),
(5, 'Reservado por 2 dias', 'reserved', '2015-12-20', 5),
(6, 'Reservado por 2 dias', 'reserved', '2015-12-21', 6),
(7, 'Reservado por 2 dias', 'reserved', '2015-12-21', 7),
(8, 'Reservado por 2 dias', 'reserved', '2015-12-22', 8),
(9, 'Reservado por 2 dias', 'reserved', '2015-12-22', 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transactions`
--

CREATE TABLE IF NOT EXISTS `transactions` (
  `transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `component_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `num_parts` int(11) NOT NULL,
  PRIMARY KEY (`transaction_id`),
  KEY `fk_order` (`order_id`),
  KEY `fk_component` (`component_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=61 ;

--
-- Volcado de datos para la tabla `transactions`
--

INSERT INTO `transactions` (`transaction_id`, `order_id`, `component_id`, `amount`, `num_parts`) VALUES
(1, 5, 31, 80, 4),
(2, 5, 31, 100, 2),
(3, 6, 31, 60, 3),
(4, 6, 32, 200, 4),
(5, 6, 30, 300, 5),
(6, 6, 29, 80, 4),
(7, 7, 29, 20, 1),
(8, 8, 1, 50, 1),
(9, 8, 2, 40, 1),
(10, 8, 3, 10, 1),
(11, 9, 7, 500, 5),
(12, 9, 8, 900, 9),
(13, 10, 7, 300, 3),
(14, 10, 8, 800, 8),
(15, 11, 7, 700, 7),
(16, 12, 10, 275, 5),
(17, 13, 9, 100, 2),
(18, 13, 11, 105, 3),
(19, 14, 1, 50, 1),
(20, 14, 2, 80, 2),
(21, 14, 3, 30, 3),
(22, 15, 1, 200, 4),
(23, 15, 3, 30, 3),
(24, 16, 2, 160, 4),
(25, 17, 4, 50, 2),
(26, 17, 6, 50, 2),
(27, 18, 27, 2, 1),
(28, 18, 30, 240, 4),
(29, 18, 32, 200, 4),
(30, 19, 13, 125, 4),
(31, 19, 14, 40, 9),
(32, 20, 13, 250, 7),
(33, 20, 17, 47, 1),
(34, 20, 18, 100, 2),
(35, 21, 28, 300, 10),
(36, 22, 31, 400, 20),
(37, 23, 29, 780, 39),
(38, 24, 31, 180, 4),
(39, 25, 29, 780, 39),
(40, 25, 30, 5940, 99),
(41, 26, 35, 50, 1),
(42, 27, 35, 50, 1),
(43, 28, 35, 50, 8),
(44, 29, 11, 35, 1),
(45, 30, 11, 35, 1),
(46, 31, 12, 450, 9),
(47, 32, 12, 100, 1),
(48, 33, 39, 5000, 1),
(49, 34, 39, 2500, 50),
(50, 35, 39, 50, 1),
(51, 36, 39, 250, 1),
(52, 37, 39, 100, 2),
(53, 38, 12, 200, 2),
(54, 39, 9, 50, 1),
(55, 40, 8, 200, 2),
(56, 41, 32, 50, 1),
(57, 42, 15, 25, 1),
(58, 42, 17, 94, 2),
(59, 43, 15, 50, 2),
(60, 44, 30, 60, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `role` enum('user','editor','admin') COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Francisco', 'desarrollojr@soyservidor.com', '$2y$10$RZ7AlQl.C9uCl0Nya13vFenaK0VN3ypwLVpMfTJIB68TJ2cqtFrIi', 'admin', 'UDojazflF4gwuYub1sGfwyDXkDIUR2dUqppPHbHpfoUTV9zQqiQAPOKN2pmZ', '2015-12-16 03:10:55', '2016-01-16 18:17:17'),
(2, 'Jacobo', 'jacobo@soyservidor.com', '$2y$10$5.0I4ZC32mPwCibTpPs6BO29KWS9lE99yH3xkMxv4feghYdpF9F36', 'admin', 'AEARjX6f9HI7JCGViapqweMKcw0NGezasehn9P3Sxjbvy3d3wyMwPxfLr2b9', '2015-12-16 03:10:56', '2016-01-04 23:57:00'),
(3, 'Pedro picapiedra', 'pedro@soyservidor.com', '$2y$10$GwLsa8zbeEHN4cHXGe3a2esEQNQ7xK82VMXfKcv5MeCvaNxGR3fby', 'user', 'e01g7zIEmU1G86UCXRCjOpIzHyZkJFhQJ2WymdQ5TmW1llAIlDUWreqhY6Lx', '2015-12-16 03:59:10', '2015-12-16 04:05:28'),
(4, 'Eduardo Ramos', 'eduardo@soyservidor.com', '$2y$10$qKuqkkAr.UfNSOVg7FNuiecdubZH39nQWlmT0zzvZRQmHxU2Wk8eS', 'user', 'jNjonBErXEtR5jYyY6VHEe8F7S3hkt9IXVsyv3E0ZleGtdQLZVVgySO3Xb1s', '2015-12-16 04:00:15', '2015-12-16 04:00:31');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `city`
--
ALTER TABLE `city`
  ADD CONSTRAINT `city_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `country` (`country_id`);

--
-- Filtros para la tabla `destination`
--
ALTER TABLE `destination`
  ADD CONSTRAINT `destination_ibfk_1` FOREIGN KEY (`city_id`) REFERENCES `city` (`city_id`);

--
-- Filtros para la tabla `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `fk_package` FOREIGN KEY (`package_id`) REFERENCES `package` (`package_id`),
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`buyer_id`) REFERENCES `buyer` (`buyer_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `package`
--
ALTER TABLE `package`
  ADD CONSTRAINT `package_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `package_ibfk_3` FOREIGN KEY (`destination_id`) REFERENCES `destination` (`destination_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `package_component`
--
ALTER TABLE `package_component`
  ADD CONSTRAINT `package_component_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `package` (`package_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `package_component_ibfk_2` FOREIGN KEY (`component_id`) REFERENCES `component` (`component_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `payment`
--
ALTER TABLE `payment`
  ADD CONSTRAINT `fk_paymen_method` FOREIGN KEY (`payment_method_id`) REFERENCES `paymentmethod` (`paymentMethod_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `payment_ibfk_1` FOREIGN KEY (`component_id`) REFERENCES `component` (`component_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `payment_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `paymentmethod`
--
ALTER TABLE `paymentmethod`
  ADD CONSTRAINT `paymentmethod_ibfk_1` FOREIGN KEY (`paymentMethod_id`) REFERENCES `payment` (`payment_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `reserve`
--
ALTER TABLE `reserve`
  ADD CONSTRAINT `reserve_ibfk_2` FOREIGN KEY (`buyer_id`) REFERENCES `buyer` (`buyer_id`);

--
-- Filtros para la tabla `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `fk_component` FOREIGN KEY (`component_id`) REFERENCES `component` (`component_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_order` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
