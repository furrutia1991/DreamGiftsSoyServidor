<?php 


require_once('init.php');

$orders_to_cancel = db_get_array("SELECT email, name, order_id  FROM cscart_orders WHERE status = 'O' AND TIMESTAMPDIFF(HOUR,FROM_UNIXTIME(timestamp), NOW()) > 72");

$affected_rows =  db_query("UPDATE cscart_orders SET status = 'I' WHERE status = 'O' AND TIMESTAMPDIFF(HOUR,FROM_UNIXTIME(timestamp), NOW()) > 72");

foreach ($orders_to_cancel as $key => $value) 
{

	$name = $value['firstname'];
	$order_id = $value['order_id'];

	$para      = $value['email'];
	$titulo    = "Tu pedido #$order_id ha sido cancelado. Dreamgifts";
	$mensaje   = "Hola, $name. \nTu pedido #$order_id ha sido cancelado ya que no recibimos tu confirmacion de pago via deposito o transferencia bancaria\n\nAtte:\n\n Equipo de Dreamgifts";

	$cabeceras .= "From: Dreamgifts <desarrollojr@soyservidor.com>" . "\r\n";

	mail($para, $titulo, $mensaje,$cabeceras);

}