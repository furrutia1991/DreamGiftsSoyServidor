<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageComponent extends Model
{
 	protected $table = 'package_component';

 	protected $primaryKey = 'id';
 	public $timestamps = false;

 	public function package()
 	{
 		return $this->belongsTo('App\Package', 'package_id');
 	}

 	public function component()
 	{
 		return $this->belongsTo('App\Component', 'component_id');
 	}

 	public function reserve()
 	{
 		return $this->belongsTo('App\Reserve', 'reserve_id');
 	}
	public function payment()
 	{
 		return $this->belongsTo('App\Payment', 'payment_id');
 	} 	

}
