<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    protected $table = 'paymentmethod';
    protected $primaryKey = 'paymentMethod_id';
    public $timestamps = false;

    public function payment()
    {
    	return $this->hasMany('App\Payment', 'payment_id');
    }

}
