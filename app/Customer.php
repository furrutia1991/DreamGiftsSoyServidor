<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
 	protected $table = 'customer';

 	protected $primaryKey = 'customer_id';
 	public $timestamps = false;

 	public function package()
 	{
 		return $this->hasMany('App\Package', 'customer_id');
 	}

}
