<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Customer;
use Illuminate\Support\Facades\Mail;
use App\Package;
use App\Destination;
use App\Component;
use App\Payment;
use App\PaymentMethod;
use App\Reserve;
use App\PackageComponent;
use App\Transaction;
use App\Country;
use App\Buyer;
use App\City;
use App\Order;
use Storage;
use Image;
use File;
use DB;

class OrderController extends Controller
{

    public function getOrders($id)
    {
        $arr = array();
        $packageComponent = PackageComponent::where('package_id', $id)->get();
        $package = Package::find($id);
        $component = Component::where('package_id', $id)->get();
        //$transaction = Transaction::whereIn('component_id', $component[]);
        $order = Order::where('package_id', $id)->paginate(5);


        foreach ($component as $key => $value) {
            $arr[$value->tittle] = $transaction = Transaction::where('component_id', $value->component_id)->get();
        }

        //return($component);
        //return($payment);
        //return($package);
        return view('transaction.index', ['package' => $package, 'component' => $component, 'packageComponent' => $packageComponent, 'arr' => $arr, 'order' => $order]);
    }


    public function searchTransactions(Request $request, $id, $text)
    {

        $package = Package::find($id);

        $order = DB::table('orders')
                            ->join('buyer', 'buyer.buyer_id', '=', 'orders.buyer_id')
                            ->select('orders.*', 'buyer.name as buyer_name')
                            ->where(function ($query) use ($text){
                                        $query->where('buyer.name', 'LIKE', "%$text%")
                                                ->orWhere('orders.order_id' , 'LIKE', "%$text%")
                                                ->orWhere('orders.status' , 'LIKE', "%$text%");
                                    })
                                    ->get();

        return $order;

    }


    public function searchPackage($text)
    {
        if ($text == 'blank')
        {
            return ['redirect' => 'paquetes'];
        }

        $package = DB::table('package')
                                ->join('customer', 'customer.customer_id', '=', 'package.customer_id')
                                ->join('destination', 'destination.destination_id', '=', 'package.destination_id')
                                ->select('package.*', 'customer.name as customer_name', 'destination.name as destination_name')
                                ->where(function ($query) use ($text){
                                        $query->where('package.package_id', 'LIKE', "%$text%")
                                                ->orWhere('destination.name' , 'LIKE', "%$text%")
                                                ->orWhere('customer.name' , 'LIKE', "%$text%");
                                    })
                                    ->get();

        return $package;
    }


    public function getComponentOrder($id, $pid)
    {
        $order = Order::find($id);
        $package = Package::find($pid);
        $transaction = Transaction::where('order_id', $id)->get();
        $payment = Payment::where('order_id', $id)->get();
        $status = $this->getEnumValues('status');
        //return($payment);
        return view('transaction.transaction', ['transaction' => $transaction, 'payment' => $payment, 'order' => $order, 'package' => $package, 'status' => $status]);
    }

     public function getComponentOrderSearch($psid,$id, $pid)
    {
        $order = Order::find($id);
        $package = Package::find($pid);
        $transaction = Transaction::where('order_id', $id)->get();
        $payment = Payment::where('order_id', $id)->get();
        $status = $this->getEnumValues('status');
        //return($payment);
        return view('transaction.transaction', ['transaction' => $transaction, 'payment' => $payment, 'order' => $order, 'package' => $package, 'status' => $status]);
    }

    
    public function getDataFromCliente_buyer(Request $request)
    {
        $info_to_update = $request->input('data');//el detalle completo de la compra
        $id_package = $request->input('id_package');//id del paquete
        $info_payment = $request->input('pay');//informacion del pago, si es pago por tarjeta de credito genera mas info
        $general_total = $request->input('general_total');//valor total de la transaccion
        $personal_data = $request->input('data_personal');//datos personales del buyer


        $Buyer = Buyer::find($personal_data['email']);
        if (!$Buyer) {
            $Buyer = $this->createBuyer($personal_data);
        }
//        $id_reserve = $this->createReserve($Buyer->buyer_id);
        $order = $this->createOrder($Buyer->buyer_id, $general_total, $id_package, $info_payment['type_methode']);
        //obtener el id de la orden y llamar a la API
        $payment = $this->createPayment($info_payment, $order->order_id);


        // @todo validacion  


        foreach ($info_to_update as $componentInfo):
            $this->createTransaction($componentInfo, $order->order_id);
            $this->updateComponent($componentInfo);
        endforeach;
        $order_id = $order->order_id;
//        $em = $this->sendEmail($info_payment['type_methode'], $personal_data, $order, $info_to_update, $general_total);

        $key_id = "5678647";
        $key = 'D3RnP8g6CP2Y4W5NmPC355bW5fA7r3E5';
        $orderid = $order_id;
        $amount = $general_total;
        // $gmdate = gmdate("H, i, s, n, j, Y", time());
        // $time = gmmktime($gmdate);
        $time = time();
        $md5 = "" . $orderid . "|" . $amount . "|" . $time . "|" . $key;
        $hash = md5($md5);

        $apiArray = array('key_id' => $key_id, 'key' => $key, 'order' => $orderid, 'amount' => $amount, 'time' => $time, 'hash' => $hash, 'order' => $order_id);

        //return "Orden " . $order->order_id . " creada exitosamente ";

        return json_encode($apiArray);

    }

    public function getDataFromCliente_buyer_reserve(Request $request)
    {
        $info_to_update = $request->input('data');//el detalle completo de la compra
        $id_package = $request->input('id_package');//id del paquete
        $info_payment = $request->input('pay');//informacion del pago, si es pago por tarjeta de credito genera mas info
        $general_total = $request->input('general_total');//valor total de la transaccion
        $personal_data = $request->input('data_personal');//datos personales del buyer


        $Buyer = Buyer::find($personal_data['email']);
        if (!$Buyer) {
            $Buyer = $this->createBuyer($personal_data);
        }
        $order = $this->createOrder($Buyer->buyer_id, $general_total, $id_package, $info_payment['type_methode']);
        //obtener el id de la orden y llamar a la API
        $payment = $this->createPayment($info_payment, $order->order_id);

        foreach ($info_to_update as $componentInfo):
            $this->createTransaction($componentInfo, $order->order_id);
            $this->updateComponent($componentInfo);
        endforeach;
        $order_id = $order->order_id;

        return "prueba exitosa!!!";
    }

    public function callback()
    {
        
        $eva_a = $_GET['response'];
        if($eva_a=="1"){
            $completed = "P";
            $res="Transacci�n APROBADA - ".$eva_a.", Transacci�n: ".$_GET['transactionid'].", Autorizaci�n: ".$authcode;
            $res_eva="La transaccion fue finalizada con exito.";
            $res_comp="Por favor guarde su N�mero de transacci�n en un lugar seguro.";

            return view('transaction.response', array('eva_a' => $eva_a , 'completed' => $completed,  'res' => $res, 'res_eva' => $res_eva, 'res_comp' => $res_comp));

        }else{
            if($eva_a=="3"){
                $completed = "O";
                $res="Transacci�n NO PROCESADA - Respuesta: ".$_GET['response']." codigo: ".$_GET['response_code'].", ".$_GET['responsetext'];
                $res_eva="Error momentaneo del sistema.";
                $res_comp="Por favor intentelo mas tarde.";

                return view('transaction.response', array('eva_a' => $eva_a , 'completed' => $completed,  'res' => $res, 'res_eva' => $res_eva, 'res_comp' => $res_comp));
            }else{
                if($eva_a=="2"){
                    $completed = "D";
                    $res="DENEGADO-".$eva_a."- Respuesta: ".$_GET['response']." codigo: ".$_GET['response_code'].", ".$_GET['responsetext'];
                    $res_eva="Su tarjeta de credito fue denegada.";
                    $res_comp="Por favor contacte al emisor de su tarjeta.";

                    return view('transaction.response', array('eva_a' => $eva_a , 'completed' => $completed,  'res' => $res, 'res_eva' => $res_eva, 'res_comp' => $res_comp));

                }else{
                    if($eva_a!=""){
                        $completed = "D";
                        $res="NO EJECUTADO-".$eva_a."- Respuesta: ".$_GET['response']." codigo: ".$_GET['response_code'].", ".$_GET['responsetext'];
                        $res_eva="Transaccion declinada.";
                        $res_comp="Por favor contacte al emisor de su tarjeta.";

                        return view('transaction.response', array('eva_a' => $eva_a , 'completed' => $completed,  'res' => $res, 'res_eva' => $res_eva, 'res_comp' => $res_comp));

                    }else{
                        $completed = "A";
                        $res="Error de comunicaci�n NO HAY RESPUESTA-".$eva_a;
                        $res_eva="Error momentaneo del sistema.";
                        $res_comp="Por favor intentelo mas tarde.";        

                        return view('transaction.response', array('eva_a' => $eva_a , 'completed' => $completed,  'res' => $res, 'res_eva' => $res_eva, 'res_comp' => $res_comp));
                    }
                    
                }
            }
        }

        return redirect()->back();
    }
  

    public function email()
    {
        return view('email.email');
    }


    public function createTransaction($componentInfo, $id_order)
    {
        $transaction = new Transaction();
        return $this->saveTransaction($transaction, $componentInfo, $id_order);
    }

    public function saveTransaction($transaction, $componentInfo, $id_order)
    {
        $transaction->order_id = $id_order;
        $transaction->component_id = $componentInfo['id_source'];
//        $transaction->status = "processing";
        $transaction->amount = $componentInfo['value'];
        $transaction->num_parts = $componentInfo['gifts'];
        $transaction->save();
        return $transaction;
    }

    public function updateComponent($componentInfo)
    {
        $component = Component::find($componentInfo['id_source']);
        return $this->saveComponent($component, $componentInfo);
    }

    public function saveComponent($component, $componentInfo)
    {
        date_default_timezone_set('America/Costa_Rica');
        $datetime = date('Y-m-d H:i:s ', time());
        $component->balance = (floatval($component->balance) - floatval($componentInfo['value']));
        $component->num_parts_balance = (intval($component->num_parts_balance) - intval($componentInfo['gifts']));
        $component->date_updated = $datetime;
        $component->save();
        return $component;
    }

    /**
     * Funcion que crea una orden en el sistema
     *
     *
     * @return mixed
     */
    public function createBuyer($data)
    {
        $Buyer = new Buyer();
        return $this->saveBuyer($Buyer, $data);
    }

    public function createOrder($id_buyer, $general_total, $id_package, $type_methode)
    {
        $order = new Order();
        return $this->saveOrder($order, $id_buyer, $general_total, $id_package, $type_methode);
    }

    public function createReserve($id_buyer)
    {
        $reserve = new Reserve();
        $status = "actived";
        return $this->saveReserve($reserve, $id_buyer, $status);
    }

    public function saveReserve($reserve, $id_buyer, $status)
    {
        date_default_timezone_set('America/Costa_Rica');
        $datetime = date('Y-m-d H:i:s ', time());
        $reserve->description = "por 4 dias";
        $reserve->status = $status;
        $reserve->date_created = $datetime;
        $reserve->buyer_id = $id_buyer;
        $reserve->save();
        return $reserve;
    }

    public function saveBuyer($buyer, $data)
    {
        $buyer->email = $data['email'];
        $buyer->name = $data['name'] . " " . $data['lastname'];
        $buyer->phone_number = $data['telephone'];
        $buyer->save();
        return $buyer;
    }

    /**
     * Funcion que crea un pago en el sistema
     * @param $infoPayment
     *
     * @return mixed
     */
    public function createPayment($infoPayment, $order_id)
    {
        $Payment = new Payment();
        date_default_timezone_set('America/Costa_Rica');
        $datetime = date('Y-m-d H:i:s ', time());
        $Payment->payment_method_id = $infoPayment['type_methode'];
        $Payment->description = "por los momentos ninguna...";
//        $Payment->amount = $total;
        $Payment->date_created = $datetime;
        $Payment->order_id = $order_id;
        $Payment->component_id = 1;
        $Payment->save();
        return $Payment;
    }


    /**
     * la funcion save order crea o actualiza una orden
     * @param $order
     *
     *
     */
    public function saveOrder($order, $id_buyer, $total, $id_package, $type_methode)
    {
        date_default_timezone_set('America/Costa_Rica');
        $datetime = date('Y-m-d H:i:s ', time());
        
        $status = "";
        switch ($type_methode) {
            case "1":
                $status = "processing";
                break;
            case "2":
                $status = "pending";
                break;
            case "3":
                $status = "pending";
                break;
        }
//        $order->reserve_id = $id_reserve;
//        $order->payment_id = $id_payment;
        $order->date_created = $datetime;
        $order->status = $status;
        $order->total = $total;
        $order->buyer_id = $id_buyer;
        $order->package_id = $id_package;
        $order->save();
        return $order;
    }

    public function saveStatus(Request $request, $id, $pid)
    {
        $id_order = $request->input('id_order');
        $order = Order::find($id);
        $transactions = DB::table('transactions')->where('order_id', '=', $id_order)->get();

//        $package = Package::find($pid);
        $newStatus = $request->input('status');
        if ($newStatus == "canceled") {
            foreach ($transactions as $t):
                if ($t):
                    $component = Component::find($t->component_id);
                    $component->num_parts_balance = $component->num_parts_balance + $t->num_parts;
                    $component->balance = $component->balance + $t->amount;
                    $component->save();
                endif;
            endforeach;
        }
        $order->status = $newStatus;
        $order->save();
        return redirect()->back();
    }

    public function deleteTransaction($id, $pid)
    {
        $package = Package::find($pid);
        Order::destroy($id);
        return redirect()->route('getOrders', $package);
    }


//     public function sendEmail($id, $buyer, $order, $arr, $total)
//     {
//         $metodo = "";
//         $mesajeBuyer = "";
//         switch ($id) {
//             case "1":
//                 $metodo = "Tarjeta de Credito";
//                 $mesajeBuyer = "Has realizado un regalo DREAMGIFTS por medio de tarjeta de credito";
//                 break;
//             case "2":
//                 $metodo = "Transferencia Bancaria";
//                 $mesajeBuyer = "Has realizado un regalo DREAMGIFTS por medio de transferencia bancaria";
//                 break;
//             case "3":
//                 $metodo = "Transferencia Electronica";
//                 $mesajeBuyer = "Has realizado un regalo DREAMGIFTS por medio de transferencia electronica";
//                 break;
//         }
//         $cabeceras = 'MIME-Version: 1.0' . "\r\n";
//         $cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
//         $mensaje = "<H3>Transaccion nueva de un cliente</H3> ";
//         $mensaje .= "<p>Has Recibido un pago de " . $buyer['name'] . $buyer['lastname'] . ". Su pedido es el siguiente: </p>";
//         $mensaje .= "<h5> #Orden" . $order->order_id . "(" . $order->date_created . ")</h5>";
//         $mensaje .= "<br>";

//         $table = "<table border='1' cellspacing='0'><thead><tr><th>Producto</th><th>Cantidad</th><th>Total</th></tr></thead><tbody>";
//         foreach ($arr as $product) {
//             $table .= "<tr><td>" . $product['general_name'];

//             $table .= "</td><td>x " . $product['gifts'] . "</td>";
//             $table .= "<td>$ " . number_format(floatval($product['value']), 2) . "</td></tr>";
//         }
//         $table .= "<tr><td colspan='2'>Subtotal:</td><td>$ " . number_format($total, 2) . "</td></tr>";
//         $table .= "<tr><td colspan='2'>M&eacute;todo de pago:</td><td>" . $metodo . "</td></tr>";
//         $table .= "<tr><td colspan='2'>Total:</td><td>$ " . number_format($total, 2) . "</td></tr>";
//         $table .= "</tbody></table>";
//         $mensaje .= $table;
//         $mensaje .= "<br>";
//         $mensaje .= "<h5>Datos de Cliente<h5>";
//         $mensaje .= "<p><strong>Correo electronico:</strong> " . $buyer['email'] . " </p>";
//         $mensaje .= "<p><strong>Tel:</strong> " . $buyer['telephone'] . " </p>";
//         $mensaje .= "<h4>Facturar a </h4>";
//         $mensaje .= "<p>" . $buyer['name'] . " " . $buyer['lastname'] . "</p>";
//         $mensaje .= "<h5>Detalles de la orden de Impresion<h5>";
// //        $mensaje .= "<p><strong>Centro de impresion:</strong> " . $arr['localidad'] . " </p>";

// //        $email = "desarrollojr@soyservidor.com";
//         $email = $buyer['email'];
// // Si cualquier l�nea es m�s larga de 70 caracteres, se deber�a usar wordwrap()
//         $mensaje = wordwrap($mensaje, 70, "\r\n");
//         $cabeceras .= 'To: Equipo <' . $email . '>' . "\r\n";
//         $cabeceras .= 'From: Nuevo Pedido <support@dreamgifts.hn>' . "\r\n";
//         $sent_message = mail($email, '[ADMIN DREAMGIFTS] Informacion de transaccion (' . $order->order_id . ')', $mensaje, $cabeceras, $mesajeBuyer);
// //        $sent_message2 = mail($buyer['email'], '[ADMIN DREAMGIFTS] Informacion de transaccion (' . $order->order_id . ')', $mesajeBuyer, $cabeceras);


//     }

    public function sendEmailTo()
    {

    }


    public function getEnumValues($column)
    {
        $type = DB::select(DB::raw("SHOW COLUMNS FROM orders WHERE Field = '$column'"))[0]->Type;
        preg_match('/^enum\((.*)\)$/', $type, $matches);
        $enum = array();
        foreach (explode(',', $matches[1]) as $value) {
            $v = trim($value, "'");
            $enum = array_add($enum, $v, $v);
        }
        return $enum;
    }


}
