<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Customer;
use App\Package;
use App\Destination;
use App\Component;
use App\Payment;
use App\PaymentMethod;
use App\Reserve;
use App\PackageComponent;
use App\Country;
use App\Buyer;
use App\City;
use Storage;
use Image;
use File;
use DB;

class PackageController extends Controller
{
    
    public function getPackages()
    {
        $package = Package::paginate(5);
        $customer = Customer::all();
        $destination = Destination::all();
        $component = Component::all();
        return view('package.index', ['package' => $package, 'customer' =>$customer, 'destination' =>$destination, 'component' => $component]);

        //return ($destination);
    }

    public function getPackage($id)
    {
        $package = Package::find($id);
        $customer = Customer::all();
        $destination = Destination::all();
        $component = Component::all();
        $packageComponent = PackageComponent::where('package_id', $id)->get();
        $payment = Payment::all();
        $buyer = Buyer::all();
        $reserve = Reserve::all();
        $paymentmethod = PaymentMethod::all();


        return view('package.package', ['package' =>$package, 'customer' =>$customer, 'destination' =>$destination, 'component' => $component, 'packageComponent' => $packageComponent, 'payment' => $payment, 'buyer' => $buyer, 'reserve' => $reserve, 'paymentmethod' => $paymentmethod]);
        //return ($package);
    }

    public function searchPackage($text)
    {
        if ($text == 'blank')
        {
            return ['redirect' => 'paquetes'];
        }

        $package = DB::table('package')
                                ->join('customer', 'customer.customer_id', '=', 'package.customer_id')
                                ->join('destination', 'destination.destination_id', '=', 'package.destination_id')
                                ->select('package.*', 'customer.name as customer_name', 'destination.name as destination_name')
                                ->where(function ($query) use ($text){
                                        $query->where('package.package_id', 'LIKE', "%$text%")
                                                ->orWhere('destination.name' , 'LIKE', "%$text%")
                                                ->orWhere('customer.name' , 'LIKE', "%$text%");
                                    })
                                    ->get();

        return $package;
    }


    public function searchDestination($text)
    {
        if ($text == 'blank')
        {
            return ['redirect' => 'destinos'];
        }

        
        $destination = DB::table('destination')
                                    ->join('city', 'city.city_id', '=', 'destination.city_id')
                                    ->join('country', 'country.country_id', '=','city.country_id')
                                    ->select('destination.*', 'city.name as city_name', 'country.name as country_name')
                                    ->where(function ($query) use ($text){
                                        $query->where('destination.name', 'LIKE', "%$text%")
                                                ->orWhere('city.name' , 'LIKE', "%$text%")
                                                ->orWhere('country.name' , 'LIKE', "%$text%");
                                    })
                                    ->get();
        
        return $destination;
    }

    // Función que devuelve los paquetes a la vista del comprador
    public function getPackageUser($id)
    {

        $package = Package::find($id);
        $customer = Customer::all();
        $destination = Destination::all();
        $component = Component::all();
        $packageComponent = PackageComponent::where('package_id', $id)->get();



        return view('welcome', ['package' =>$package, 'customer' =>$customer, 'destination' =>$destination, 'component' => $component, 'packageComponent' => $packageComponent]);
        //return ($package);
    }

    public function deletePackage($id)
    {
        Package::destroy($id);
        return redirect()->back();
    }

    

    
    public function saveWedding(Request $request, $id)
    {
         $rules = [
            'picture' => 'required',
            'date_event' => 'required',
            'date_finalized' => 'required',
            'destination' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
                
            return redirect()->back()->withErrors($validator->errors());

        }

        $package = new Package();
        $customer = Customer::find($id);
        $destination = Destination::all();
        // return ($id);


        $component = Component::all();
        $packageComponent = PackageComponent::where('package_id', $package->package_id)->get();
        $payment = Payment::all();
        $buyer = Buyer::all();
        $reserve = Reserve::all();
        $paymentmethod = PaymentMethod::all();



        $package->customer_id = $id;
        $package->destination_id = $request->input('destination');
        if($file = $request->file('picture'))
        {
//            $mime = $request->file('picture')->getMimeType();
//            $extension = $file->getClientOriginalExtension();

            $placeToSaveFiles =  public_path();
            opendir($placeToSaveFiles);
            $filename = $_FILES['picture']['name'];
            $destination = $placeToSaveFiles . '/images/' . $filename;
            copy($_FILES['picture']['tmp_name'], $destination);
            $pathAgreeFile = "public/images/" . $filename;
            $package->picture = "/DreamGiftsSoyServidor/public/images/".$filename;
        }
        $package->message = $request->input('message');
        $datetime = date('Y-m-d H:i:s ', time());
        $package->date_created = $datetime;
        $package->date_finalized = $request->input('date_finalized');
        $package->date_event = $request->input('date_event');
        $package->amount = $request->input('amount');
         if($package->save())
        {
            return view('package.package', ['package' =>$package, 'customer' =>$customer, 'destination' =>$destination, 'component' => $component, 'packageComponent' => $packageComponent, 'payment' => $payment, 'buyer' => $buyer, 'reserve' => $reserve, 'paymentmethod' => $paymentmethod]);
        }
        else
        {
            return redirect()->back()->withErrors(['error' => 'Ocurrio un error al guardar el paquete']);     
        }        
        //return redirect()->back();
    }

    public function newPackage(Request $request)
    {
        $package = new Package();
        return $this->savePackage($request, $package);
    }

    public function newPackages($id)
    {
        $package = new Package();
        $customer = Customer::find($id);
        $destination = Destination::all();
        $city = City::all();
        return view('package.newPackage', ['package' =>$package, 'customer' => $customer, 'city' => $city,   'destination' => $destination] );
    }

    public function savePackage($request, $package)
    {   
        $rules = [
            'message' => 'required',
            'date_event' => 'required',  
            'amount' => 'required'          
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
                
            return redirect()->back()->withErrors($validator->errors());
        }
        $package->customer_id = $request->input('name');
        $package->destination_id = $request->input('destination');
        // $package->picture = $request->input('picture');
        $package->message = $request->input('message');
        $datetime = date('Y-m-d H:i:s ', time());
        $package->date_created = $datetime;
        $package->date_finalized = $request->input('date_finalized');
        $package->date_event = $request->input('date_event');
        $package->amount = $request->input('amount');
        
        
        if($file = $request->file('picture'))
        {
//            $mime = $request->file('picture')->getMimeType();
//            $extension = $file->getClientOriginalExtension();

            $placeToSaveFiles =  public_path();
            opendir($placeToSaveFiles);
            $filename = $_FILES['picture']['name'];
            $destination = $placeToSaveFiles . '/images/' . $filename;
            copy($_FILES['picture']['tmp_name'], $destination);
            $pathAgreeFile = "public/images/" . $filename;
            $package->picture = "/DreamGiftsSoyServidor/public/images/".$filename;
        }
        if($package->save())
        {
            return redirect()->back();  

        }
        else
        {
            return redirect()->back()->withErrors(['error' => 'Ocurrio un error al guardar el paquete']);     
        } 

        //return($package);

    }

    public function updatePackage(Request $request, $id)
    {
        $package = Package::find($id);
        $package->message = $request->input('message');
        $package->customer->name = $request->input('name');
        $package->destination->name = $request->input('destination');
        $datetime = date('Y-m-d H:i:s ', time());
        $package->date_created = $datetime;
        $package->date_finalized = $request->input('date_finalized');
        $package->date_event = $request->input('date_event');
        $package->amount = $request->input('amount');
        if($file = $request->file('picture'))
        {
            $mime = $request->file('picture')->getMimeType();
            $extension = $file->getClientOriginalExtension();

            $placeToSaveFiles =  public_path();
            opendir($placeToSaveFiles);
            $filename = $_FILES['picture']['name'];
            $destination = $placeToSaveFiles . '/images/' . $filename;
            copy($_FILES['picture']['tmp_name'], $destination);
            $pathAgreeFile = "public/images/" . $filename;
            $package->picture = "/DreamGiftsSoyServidor/public/images/".$filename;
        }
        if($package->save())
        {
            return redirect()->back();  
        }
        else
        {
            return redirect()->back()->withErrors(['error' => 'Ocurrio un error al guardar el paquete']);     
        }        


        // return $this->savePackage($request, $package);
    }

    public function newComponent(Request $request, $id)
    {
        $component = new Component();
        // return ("Nuevo componente");
        return $this->saveComponent($request, $component, $id);
    }

    public function updateComponent(Request $request, $id)
    {
        $rules = [
            'tittle' => 'required',
            'amount' => 'required',  
            'balance' => 'required'          
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
                
            return redirect()->back()->withErrors($validator->errors());
        }
        $datetime = date('Y-m-d H:i:s ', time());

        $component = Component::find($id);
        $component->tittle = $request->input('tittle');
        $component->description = $request->input('description');
        $component->amount = $request->input('amount');
        $component->balance = $request->input('balance');
        $component->num_parts = $request->input('num_parts');
        $component->num_parts_balance = $request->input('num_parts_balance');
        $component->date_created = $datetime;
        $component->date_updated = $datetime;

        if($file = $request->file('picture'))
        {
            $mime = $request->file('picture')->getMimeType();
            $extension = $file->getClientOriginalExtension();

            $placeToSaveFiles =  public_path();
            opendir($placeToSaveFiles);
            $filename = $_FILES['picture']['name'];
            $destination = $placeToSaveFiles . '/images/components/' . $filename;
            copy($_FILES['picture']['tmp_name'], $destination);
            $pathAgreeFile = "public/images/components/" . $filename;
            $component->picture = "/DreamGiftsSoyServidor/public/images/components/".$filename;
            $component->save();
        }

        if($component->save())
        {
            return redirect()->back();
        }
        else
        {
            return redirect()->back()->withErrors(['error' => 'Ocurrio un error al guardar el paquete']);     
        }       
    }

    public function saveComponent($request, $component, $id)
    {
          $rules = [
            'tittle' => 'required',
            'total_amount' => 'required'      
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
                
           
           return redirect()->back()->withErrors($validator->errors());
        }

        $component->tittle = $request->input('tittle');
        $component->description = $request->input('description');
        $num_parts = $request->input('num_parts');
        $total_amount = $request->input('total_amount');
        $component->num_parts_balance = $request->input('num_parts');
        $component->num_parts = $num_parts;
        $component->total_amount = $total_amount;
        $amount = $total_amount / $num_parts;
        $component->amount = $amount;
        $component->package_id = $id;
        $component->balance = $request->input('total_amount');
        $datetime = date('Y-m-d H:i:s ', time());
        $component->date_created = $datetime;
        $component->date_updated = $datetime;

        if($file = $request->file('picture'))
        {
//            $mime = $request->file('picture')->getMimeType();
//            $extension = $file->getClientOriginalExtension();

            $placeToSaveFiles =  public_path();
            opendir($placeToSaveFiles);
            $filename = $_FILES['picture']['name'];
            $destination = $placeToSaveFiles . '/images/components/' . $filename;
            copy($_FILES['picture']['tmp_name'], $destination);
            $pathAgreeFile = "public/images/components/" . $filename;
            $component->picture = "/DreamGiftsSoyServidor/public/images/components/".$filename;
            $component->save();
        }
        if($component->save())
        {
            $packageComponent = new PackageComponent();
            $packageComponent->package_id = $id;
            $id_component = $component->component_id;
            $packageComponent->component_id = $id_component;
            $packageComponent->save();
            //return("guardado");
            return redirect()->back();
        }
        else
        {
            return("Error al guardar");
            //return redirect()->back()->withErrors(['error' => 'Ocurrio un error al guardar el paquete']);     
        }

    }


    public function getDestinations()
    {
        $destination = Destination::paginate(5);
        $country = Country::all();
        $city = City::all();
        //return($country);
        return view('destination.index', ['destination' => $destination, 'country' => $country, 'city' => $city]);

    }

    public function getDestination($id)
    {   
        $destination = Destination::find($id);
        $city = City::all();
        return view('destination.destination', ['destination' => $destination, 'city' => $city]);

    }


    public function newDestination(Request $request)
    {
        $destination = new Destination();
        $city = new City();
        $destination->description = $request->input('description');
        $destination->name = $request->input('name');
        $destination->city_id = $request->input('city');
        $destination->phone = $request->input('phone');
        $destination->postal_code = $request->input('postal_code');
        $destination->save();

        return redirect()->back();

    }

    public function deleteDestination($id)
    {
        Destination::destroy($id);
        return redirect()->back();
    }

    public function updateDestination(Request $request, $id)
    {
        $rules = [
            'name' => 'required',
            'city' => 'required', 
            'phone' => 'required',
            'description' => 'required'

        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
                
            return redirect()->back()->withErrors($validator->errors("debes elegir una ciudad"));
        }

        $destination = Destination::find($id);
        $destination->description = $request->input('description');
        $destination->postal_code = $request->input('postal_code');
        $destination->name = $request->input('name');
        $destination->phone = $request->input('phone');
        $destination->city_id = $request->input('city');
        $destination->save();

        return redirect()->back();
    }

}
