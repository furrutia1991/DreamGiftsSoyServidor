<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Customer;
use App\Package;
use App\Destination;
use App\Component;
use App\Payment;
use App\PaymentMethod;
use App\Reserve;
use App\PackageComponent;
use App\Country;
use App\Buyer;
use App\City;
use Storage;
use Image;
use File;
use DB;


class CustomerController extends Controller
{
    public function getCustomers()
    {
        $customers = Customer::paginate(5);
        return view('customer.index', ['customers' => $customers]);
    }
   
    public function getCustomer($id) 
    {
        $customer = Customer::find($id);
        $package = \DB::table('package')
                    ->where('customer_id', $id)->get();
        if ($package)
        {
            return view('customer.customer',['customer' => $customer, 'package'=> $package[0]]);
        }
        else
        {
            return view('customer.customer',['customer' => $customer, 'package'=> false]);
        }

        //dd($package[0]);
        //return ($package[0]);
    }

    public function  searchCustomer($text)
    {
        if ($text == 'blank') 
        {
            return Customer::all();
        }

        $customers = Customer::where('name', 'LIKE', "%$text%")->orWhere('dni_number' , $text)->get();
        
        return $customers;
    }

     public function deleteCustomer($id)
    {
        Customer::destroy($id);
        return redirect()->back();
    }

    public function updateCustomer(Request $request, $id)
    {
        $customer = Customer::find($id);
        return $this->saveCustomer($customer, $request);
    }

    public function newCustomer(Request $request)
    {
        $customer = new Customer();
        return $this->saveCustomer($customer, $request);
    }


    public function newCustomerToNewPackage(Request $request)
    {
        $customer = new Customer();
        return $this->saveCustomerToNewPackage($customer, $request);
    }

    public function saveCustomer($customer, $request)
    {
        
        $rules = [
            'name' => 'required',
            'address' => 'required',
            'dni_number' => 'required',
            'phone_number' => 'required'            
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
                
            return redirect()->back()->withErrors($validator->errors());
        }
        
        $customer->name = $request->input('name');
        $customer->dni_number = $request->input('dni_number');
        $customer->address = $request->input('address');
        $customer->phone_number = $request->input('phone_number');
        $customer->email = $request->input('email');

        $customer->save();
        return redirect()->back();
    }

    public function newCustomerPackage()
    {
        return view('customer.newCustomer');
    }

    public function saveCustomerToNewPackage($customer, $request)
    {
        
        $rules = [
            'name' => 'required',
            'address' => 'required',
            'dni_number' => 'required',
            'phone_number' => 'required'            
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
                
            return redirect()->back()->withErrors($validator->errors());
        }

        $package = Package::all();
        $destination = Destination::all();
        $customer->name = $request->input('name');
        $customer->dni_number = $request->input('dni_number');
        $customer->address = $request->input('address');
        $customer->phone_number = $request->input('phone_number');
        $customer->email = $request->input('email');

        $customer->save();
        return view('package.newPackage' , ['customer' => $customer, 'destination' => $destination]); 
    }

    
}
