<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Customer;
use App\Package;
use App\Destination;
use App\Component;
use App\Payment;
use App\PaymentMethod;
use App\Reserve;
use App\PackageComponent;
use App\Buyer;
use Storage;
use Image;
use File;
use DB;


class PackageComponentController extends Controller
{

    public function getPackagesComponents()
    {
        $packageComponents = PackageComponent::all();
        return view('packageComponent.index', ['packageComponents' => $packageComponents]);
    }

    public function getPackageComponent($id)
    {
        $packageComponent = PackageComponent::find($id);
        $package = Package::all();
        $component = Component::all();
        $payment = Payment::all();
        $buyer = Buyer::all();
        $reserve = Reserve::all();
        $paymentmethod = PaymentMethod::all();
        //return($payment);
        //return ($packageComponent);
        return view('packageComponent.packageComponent', ['packageComponent' => $packageComponent, 'package' => $package, 'component'=> $component, 'payment' => $payment, 'buyer' => $buyer, 'reserve'=> $reserve]);    
    }

    public function deletePackageComponent($id)
    {
        PackageComponent::destroy($id);
        return redirect()->back();

    }

}
