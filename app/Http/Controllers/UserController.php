<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;


class UserController extends Controller
{
    public function index()
    {
        $users = User::orderBy('active', 0)->get();
        return view('user.index', ['users' => $users, 'user_active' => true]);
    }

    public function deleteUser($id)
    {
    	User::destroy($id);
    	return redirect()->back();
    }

    public function changeStatus(Request $request)
    {
    	$user = User::find($request->input('user_id'));
    	$user->active = $request->input('status');
    	$user->save();
    	return;
    }
}
