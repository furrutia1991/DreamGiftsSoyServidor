<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/terminos-y-condiciones', ['as' => 'terms', 'uses' => 'HomeController@terms']);

Route::get('/email', ['as'=> 'email', 'uses' => 'OrderController@email']);

Route::get('/paquete/{id}', ['as'=> 'getPackageUser', 'uses' => 'PackageController@getPackageUser']);

Route::post('/process-data',['as' => 'getDataFromBuyer', 'uses' => 'OrderController@getDataFromCliente_buyer']);
Route::post('/process-data-reserve',['as' => 'getDataFromBuyer_toReserve', 'uses' => 'OrderController@getDataFromCliente_buyer_reserve']);

Route::get('/boda/{id}', ['as'=> 'getPackageUser', 'uses' => 'PackageController@getPackageUser']);

Route::get('/callback', ['as' => 'callback', 'uses' => 'OrderController@callback']);


Route::post('/process-data-backup', function(){

    // $data = $_POST['data'];
    // $pivot = "";
    // foreach ($data as $value) {
    // $pivot . "name: ".$value["name"]." value ".$value["value"]." titulo: ".$value["general_name"]." descrpcion".$value["description"]
    // }
    // $datatest = $_POST['tam'];
    // echo "mazizo ".$datatest.$data[0];
});

Route::group(['middleware' => 'auth'], function(){

    Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

    Route::get('/home', ['as' => 'home', 'uses' => 'HomeController@index']);

        
    /*Customers*/

    Route::get('/clientes/buscar/{text}', ['as' => 'searchCustomer', 'uses' => 'CustomerController@searchCustomer']);

    Route::get('/clientes', ['as'=> 'customers', 'uses' => 'CustomerController@getCustomers']);

    Route::get('/cliente/{id}', ['as'=> 'customer', 'uses' => 'CustomerController@getCustomer']);

    Route::get('/cliente/eliminar/{id}', ['as'=> 'deleteCustomer', 'uses' => 'CustomerController@deleteCustomer']);

    Route::post('/cliente', ['as'=> 'newCustomer', 'uses' => 'CustomerController@newCustomer']);

    Route::post('/cliente/{id}', ['as'=> 'updateCustomer', 'uses' => 'CustomerController@updateCustomer']);

    route::get('/nuevo_cliente', ['as' => 'newCustomerPackage', 'uses' => 'CustomerController@newCustomerPackage' ]);

    Route::post('/nuevo_cliente/nuevo_paquete', ['as' => 'newCustomerToNewPackage', 'uses' => 'CustomerController@newCustomerToNewPackage' ]);


    /*Package*/

    Route::get('/paquetes/buscar/{text}', ['as' => 'searchPackage', 'uses' => 'PackageController@searchPackage']);

    Route::get('/paquetes', ['as'=> 'packages', 'uses' => 'PackageController@getPackages']);

    Route::get('/paquete/{id}', ['as'=> 'package', 'uses' => 'PackageController@getPackage']);

    Route::get('/paquete/eliminar/{id}', ['as'=> 'deletePackage', 'uses' => 'PackageController@deletePackage']);

    Route::get('/cliente/{id}/nuevopaquete', ['as' => 'newPackages' , 'uses' => 'PackageController@newPackages']);

    Route::post('/paquete/{id}', ['as'=> 'updatePackage', 'uses' => 'PackageController@updatePackage']);

    Route::post('/paquete', ['as' => 'newPackage', 'uses' => 'PackageController@newPackage']);

    Route::post('/paquete/{id}/nuevopaquete', ['as' => 'saveWedding' , 'uses' => 'PackageController@saveWedding']);

    Route::post('/paquete/{id}/nuevocomponente', ['as' => 'newComponent', 'uses' => 'PackageController@newComponent']);

    Route::post('/paquete/componente/{id}', ['as' => 'updateComponent', 'uses' => 'PackageController@updateComponent']);


    /*Orders*/

    Route::get('/paquete/{id}/transacciones/buscar/{text}', ['as' => 'searchTransactions', 'uses' => 'OrderController@searchTransactions']);

    Route::get('/orden/{id}/transacciones/eliminar/{pid}', ['as' => 'deleteTransaction', 'uses' => 'OrderController@deleteTransaction']);

    Route::post('/orden/{id}/transacciones/{pid}/estado', ['as' => 'saveStatus', 'uses' => 'OrderController@saveStatus']);

	Route::get('/orden/{id}/transacciones/{pid}', ['as' => 'getComponentOrder', 'uses' => 'OrderController@getComponentOrder']);

    Route::get('paquete/{psid}/orden/{id}/transacciones/{pid}', ['as' => 'getComponentOrderSearch', 'uses' => 'OrderController@getComponentOrderSearch']);

    Route::get('/paquete/{id}/transacciones',['as' => 'getOrders', 'uses' => 'OrderController@getOrders']);

    Route::get('/transacciones', ['as' => 'transaccion', 'uses' => 'OrderController@hola' ]);



    /* Destination */

    Route::get('/destinos/buscar/{text}', ['as' => 'searchDestination', 'uses' => 'PackageController@searchDestination']);

    Route::get('/destinos', ['as' => 'destinations', 'uses' => 'PackageController@getDestinations']);

    Route::get('/destino/{id}', ['as' => 'destination', 'uses' => 'PackageController@getDestination']);

    Route::post('/destino/editar/{id}', ['as' => 'updateDestination', 'uses' => 'PackageController@updateDestination']);

    Route::post('/destinos/nuevo_destino', ['as' => 'newDestination', 'uses' => 'PackageController@newDestination']);
    
    Route::get('/destinos/eliminar/{id}', ['as' => 'deleteDestination', 'uses' => 'PackageController@deleteDestination']);



    /*PackageComponent*/

    Route::get('/paquete/componente', ['as'=> 'Components', 'uses' => 'PackageComponentController@getPackagesComponents']);

    Route::get('/paquete/componente/{id}', ['as'=> 'packageComponent', 'uses' => 'PackageComponentController@getPackageComponent']);

    Route::get('/paquete/componente/eliminar/{id}', ['as' => 'deletePackageComponent', 'uses' => 'PackageComponentController@deletePackageComponent']);


    Route::post('/paquete/componentes/{id}', ['as' => 'updatePackageComponent', 'uses' => 'PackageComponentController@updatePackageComponent']);




    Route::group(['middleware' => 'active'], function(){
        Route::get('/usuarios', ['as' => 'users', 'uses' => 'UserController@index']);
        Route::post('/usuario', ['as' => 'statusUser', 'uses' => 'UserController@changeStatus']);
        Route::get('/usuario/eliminar/{id}', ['as' => 'deleteUser', 'uses' => 'UserController@deleteUser']);
    });



});

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);
