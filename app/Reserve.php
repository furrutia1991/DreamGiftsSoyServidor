<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reserve extends Model
{
    protected $table = 'reserve';

    protected $primaryKey = 'reserve_id';
    public $timestamps = false;

    public function packagecomponent()
    {
    	return $this->hasMany('App\PackageComponent','reserve_id');
    }

    public function buyer()
    {
    	return $this->belongsTo('App\Buyer', 'buyer_id');
    }

}
