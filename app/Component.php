<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Component extends Model
{
    protected $table = 'component';

    protected $primaryKey = 'component_id';
    public $timestamps = false;

    public function packagecomponent()
    {
    	return $this->hasMany('App\PackageComponent', 'component_id');
    }
    public function transaction()
    {
        return $this->hasMany('App\Transaction', 'transaction_id');
    }
}
