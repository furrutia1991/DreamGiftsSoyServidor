<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transactions';
    protected $primaryKey = 'transaction_id';
    public $timestamps = false;

    public function order()
    {
        return $this->belongsTo('App\Order', 'order_id');
    }

    public function component()
    {
        return $this->belongsTo('App\Component', 'component_id');
    }


}
