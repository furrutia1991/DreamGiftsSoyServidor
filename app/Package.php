<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $table = 'package';
    protected $primaryKey = 'package_id';
    public $timestamps = false;

    public function customer()
    {
    	return $this->belongsTo('App\Customer', 'customer_id');
    }

   	public function destination()
   	{
   		return $this->belongsTo('App\Destination', 'destination_id');
   	}

    public function packagecomponent()
    {
      return $this->hasMany('App\PackageComponent', 'package_id');
    }

}
