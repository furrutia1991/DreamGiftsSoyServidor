<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
 	protected $table = 'payment';
    protected $primaryKey = 'payment_id';
    public $timestamps = false;

    public function metodopago()
    {
        return $this->belongsTo('App\PaymentMethod', 'payment_method_id');
    }

    public function paymentmethod()
    {
        return $this->belongsTo('App\PaymentMethod', 'payment_method_id');
    }

    public function component()
    {
        return $this->belongsTo('App\Component', 'component_id');
    }

    public function order()
    {
    	return $this->belongsTo('App\Order', 'order_id');
    }

}