<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    protected $primaryKey = 'order_id';
    public $timestamps = false;

   	public function buyer()
   	{
   		return $this->belongsTo('App\Buyer', 'buyer_id');
   	}

   	public function package()
   	{
   		return $this->belongsTo('App\Package', 'package_id');
   	}

   	public function reserve()
   	{
   		return $this->belongsTo('App\Reserve', 'reserve_id');	
   	}

	   public function payment()
   	{
   		return $this->belongsTo('App\Payment', 'payment_id');	
   	}   	

   	public function paymentmethod()
   	{
   		return $this->belongsTo('App\PaymentMethod', 'payment_method_id');	
   	}

   	public function component()
   	{
   		return $this->belongsTo('App\Component', 'component_id');	
   	}
	public function transaction()
	{
		return $this->hasMany('App\Transaction', 'transaction_id');
	}
}
