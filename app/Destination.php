<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Destination extends Model
{
	protected $table = 'destination';

	protected $primaryKey = 'destination_id';
	public $timestamps = false;

	public function package()
	{
		return $this->hasMany('App\Package', 'destination_id');
	}

	public function city()
	{
		return $this->belongsTo('App\City', 'city_id');
	}

}
