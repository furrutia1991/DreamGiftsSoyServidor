<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buyer extends Model
{
    protected $table = 'buyer';
    protected $primaryKey = 'buyer_id';
    public $timestamps = false;

    public function reserve()
    {
    	return $this->hasMany('App\Reserve', 'buyer_id');
    }

    public function payment()
    {
    	return $this->hasMany('App\Payment', 'buyer_id');
    }
}
