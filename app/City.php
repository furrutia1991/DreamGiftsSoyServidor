<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'city';

    protected $primaryKey = 'city_id';
    public $timestamps = false;

    public function country()
    {
    	return $this->belongsTo('App\Country', 'country_id');
    }

    public function destination()
    {
    	return $this->hasMany('App\Destination', 'city_id');
    }
    
}
