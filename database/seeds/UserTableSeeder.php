<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	factory(App\User::class)->create([
        	'name' => 'Francisco',
        	'password' => bcrypt('admin'),
            'email' => 'desarrollojr@soyservidor.com',
        	'role' => 'admin'

        ]);
        factory(App\User::class)->create([
            'name' => 'Jacobo',
            'email' => 'jacobo@soyservidor.com',
            'password' => bcrypt('admin'),
            'role' => 'admin'

        ]); 

    }
}
