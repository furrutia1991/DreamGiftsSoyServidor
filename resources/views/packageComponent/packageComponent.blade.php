@extends('layouts.auth')

@section('content')
	<div class="row header">
		<div class="col s12">
			<a href="{{route('package', $packageComponent->package_id)}}" class="waves-effect waves-light btn-large"><i class="material-icons right">settings_backup_restore</i>Atras</a>
		</div>
		
		
	</div>

	<form action="{{route('updateComponent', $packageComponent->component_id )}}" method="POST" enctype="multipart/form-data">
		{!! csrf_field() !!}
		<div class="row">
			<div class="col s12 l8 offset-l2">
				<div class="row">
					<div class="package-image col s12 l6">
						<label>Foto
							<img src="{{$packageComponent->component->picture}}" class="package-image" alt="package-image"><input type="file" name="picture">
							
						</label>
					</div>
					<div class="col s12 l6">
						<label>Descripcion:
							<input type="text" name="description" value="{{$packageComponent->component->description}}" required>
						</label>
						<!-- <div style="margin-top: 15%" class="col s12 l12 center">
							<a href="{{ route ('getComponentOrder', [$packageComponent->package_id, $packageComponent->component_id]) }}" class="waves-effect waves-light btn-large"><i class="material-icons right">markunread_mailbox</i>Ver Transacciones</a>
						</div> -->
					</div>
					
				</div>
				<div class="row">
					<div class="col s12 l6">
						<label>Nombre:
							<input type="text" name="tittle" value="{{$packageComponent->component->tittle}}" required>
						</label>
					</div>
					<div class="col s12 l6">
						<label>Fecha de creación:
							<input type="date" name="date_created" class="datepicker" value="{{$packageComponent->component->date_created}}" required>
						</label>
					</div>
				</div>
				<div class="row">
					<div class="col s12 l6">
						<label>Precio:
						<input type="number" name="amount"   value="{{$packageComponent->component->amount}}" required>
						</label>
					</div>
					<div class="col s12 l6">
						<label>Saldo:
							<input type="number" name="balance"   value="{{$packageComponent->component->balance}}" required>
						</label>
					</div>
				</div>
				<div class="row">
					<div class="col s12 l6">
						<label>Cantidad de abono de regalo:
							<input type="number" name="num_parts" value="{{$packageComponent->component->num_parts}}" required>
						</label>
					</div>
					<div class="col s12 l6">
						<label>Abonos pendientes:
							<input type="number" name="num_parts_balance" value="{{$packageComponent->component->num_parts_balance}}" required>
						</label>
					</div>
				</div>
				<div>
					<button type="submit" class="right btn-large waves-effect waves-light" name="action">Guardar<i class="material-icons right">send</i>
					</button>
				</div>
			</div>
		</div>
	</form>

@stop