@extends('layouts.auth')

@section('css')
	@parent
	<link rel="stylesheet" href="{{ asset('css/responsive-tables.css') }}">
	<link rel="stylesheet" href="{{ asset('css/jquery.dynatable.css') }}">
@stop
@section('content')
	<div class="row">
		<div class="col s12 l8 offset-l2">
			<table class="responsive-table" id="my-table">
				<thead>
					<tr>
						<th  width="100">Ver</th>
						<th  width="100">Código</th>
						<th  width="200">Boda</th>
						<th  width="200">Fecha</th>	
						<th  width="300">Destino</th>
						<th  width="200">Eliminar</th>
					</tr>
				</thead>
				<tbody class="packages-body">
					@foreach ($packageComponents as $element)
					<tr>
						<td class="show"><a href="#" class="waves-effect waves-light btn-large"><i class="material-icons right">search</i></a></td>
						<td class="code">{{$element->id}}</td>
						
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@stop