<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <title>Pagina no encontrada</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/normalize.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.1/css/materialize.min.css">
    
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container-404 {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content-404 {
                text-align: center;
                display: inline-block;
            }

            .title-404 {
                color: #dd0000;
                font-size: 72px;
                margin-bottom: 40px;
            }
        </style>
    </head>
    <body>
        <div class="navbar-fixed">
            <nav>
                <div class="nav-wrapper">
                  <a href="{{route('home')}}" class="brand-logo center"><img src="{{ asset('images/logo.png') }}" alt=""/></a>
                  <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                  <ul class="right hide-on-med-and-down">
                    <!-- <li><a href="#brands">Marcas</a></li>
                    <li><a href="#services">Servicios</a></li>
                    <li><a href="#contact">Contacto</a></li>
                    <li><a href="#motos">Motos</a></li>
                    <li><a href="#products">Productos</a></li> -->
                  </ul>
                  <ul class="side-nav" id="mobile-demo">
                    <!-- <li><a href="#brands">Marca</a></li>
                    <li><a href="#services">Servicios</a></li>
                    <li><a href="#contact">Contacto</a></li>
                    <li><a href="#motos">Motos</a></li>
                    <li><a href="#products">Productos</a></li> -->
                  </ul>
                </div>
              </nav>
        </div>
        <div class="container-404">
            <div class="content-404">
                <img style="width:30%" src="https://cdn0.iconfinder.com/data/icons/car-crash/500/bump-512.png" alt="">
                <div class="title-404">ups! pagina no encontrada</div>
            </div>
        </div>
        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <script src="{{ asset('js/modernizr.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.1/js/materialize.min.js"></script>
    </body>
</html>
