@extends('layouts.auth')


@section('content')
	
	<div class="users-content row">
		<div class="col s12 m10 l8 offset-l2 offset-m1 ">
			<table class="responsive-table">
			  <thead>
			    <tr>
			      	<th height="50" width="100">Nombre</th>
			      	<th height="50" width="100">Email</th>
			      	<th height="50" width="100">Fecha creacion</th>
					<th height="50" width="100">Estado</th>
			      	<th height="50" width="100">Eliminar</th>
			      	
			    </tr>
			  </thead>
			  <tbody class="providers-body">

			  	@foreach ($users as $element)
			  		<tr>
			  		  	<td>{{$element->name}}</td>
			  		  	<td>{{$element->email}}</td>
			  		  	<td>{{$element->created_at}}</td>
			  		  	<td id="{{$element->id}}"></td>
						<td><a href="{{ route('deleteUser', $element->id) }}"class="waves-effect waves-light btn-large"><i class="material-icons right">delete</i>Eliminar</a></td>
			  		</tr>
			  	@endforeach
			    
			  </tbody>
			</table>
		</div>
	</div>	
@stop
