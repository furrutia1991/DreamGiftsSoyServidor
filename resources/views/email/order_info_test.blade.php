<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Document</title>
</head>
<body>
<!-- Este es una template de prueba -->
<table border='1' cellspacing='0' bgcolor="#FFFFFF" cellspacing="0" cellpadding="10" border="0" width="650" style="border:1px solid #e0e0e0">
    <tr>
        <td valign="top">
            <a href="http://dreamgifts.ss.hn/" target="_blank">
                <div style="background:url(http://dreamgifts.ss.hn/wp-content/uploads/2015/12/slide_principal.jpg); no-repeat; background-size: cover; width: 100%; height: 170px;">
                    <img src="http://dreamgifts.ss.hn/wp-content/uploads/2015/12/logo-sticky-110x80.png">
                </div>
            </a>
            <!-- <a href="http://dreamgifts.ss.hn/" target="_blank"><img src="http://dreamgifts.ss.hn/wp-content/uploads/2015/12/slide_principal.jpg" alt="dreamgifts.hn" style="margin-bottom:10px; max-width: 100% " border="0" class="CToWUd"></a> -->
        </td>
    </tr>
    <tr>
        <td valign="top">
            <h1 style="font-size:22px;font-weight:normal;line-height:22px;margin:0 0 11px 0">Hola, </h1>
            <p style="font-size:12px;line-height:16px;margin:0">
                Gracias por hacer tu regalo en dreamgifts.hn Si tienes dudas puedes escribirnos a <a href="mailto:contacto@dreamgifts.hn" style="color:#1e7ec8" target="_blank">contacto@dreamgifts.hn</a>.</p>
            <p style="font-size:12px;line-height:16px;margin:0">A continuación te mostramos el detalle de tu Regalo.</p>
        </td>
    </tr>
    <tr>
        <td>
            <h2 style="font-size:18px;font-weight:normal;margin:0">Orden: # <small>(Fecha: )</small></h2>
        </td>
    </tr>
        
    <tr>
        <td>
            <table cellspacing="0" cellpadding="0" border="0" width="650">
            <thead>
            <tr>
                <th align="left" width="325" bgcolor="#EAEAEA" style="font-size:13px;padding:5px 9px 6px 9px;line-height:1em">Información de Cobro:</th>
                <th width="10"></th>
                <th align="left" width="325" bgcolor="#EAEAEA" style="font-size:13px;padding:5px 9px 6px 9px;line-height:1em">Método de Pago:</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td valign="top" style="font-size:12px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">
                   <table>
                       <thead>
                        <tr>
                            <th>Producto</th>
                            <th>Cantidad</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>

                </td>
                <td>&nbsp;</td>
                <td valign="top" style="font-size:12px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">
                    <p>Pago en agencia bancaria</p>
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                    <p align="center"><strong>INSTRUCCIONES PARA PAGO POR MEDIO DE AGENCIA BANCARIA</strong></p>
                                    <ol>
                                      <li><strong>Busca una sucursal de cualquiera de los bancos afiliados</strong> más cercana a tu localidad. </li>
                                      <li>
                                        <p>Por favor realiza tu pago de $:  a cualquiera de las siguientes números de cuentas: </p>
                                        <ul>
                                            <li><p>1.Banco Fichosa: 10900000012</p></li>
                                            <li><p>2.Banco Bac: 10900000012</p></li>
                                            <li><p>3.Banco Lafise: 20300000046</p></li>
                                        </ul>
                                      </li>
                                      <li><strong>Confirma nuestros números de cuenta </strong>para enviar dinero con los siguientes datos:
                                    <ul>
                                          <li><strong>Nombre (Beneficiario):</strong> DREAMGIFTS S.A. de C.V.</li>
                                          <li><strong>Cuidad:</strong> Tegucigalpa</li>
                                          <li><strong>País:</strong> Honduras</li>
                                          <li><strong>Cantidad a enviar:</strong> Es la cantidad que se refleja en tu carretilla. </li>
                                          <li><strong>Importante: </strong> Cualquier duda o reclamo en tu transaccion puedes escribirnos a <a href="mailto:contacto@dreamgifts.hn" style="color:#1e7ec8" target="_blank">contacto@dreamgifts.hn</a>. </li>
                                        </ul>
                                      </li>

                                      <li>Cuando tengas el Código o número de control de transferencias, nos escribes un correo a <a href="mailto:contacto@dreamgifts.hn" target="_blank">contacto@dreamgifts.hn"</a> donde nos envías el Código o número de control con los siguientes datos:
                                        <ul>
                                          <li>Numero de orden</li>
                                          <li>Tu nombre completo</li>
                                          <li>Nombre completo de quien realizo la transferencia</li>
                                          <li>Cuidad y País de donde envías</li>
                                          <li>Tu correo electrónico</li>
                                        </ul>
                                      </li>
                                    </ol>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
</table>
</td>
</tr>
    
    <tr>
        <td bgcolor="#EAEAEA" align="center" style="background:#eaeaea;text-align:center"><center>
          <p style="font-size:12px;margin:0">Muchas Gracias, <strong>DREAMGIFTS</strong></p></center></td>
    </tr>

</body>
</html>







                    
<!-- <table cellspacing="0" cellpadding="0" border="0" width="650" style="border:1px solid #eaeaea">
    <thead>
        <tr>
            <th align="left" bgcolor="#EAEAEA" style="font-size:13px;padding:3px 9px">Artículo</th>
            <th align="left" bgcolor="#EAEAEA" style="font-size:13px;padding:3px 9px">Código</th>
            <th align="center" bgcolor="#EAEAEA" style="font-size:13px;padding:3px 9px">Cantidad</th>
            <th align="right" bgcolor="#EAEAEA" style="font-size:13px;padding:3px 9px">Subtotal</th>
        </tr>
    </thead>

            <tbody bgcolor="#F6F6F6">
        <tr>
    <td align="left" valign="top" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc">
        <strong style="font-size:11px">"CAMISA NEGRA" POLO UNIFORME - Unisex</strong>
                <dl style="margin:0;padding:0">
                        <dt><strong><em>Talla</em></strong></dt>
            <dd style="margin:0;padding:0 0 0 9px">
                S            </dd>
                    </dl>
                                                        </td>
    <td align="left" valign="top" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc">508956-07-camisa-negra-polo-<wbr>unisex-s</td>
    <td align="center" valign="top" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc">2</td>
    <td align="right" valign="top" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc">
                                                <span>L&nbsp;1,618.00</span>            

                    

            </td>
</tr>
    </tbody>
                <tbody>
        <tr>
    <td align="left" valign="top" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc">
        <strong style="font-size:11px">"CAMISA <span class="il">BLANCA</span>" POLO UNIFORME - Unisex</strong>
                <dl style="margin:0;padding:0">
                        <dt><strong><em>Talla </em></strong></dt>
            <dd style="margin:0;padding:0 0 0 9px">
                S            </dd>
                    </dl>
                                                        </td>
    <td align="left" valign="top" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc">568241-02-camisa-<span class="il">blanca</span>-polo-<wbr>unisex-s</td>
    <td align="center" valign="top" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc">1</td>
    <td align="right" valign="top" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc">
                                                <span>L&nbsp;1,019.00</span>            

                    

            </td>
</tr>
    </tbody>
        
    <tbody>
                <tr>
        <td colspan="3" align="right" style="padding:3px 9px">
                        Subtotal                    </td>
        <td align="right" style="padding:3px 9px">
                        <span>L&nbsp;2,637.00</span>                    </td>
    </tr>
            <tr>
        <td colspan="3" align="right" style="padding:3px 9px">
                        Manipulación y Envío                    </td>
        <td align="right" style="padding:3px 9px">
                        <span>L&nbsp;79.64</span>                    </td>
    </tr>
            <tr>
        <td colspan="3" align="right" style="padding:3px 9px">
                        <strong>Importe total</strong>
                    </td>
        <td align="right" style="padding:3px 9px">
                        <strong><span>L&nbsp;2,716.64</span></strong>
                    </td>
    </tr>
        </tbody>
</table> -->