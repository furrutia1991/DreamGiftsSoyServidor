@extends('layouts.auth')

@section('css')
	@parent
	<link rel="stylesheet" href="{{ asset('css/responsive-tables.css') }}">
	<link rel="stylesheet" href="{{ asset('css/jquery.dynatable.css') }}">
@stop

@section('content')
	<div class="row header">
		<div class="col  s12">
			 <a href="{{route('customer', $customer->customer_id)}}" class="left waves-effect waves-light btn-large"><i class="material-icons right">settings_backup_restore</i>Atras</a>
		</div>
	</div>
	<div class="tittle">
		<h4 style="text-align: center">Nuevo Paquete</h4>
	</div>
	<br>
	<div class="row">
		<div class="col s12 l8 offset-l2">
			   
			<form id="frm-create" action="{{ route('saveWedding', $customer->customer_id)}}"  method="POST" enctype="multipart/form-data">
				{!! csrf_field() !!}
				<div class="row">
					<div class="col s12 l12">
						<div class="row">
							<div class="col s12 l6">
								<img src="" class="package-image" alt="Foto de boda"><input type="file" name="picture" required>
							</div>
							<div class="col s12 l6">
								<div class="input-field col s12">
								  <textarea name="message" id="textarea1" class="materialize-textarea" required></textarea>
								  <label for="textarea1">Mensaje</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col s12 l6">
								<label>Nombre de Boda:</label>
									<input type="text" name="name" value="{{$customer->name}}" required>
							</div>
							<div class="col s12 l6">
								<label>Precio del paquete:</label>
									<input type="number" name="amount" value="" required>
							</div>
						</div>
						<div class="row">
							<div class="col s12 l6">
								<label>Fecha de boda:</label>
									<input id="wedding_date" type="date" name="date_event" class="datepicker wedding_date"  value="" required>
							</div>
							<div class="col s12 l6">
								<label>Fecha de cierre del paquete:</label>
									<input id="close_date" type="date" name="date_finalized" class="datepicker"  value="" required>
							</div>
						</div>
						<div class="row">
							<!-- <div class="col s12 l6">
								<label>Fecha de creación del paquete:
									<input type="date" name="close_dated" class="datepicker"  value="">
								</label> 
							</div> -->
							<div class="col s12 l6 ">
									<select name="destination">
										<option value="" disabled selected>Elige un destino</option>
									@foreach($destination as $element)
										<option value="{{$element->destination_id}}" required>{{$element->name}}</option>
									@endforeach
									</select>
								<label>Destino del paquete:</label>
								<div class="chip">
									<a class="modal-trigger" href="#modal1">Crear un nuevo destino</a>
								 </div>
							</div>

						</div>
						<div>
							<button id="btn-create" type="button" class="right btn-large waves-effect waves-light" name="action">Guardar<i class="material-icons right">send</i>
							</button>
						</div>
					</div>
				</div>
			</form>
					{{-- Destination Modal --}}
					<div id="modal1" class="modal" style="padding: 3em">
					    <div class="modal-content">
					    	<h4 style="text-align: center">Nuevo Destino</h4>
								<form action="{{ route('newDestination')}}" method="POST" enctype="multipart/form-data">
									{!! csrf_field() !!}

									<div class="row">
										<div class="col s12 l6">
											<label>
												Nombre:
											</label>
												<input type="text" name="name" required>
										</div>
										<div class="col s12 l6">
											<label>
												Descripción:
											</label>
												<input type="text" name="description" required>
										</div>
									</div>
									<div class="row">
										<div class="col s12 l6">
											<label>
												No. Telefono:
											</label>
												<input type="number" name="phone" cols="10" rows="2"></input>
										</div>
										<div class="input-field col s12 l6">
										    <select name="city" required>
										      <option value="" disabled selected>Elige una ciudad</option>
										      	@foreach($city as $element)
													<option value="{{$element->city_id}}">{{$element->name}}</option>
												@endforeach
										    </select>
										    <label>Ciudad:</label>
										</div>
										
									</div>
									<div class="row">
										<div class="col s12 l6">
											<label>
												Código Postal:
											</label>
												<input type="number" name="postal_code" cols="10" rows="2"></input>
										</div>
									</div>
									<button class="left btn waves-effect waves-light modal-action modal-close waves-green">Cerrar<i class="material-icons right">cancel</i>
									</button>
									<button class="right btn waves-effect waves-light" type="submit" name="action">Guardar<i class="material-icons right">send</i>
						  			</button>
								</form>	    		
					    </div>
					</div>
		</div>
	</div>
@stop

@section('script')
	@parent
	<script>

		jQuery(document).ready(function($) {
			/*Validaciones formulario de creacicion de contratos*/
				$("#btn-create").click(function () {
					var close_date = toDate($("#close_date").val());
					var wedding_date = toDate($("#wedding_date").val());
					// alert("fecha boda: "+$("#wedding_date").val());
					// alert("fecha cierre paquete: " + $("#close_date").val() );

					if ($("#wedding_date").val() != "" && $("#close_date").val() != "") {

						if (wedding_date < close_date) {
							// console.log("Fecha de boda menor que fecha de cierre");
							var notice_order = $('<span>La fecha de cierre del paquete ' + " " + $("#close_date").val() + ' no puede ser mayor o igual a la fecha de la boda' + " " + $("#wedding_date").val() + ' </span>');
						 	Materialize.toast(notice_order, 9000);					
						}
						else
						{
							$('#frm-create').submit();
						}
					}else{
						var notice_order_ = $('<span>Debe espicificar las fechas antes de guardar paquete </span>');
						 	Materialize.toast(notice_order_, 9000);
					}
				});
			});

			function toDate(dateStr) {
			var parts = dateStr.split(" ");
				switch(parts[1])
				{
					case "January,": parts[1] = 1;
					break;

					case "February,": parts[1] = 2;
					break;

					case "March,": parts[1] = 3;
					break;

					case "April,": parts[1] = 4;
					break;

					case "May,": parts[1] = 5;
					break;

					case "June,": parts[1] = 6;
					break;

					case "July,": parts[1] = 7;
					break;

					case "August,": parts[1] = 8;
					break;

					case "September,": parts[1] = 9;
					break;

					case "October,": parts[1] = 10;
					break;

					case "November,": parts[1] = 11;
					break;

					case "December,": parts[1] = 12;
					break;

				}
				return new Date(parts[2], parts[1] - 1, parts[0]);
				// console.log(parts[2]);
				// console.log(parts[1]);
				// console.log(parts[0]);
			}


	</script>


@stop