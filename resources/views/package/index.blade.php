	@extends('layouts.auth')

@section('css')
	@parent
	<link rel="stylesheet" href="{{ asset('css/responsive-tables.css') }}">
	<link rel="stylesheet" href="{{ asset('css/jquery.dynatable.css') }}">
@stop

@section('content')
	<div class="row header">
		<div class="col l12 m8 offset-m2 s12">
			 <a href="{{route ('home')}}" class="left waves-effect waves-light btn-large"><i class="material-icons right">settings_backup_restore</i>Atras</a>
			 <a href="{{route ('newCustomerPackage')}}" class="right waves-effect w	aves-light btn-large"><i class="material-icons right">playlist_add</i>Nuevo Paquete</a>
		</div>
	</div>
	<div class="tittle">
		<h2 style="text-align: center">Paquetes</h2>
	</div>
	<div class="row">
		<div class="col l8 offset-l2 s12">
			<label>Buscar paquete</label>
			<input type="text" class="search-packages" placeholder="Buscar paquete por código, nombre de boda, nombre de destino..." autofocus>
		</div>
		<div >
			<a href="javascript:location.reload()" class="refresh-btn left btn waves-effect waves-light">Ver todos<i class="material-icons right">perm_identity</i>
		  	</a>
		</div>
	</div>
	<div class="row">
		<div class="col s12 l10 offset-l1">
			<table class="responsive-table" id="my-table">
				<thead>
					<tr>
						<th  width="50" height="50">Ver</th>
						<th  width="100" height="50">Código</th>
						<th  width="100" height="50">Boda</th>
						<th  width="100" height="50">Fecha</th>
						<th  width="100" height="50">Destino</th>
						<th class="Jdelete" width="100" height="50">Eliminar</th>
					</tr>
				</thead>
				<tbody class="packages-body">
					@foreach ($package as $element)
					<tr>
						<td class="show"><a href="{{route('package', $element->package_id) }}" class="waves-effect waves-light btn-large"><i class="material-icons right">search</i></a></td>
						<td class="code">{{$element->package_id}}</td>
						<td class="name">{{$element->customer->name}}</td>
						<td class="wedding">{{$element->date_event}}</td>
						<td class="Comentario">{{$element->destination->name}}</td>
						<td class="delete"><a href="#modal-delete-component_{{$element->package_id}}" class="modal-trigger waves-effect waves-light btn-large"><i class="material-icons right">delete</i>Eliminar</a></td>
					</tr>
						{{-- Delete Modal --}}
						<div id="modal-delete-component_{{$element->package_id}}" class="modal">
					        <div class="modal-content">
					            <div class="row">
					                <div class="col l8 offset-l2">
					                    <h4 style="text-align: center;">Elminar Paquete</h4>
					                    <p style="text-align: center;">
					                    	¿Esta seguro que desea eliminar el paquete de {{$element->customer->name}}?
					                    	
					                    </p>
					                </div>
					            </div>
					            <br>
					            <div class="row">
					                <div class="col l10 offset-l2">
					                    <a   href="{{route('deletePackage', $element->package_id) }}" class="col l4 btn waves-effect" name="action">
					                        Confirmar
					                        <i class="material-icons right"><i class="mdi-toggle-check-box"></i></i>
					                    </a>
					                    <button class="modal-close col l4 offset-l1 btn waves-effect" type="submit" id="decline-delete-component"
					                            name="action">
					                        No
					                        <i class="material-icons right"><i class="mdi-action-highlight-remove"></i></i>
					                    </button>
					                </div>
					            </div>

					        </div>
					        <div class="modal-footer">
					            <a class="modal-action modal-close waves-effect waves-green btn-flat tooltipped" id="out-modal"
					               data-position="top" data-delay="50" data-tooltip="Salir de esta pantalla">Salir</a>
					        </div>
					    </div>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

<div class="row paginate">
	<div class="col s12 l4 offset-l4">

		{!! $package->render() !!}
	</div>
</div>	
@stop
@section('script')
	@parent
	<script>
		jQuery(document).ready(function($) {
		$('.search-packages').keyup(function(event) {
		    if(event.keyCode == 13){
		        var text = $(this).val() ? $(this).val() : 'blank';

		        $.get('paquetes/buscar/' + text, function(data) {

		            if (data) {
		                
		                if(data.redirect)
		                {
		                    window.location.href = data.redirect;
		                }
		                $('.Jdelete').css("display", "none");
		                $('.paginate').css("display", "none");
		                $('.packages-body').html('');
		                $('.refresh-btn').css("display", "block");
		                
		                $.each(data, function(index, val) {

		                    var markup = "<tr>" +
		                        "<td width='100'><a class='waves-effect waves-light btn-large' href='paquete/" + val.package_id + "'>" + "<i class='material-icons right'>search</i></a></td>" +
		                        "<td width='100'>" + val.package_id + "</td>" +
		                        "<td width='100'>" + val.customer_name + "</td>" +
		                        "<td width='100'>" + val.date_event + "</td>" +
		                        "<td width='100'>" + val.destination_name + "</td>" +
		                    "</tr>";
		                    $('.packages-body').append(markup);
		                })
		            }

		        })
		    }
			    
			});
		});
		
	</script>
@stop


