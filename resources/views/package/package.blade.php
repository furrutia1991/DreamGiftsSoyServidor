@extends('layouts.auth')

@section('content')
	<div class="row header">
		<div class="col s12">
			<a href="{{route ('packages')}}" class="waves-effect waves-light btn-large"><i class="material-icons right">settings_backup_restore</i>Atras</a>
			
			<a class="right modal-trigger waves-effect waves-light btn-large" href="#modal1"><i class="material-icons right">library_add</i>Agregar Componente</a>
		</div>
	</div>

	<form action="{{ route('updatePackage', $package->package_id) }}"  method="POST" enctype="multipart/form-data">
		{!! csrf_field() !!}
		<div class="tittle">
			<h3 style="text-align: center">Boda: {{$package->customer->name}}</h3>
			<h2 style="text-align: center">{{$package->date_event}}</h2>
		</div>

		<br>
	
		<div class="row">
			<div class="col s12 l8 offset-l2">
				<div class="row">
					<div class="package-image col s12 l6">
						<label>Foto
						</label>
							<img src="{{$package->picture}}" class="package-image" alt="package-image"><input type="file" name="picture">
					</div>
					<div class="col s12 l6">
						<div class="input-field col s12">
						  <textarea name="message" id="textarea1" class="materialize-textarea" required>{{$package->message}}</textarea>
						  <label for="textarea1">Mensaje</label>
						</div>
						
						<div style="margin-top: 15%" class="col s12 l12 center">
							<a href="{{ route ('getOrders', $package->package_id ) }}" class="waves-effect waves-light btn-large"><i class="material-icons right">markunread_mailbox</i>Ver Transacciones</a>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col s12 l6">
						<label>Nombre de Boda:
						</label>
							<input type="text" name="name" value="{{$package->customer->name}}" required>
					</div>
					<div class="col s12 l6">
						<label>Destino del paquete:
						</label>
							<input type="text" name="destination" value="{{$package->destination->name}}" required>
					</div>
				</div>
				<div class="row">
					<div class="col s12 l6">
						<label>Fecha de boda:
						</label>
						<input type="date" name="date_event" class="datepicker"  value="{{$package->date_event}}" required>
					</div>
					<div class="col s12 l6">
						<label>Fecha de cierre del paquete:
						</label>
							<input type="date" name="date_finalized" class="datepicker"  value="{{$package->date_finalized}}" required>
					</div>
				</div>
				<div class="row">
					<div class="col s12 l6">
						<label>Precio del paquete: 
						</label>
							<input type="number" name="amount" value="{{$package->amount}}" required>
					</div>
					<div class="col s12 l6">
						<label>URL del paquete:
						</label>
						<input  type="text" name="url" value="{{url()}}/boda/{{$package->package_id}}">
						<a href="{{url()}}/boda/{{$package->package_id}}" >Ir al paquete</a>
					</div>
				</div>
				<div>
					<button href="#modal-change-package" class="modal-trigger right btn-large waves-effect waves-light" name="action">Guardar<i class="material-icons right">send</i>
					</button>
				</div>
			</div>
		</div>

		{{-- Confirm Modal --}}
		<div id="modal-change-package" class="modal">
	        <div class="modal-content">
	            <div class="row">
	                <div class="col l8 offset-l2">
	                    <h4>Cambio de paquete</h4>
	                    <p style="text-align: center;">
	                    	¿Esta seguro que desea modificar este paquete?
	                    </p>
	                </div>
	            </div>
	            <br>
	            <div class="row">
	                <div class="col l10 offset-l2">
	                    <button class="col l4 btn waves-effect" type="submit" id="confirm-change-package"
	                            name="action">
	                        Confirmar
	                        <i class="material-icons right"><i class="mdi-toggle-check-box"></i></i>
	                    </button>
	                    <button class="col l4 offset-l1 btn waves-effect">
	                        No
	                        <i class="material-icons right" id="out-modal"><i class="mdi-action-highlight-remove"></i></i>
	                    </button>
	                </div>
	            </div>

	        </div>
	        <div class="modal-footer">
	            <a class="modal-action modal-close waves-effect waves-green btn-flat tooltipped" id="out-modal"
	               data-position="top" data-delay="50" data-tooltip="Salir de esta pantalla">Salir</a>
	        </div>
	    </div>

	</form>

	<div class="divider"></div>
	<div class="tittle">
			<h2 style="text-align: center">Regalos de luna de miel: {{$package->customer->name}}</h2>
	</div>
	<div class="row">
		<div class="col l8 offset-l2 s12">
			<table class="responsive-table" id="packagetable">
				<thead>
					<tr>
						<th width="100" height="65">Ver</th>
						<th width="100" height="65">Nombre</th>
						<th width="100" height="65">Descipción</th>
						<th width="100" height="65">Precio</th>
						<th width="100" height="65">Eliminar</th>
					</tr>
				</thead>
				<tbody class="components-body">
					@if($packageComponent)
						@foreach ($packageComponent as $element)
						<tr style="max-width: 15em">
							<td class="show"><a href="{{route('packageComponent', $element->id) }}" class="waves-effect waves-light btn-large"><i class="material-icons right">search</i></a></td>
							<td class="name">{{$element->component->tittle}}</td>
							<td class="cut description">{{$element->component->description}}</td>
							<td class="amount">$: {{$element->component->amount}}</td>
							<td class="delete"><a id="{{$element->id}}" href="#modal-delete-component_{{$element->id}}" class="waves-effect waves-light btn-large modal-trigger"><i class="material-icons right">delete</i>Eliminar</a></td>
						</tr>
							{{-- Delete Modal --}}
								<div id="modal-delete-component_{{$element->id}}" class="modal">
							        <div class="modal-content">
							            <div class="row">
							                <div class="col l8 offset-l2">
							                    <h4 style="text-align: center;">Elminar Componente</h4>
							                    <p style="text-align: center;">
							                    	¿Esta seguro que desea eliminar el componente {{$element->component->tittle}}?
							                    	
							                    </p>
							                </div>
							            </div>
							            <br>
							            <div class="row">
							                <div class="col l10 offset-l2">
							                    <a   href="{{route('deletePackageComponent', $element->id) }}" class="col l4 btn waves-effect" name="action">
							                        Confirmar
							                        <i class="material-icons right"><i class="mdi-toggle-check-box"></i></i>
							                    </a>
							                    <button class="modal-close col l4 offset-l1 btn waves-effect" type="submit" id="decline-delete-component"
							                            name="action">
							                        No
							                        <i class="material-icons right"><i class="mdi-action-highlight-remove"></i></i>
							                    </button>
							                </div>
							            </div>

							        </div>
							        <div class="modal-footer">
							            <a class="modal-action modal-close waves-effect waves-green btn-flat tooltipped" id="out-modal"
							               data-position="top" data-delay="50" data-tooltip="Salir de esta pantalla">Salir</a>
							        </div>
							    </div>
						@endforeach
					@else
						<h2>No hay transacciones para este paquete aun.</h2>
					@endif
				</tbody>
			</table>
		</div>
	</div>

	{{-- Component Modal --}}

	<div id="modal1" class="modal" style="padding: 3em">
	    <div class="modal-content">
	    	<h4 style="text-align: center">Nuevo Componente</h4>
				<form action="{{route('newComponent', $package->package_id )}}" method="POST" enctype="multipart/form-data">
					{!! csrf_field() !!}
					<div class="row">
						<div class="col s12 l6">
							<label>
							</label>
								<img src="" class="component-image" alt="Foto de componente"><input type="file" name="picture" required>
						</div>
					</div>
					<div class="row">
						<div class="col s12 l6">
							<label>Nombre:
							</label>
								<input type="text" name="tittle" required>
						</div>
						<div class="col s12 l6">
							<label>Descipción:
							</label>
								<input type="text" name="description" required>
						</div>
					</div>
					<div class="row">
						<div class="col s12 l6">
							<label>Precio:
								<input type="text" id="price" onkeypress="return /\d/.test(String.fromCharCode(((event||window.event).which||(event||window.event).which)));" name="total_amount" >
							</label>
						</div>
						<div class="col s12 l6">
							<label>Cantidad de abonos para el regalo:	
							</label>
								<input type="text" id="quantity" onkeypress="return /\d/.test(String.fromCharCode(((event||window.event).which||(event||window.event).which)));" name="num_parts" >
						</div>
					</div>
					<div class="row">
						<div class="col s12 l6">
							<label>Fecha de ultima actualización:
							</label>
								<input type="date" name="date_updated" class="datepicker"  value="<?php echo date("d/m/Y"); ?>" required>
						</div>
						<div class="col s12 l6">
							<label>Precio por regalo:
								 <p><h4 id="total"></h4></p>
							</label>
						</div>
					</div>
						
					<div class="row">
						<div style="display: none;" class="col s12 l6">
							<label>Fecha de creación del paquete:
							</label>
								<input type="date" name="date_created" class="datepicker"  value="<?php echo date("d/m/Y"); ?>" required>
						</div>
					</div>
					<button class="left btn waves-effect waves-light modal-action modal-close waves-green">Cerrar<i class="material-icons right">cancel</i>
					</button>
					<button class="right btn waves-effect waves-light" type="submit" name="action">Guardar<i class="material-icons right">send</i>
		  			</button>
				</form>	    		
	    </div>
	</div>
@stop

@section('script')
	@parent
	<script>
		jQuery(document).ready(function($) {
			$('#quantity').keyup(function(event) {
				if($('#price').val().length>0){
						var total = $('#price').val()/$('#quantity').val();
						if(total)
						{	
							if ($('#quantity').val().length>0) {
								//alert(total);
								$('#total').text("Total: " + "$" + total.toFixed(2));
							}
						}
						else
						{
							$('#total').text("Total: " + "$ 0.00");
						}
					}
				});
			$("#confirm-change-package").click(function () {
		            jQuery("#order_state").submit();
		        });
			});
	</script>
@stop

