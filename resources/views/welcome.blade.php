<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Luna de Miel</title>
    @section('css')
    <link rel="stylesheet" href="{{ asset('css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('css/normalize.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/materialize.css')}}">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.1/css/materialize.min.css">
    <link href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{{ asset('images/favicon.png') }}}">
    @show

</head>
<body>

@section('header')
<div class=" navbar-fixed">
    <nav>
        <div class=" navbar-fixed">
            <nav>
                <div class="nav-wrapper">
                  <a href="http://dreamgifts.ss.hn/" class="brand-logo center"><img src="{{ asset('images/logo.png')}}" alt=""/></a>
                  <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                  <ul class="right hide-on-med-and-down">
                    <li><a href="#package">Regalos</a></li>
                    <li><a href="#cart">Carrito</a></li>
                    <li><a href="#checkout">Pago</a></li>
                  </ul>
                  <ul class="side-nav" id="mobile-demo">
                    <li><a href="#package">Regalos</a></li>
                    <li><a href="#cart">Carrito</a></li>
                    <li><a href="#checkout">Pago</a></li>
                  </ul>
                </div>
            </nav>

        </div>
    </nav>
</div>
@show


<div class="tittle">
    <h3 style="text-align: center">{{$package->customer->name}}</h3>

    <h2 style="text-align: center">{{$package->date_event}}</h2>
</div>
<br>

<div class="row">
    <div class="col s12 l9 offset-l2">
        <div class="row">
            <div class="col s12 m8 l6 offset-m2">
                <label>
                    <img src="{{$package->picture}}" class="package-image" alt="package-image">
                </label>
            </div>
            <div class="col s12 m8 l6 offset-m2">
                <div class="message">
                    <p>{{$package->message}}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@show


@section('content')
<?php $cont_cpmponents = 5; //este numero representa la cantidad de componentes que posee el paquete?>
<input type="hidden" name="name_<?php echo "component_1" ?>"
       id="id_components"
       value="<?php echo $cont_cpmponents; ?>"/>
<input type="hidden" name="name_id_package"
       id="id_package"
       value="{{$package->package_id}}"/>

<div class="row">
    <div class="col s12 m10 l8 offset-m1 offset-l2">
        <div id="package" class="section scrollspy container_package ">
            <h1 style="text-align: center; font-size: 50px;"><strong><span style="color: #e89a98;">NUESTROS REGALOS</span></strong></h1>
            <!-- <h1 style="text-align: center;"><span style="color: #5d5d5d; font-weight: 300;">NUESTROS REGALOS</span></h1> -->
            <ul class="collection z-depth-3 ">
                <?php $count = 0; ?>
                @foreach ($packageComponent as $element)
                <?php $count++; ?>
                <li class="collection-item avatar">
                    <div class="row general-img">
                        <div class="col l3 s12 image-component">
                            <img src="{{$element->component->picture}}" alt="" class="responsive-img redimension-img">
                        </div>
                        <div class="col l4 s12 description-img">
                            <h5>{{$element->component->tittle}}</h5>

                            <p>{{$element->component->description}}</p>
                        </div>
                        @if($element->component->balance > 0)
                        <div class="col l3 s10 offset-s1  cost-componnet">
                            <h6>Costo Regalo: ${{$element->component->amount}}</h6>
                            <h5><span id="id_result_<?php echo $count ?>"></span></h5>
                        </div>
                        @endif
                        <div class="col l2 s10 offset-s1  secondary-content">
                            @if($element->component->balance <= 0)
                            <div>
                                <h6><i class="material-icons"><i class="mdi-action-check-circle"></i></i>Regalo completado</h6>
                            </div>
                            @else
                            <label>Cantidad de regalos</label>
                            <select class="browser-default counter" id="sl_component_<?php echo $count ?>"
                                    onchange="calculate(this.id)">
                                <option value="" disabled selected>obsequiar</option>
                                <?php for ($i = 0; $i <= $element->component->num_parts_balance; $i++): ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php endfor; ?>
                            </select>
                            @endif
                        </div>
                        
                    </div>
                    <input type="hidden" name="id_source_<?php echo $count ?>"
                           id="id_component_source_<?php echo $count ?>"
                           value="<?php echo $element->component->component_id ?>"/>

                    <input type="hidden" name="name_component_<?php echo $count ?>"
                           id="id_component_value_<?php echo $count ?>"
                           value="<?php echo $element->component->amount ?>"/>
                    <input type="hidden" name="name_component_<?php echo $count ?>"
                           id="id_component_general_name_<?php echo $count ?>"
                           value="<?php echo $element->component->tittle ?>"/>
                    <input type="hidden" name="name_component_<?php echo $count ?>"
                           id="id_component_general_descrption_<?php echo $count ?>"
                           value="<?php echo $element->component->description ?>"/>
                    <input type="hidden" name="name_<?php echo "component__result_1" ?>"
                           id="id_component_result_<?php echo $count ?>"/>
                    
                </li>
                @endforeach
            </ul>
        </div>
        <div id="cart" class="section scrollspy container_cart">
        <h1 style="font-size: 50px;"><strong><span style="color: #e89a98;">CARRITO</span></strong></h1>
            <div class="row">
                <div>
                    <table class="responsive-table" id="table_cart">
                        <thead>
                        <tr>
                            <th data-field="id" colspan="3">Detalle regalo</th>
                            <th data-field="item-price" class="right">Regalos</th>
                            <th data-field="total-profit" class="center">Precio</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    <div class="divider"></div>
                    <table class="responsive-table right" id="table_cart_subtotal">
                        <tbody>
                        <tr>
                            <th class="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Subtotal</th>
                            <td class="right"></td>
                            <td class="value_total" id="col_subtotal">$ 0.00</td>
                        </tr>
                        <tr>
                            <th class="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total</th>
                            <td class="right"></td>
                            <td class="value_total" id="col_total">$ 0.00</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col s10 offset-s1 m10 l3">
                    <a class="waves-effect personal  btn-large" id="go_make_process_pay" href="#checkout">Ir a
                        pago<i class="material-icons right"><i class="mdi-action-payment"></i></i></a>
                </div>
                <div class="col s10 offset-s1 m10 l3">
                    <a class="waves-effect personal  btn-large" id="return_edit_package" href="#package">Editar
                        Paquete<i class="material-icons right"><i class="mdi-editor-mode-edit"></i></i></a>
                </div>
            </div>
        </div>

        <div id="checkout" class="section scrollspy container_checkout">
            <div class="data-personal-buyer">
                <h5>Datos personales</h5>

                <div class="divider"></div>
                <br/>

                <form class="container_form_data">
                    <div class="row personal1">
                        <div class="input-field col s12 l6">
                            <i class="material-icons prefix"><i class="mdi-action-account-circle"></i></i>
                            <input required id="icon_name" type="text" class="validate" required>
                            <label for="icon_name">Nombre</label>
                        </div>
                        <div class="input-field col s12 l6">
                            <i class="material-icons prefix"><i class="mdi-action-account-circle"></i></i>
                            <input required id="icon_lastname" type="text" class="validate" required>
                            <label for="icon_lastname">Apellido</label>
                        </div>
                    </div>
                    <div class="row personl2">
                        <div class="input-field col s12 l6">
                            <i class="material-icons prefix"><i class="mdi-communication-call"></i></i>
                            <input required id="icon_telephone" type="tel" onkeypress="return /\d/.test(String.fromCharCode(((event||window.event).which||(event||window.event).which)));" class="validate" required>
                            <label for="icon_telephone">Tel&eacute;fono</label>
                        </div>
                        <div class="input-field col s12 l6">
                            <i class="material-icons prefix"><i class="mdi-communication-email"></i></i>
                            <input required id="icon_email" type="text" pattern="^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$" class="validate" required>
                            <label for="icon_email">Correo electr&oacute;nico</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s10 m10 l3">
                            <a class="waves-effect personal  btn-large group-form-buyer" id="show-go_make_pay">
                                continuar<i class="material-icons right"><i class="mdi-action-payment"></i></i></a>
                        </div>
                    </div>
                </form>
            </div>
            <h1 style="font-size: 50px;"><strong><span style="color: #e89a98;">ELIGE UN METODO DE PAGO</span></strong></h1>
            <!-- <h3>Pago</h3> -->
            <br/>

            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="col s12 l12">
                        <p>
                            <input name="group1" type="radio" id="mp-agency-pay"/>
                            <label for="mp-agency-pay">Pago en agencia bancaria</label>
                        </p>
                        <div class="pay-methode" id="pay-agency-pay">
                            <h5>Deposito en banco o transferencia electronica</h5>

                            <div class="divider"></div>
                            <br/>
                            <div class="row">
                                <p class="left">Realiza tu pago en una agencia bancaria. Te enviaremos las instrucciones por email.</p>
                                <div class="col s10 m10 l3 right">
                                    <a class="waves-effect personal  btn-large group-go_make_pay" id="go_make_pay_tc">
                                        Continuar<i class="material-icons right"><i class="mdi-action-payment"></i></i></a>
                                </div>
                            </div>
                        </div>
                        <p>
                            <input name="group1" type="radio" id="mp-credit-card"/>
                            <label for="mp-credit-card">Tarjeta de cr&eacute;dito y/o Débito</label>
                        </p>
                        {{-- AGREGUE EL FORMULARIO PARA HACER LA PETICION A LA API --}}
                        <form id="api-form" action="https://credomatic.compassmerchantsolutions.com/api/transact.php" method="POST">
                        {{-- AGREGUE EL CAMPO REDIRECT --}}
                        <input type="hidden" name="redirect" value="http://166.63.35.26/DreamGiftsSoyServidor/public/callback">
                        {{-- AGREGUE EL CAMPO TYPE --}}
                        <input type="hidden" name="type" value="auth">
                        <div class="pay-methode" id="pay-credit-card">
                            <h5>Datos de tarjeta de cr&eacute;dito y/o Débito</h5>

                            <div class="divider"></div>
                            <br/>

                            <div class="container_1">
                                <form class="z-depth-1 grey lighten-4 container_child">
                                    <div class="row">
                                        <div class="input-field">
                                            <i class="material-icons prefix"><i class="mdi-action-account-circle"></i></i>
                                            {{-- AGREGUE EL NAME CARDNAME --}}
                                            <input id="icon_name_credit_card" name="cardName" type="text" class="validate">
                                            <label for="icon_name_credit_card">Nombre que aparece en tarjeta</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field">
                                            <i class="material-icons prefix"><i class="mdi-action-payment"></i></i>
                                            {{-- AGREGUE EL NAME CREDITCARD --}}
                                            <input id="icon_credit_card" name="cc_number" type="text" class="validate">
                                            <label for="icon_credit_card">N&uacute;mero tarjeta</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s12 m12 l4">

                                            <label>Mes de expiraci&oacute;n</label>
                                            <select class="browser-default" id="id_sl_month" name="id_sl_month">
                                                <option value="" disabled selected>Mes</option>
                                                <option value="01">Enero</option>
                                                <option value="02">Febrero</option>
                                                <option value="03">Marzo</option>
                                                <option value="04">Abril</option>
                                                <option value="05">Mayo</option>
                                                <option value="06">Junio</option>
                                                <option value="07">Julio</option>
                                                <option value="08">Agosto</option>
                                                <option value="09">Septiembre</option>
                                                <option value="10">Octubre</option>
                                                <option value="11">Noviembre</option>
                                                <option value="12">Diciembre</option>

                                            </select>
                                        </div>
                                        <div class="col s12 m12 l4">

                                            <label>A&ntilde;o de expiraci&oacute;n</label>
                                            {{-- AGREGUE EL NAME CCEXPIRATION --}}
                                            <select class="browser-default" id="id_sl_year" name="ccexpiration">
                                                <option value="" disabled selected>Año</option>
                                                <option value="16">2016</option>
                                                <option value="17">2017</option>
                                                <option value="18">2018</option>
                                                <option value="19">2019</option>
                                                <option value="20">2020</option>
                                                <option value="21">2021</option>
                                            </select>
                                        </div>

                                        <div class="input-field col s12 m12 l4">
                                            <i class="material-icons prefix"><i class="mdi-action-verified-user"></i></i>
                                            {{-- AGREGUE EL NAME CVV --}}
                                            <input id="icon_csv" name="cvv" type="text" class="validate">
                                            <label for="icon_csv">Codigo CVV</label>
                                            <a href="{{ asset('images/csv.png')}}" target="_blank" class="center-align">Encuentra el
                                                CVV</a>
                                        </div>
                                        
                                    </div>
                                </form>

                            </div>
                            <div class="row">
                                <div class="col s10 offset-s1 l8 offset-l2">
                                    <div class="col s12 m10 l10 offset-l1">
                                        <img class="responsive-img img_marcas" src="{{ asset('images/Logos-de-marcas.jpg')}}"
                                             alt="3D Secure"/>
                                    </div>
                                    <div class="col s6 m5 l4 offset-l2">  
                                        <img class="responsive-img img_visa" src="{{ asset('images/VISA1.jpg')}}" alt="3D Secure"/>
                                    </div>
                                    <div class="col s6 m5 14">
                                        <img class="responsive-img img_secure" src="{{ asset('images/MC-Secure.jpg')}}"
                                             alt="3D Secure"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col s10 m10 l3 right">
                            {{-- CAMBIE LA ETIQUETA A POR UNA ETIQUETA BUTTON TYPE SUBMIT PARA PODER SUBMITEAR EL FORM --}}
                                <button type="submit" class="waves-effect personal  btn-large group-go_make_pay" id="go_make_pay">
                                    Continuar<i class="material-icons right"><i class="mdi-action-payment"></i></i></button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col s12 l12">
                <h4 style="text-align: center; font-size: 40px;"><strong><span  id="total" style="color: #e89a98;"></span></strong></h4>
            </div>

            <div class="col s10  m10 l12">
                <div class="offset-s1 preloader-wrapper big center">
                    <div class="spinner-layer spinner-blue-only">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="gap-patch">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="col hide-on-small-only s2 m2 l2 fixed_nav_right">
        <ul class="section table-of-contents">
            <li><a href="#package" id="right_nav_package">Regalos</a></li>
            <li><a href="#cart" id="right_nav_cart">Carrito</a></li>
            <li><a href="#checkout" id="checkout_cart">Caja</a></li>
            <li><a href="#success" id="success_confirmed">Confirmaci&oacute;n</a></li>
        </ul>
    </div>
</div>

<div class="pink lighten-5 container_before_cart">
    <div class="row">
        <div class="col s8 m5 l3 offset-m1 offset-l4 subtotal">
            <p>

            <h3 id="spn_subtotal"></h3></p>
        </div>
        <div class="col s4 m5 l2 offset-m1 btn_cart">
            <a class="waves-effect personal  btn-large" id="go_cart" href="#cart"><i class="material-icons"><i
                        class="mdi-action-shopping-cart"></i></i></a>
        </div>
    </div>
</div>

<div class="terms col s12 m12 l12" style="text-align: center;">
    <a class="terms"><p>Terminos y condiciones</p></a>
</div>

@show
    
</body>

@section('footer')
<footer class="page-footer">
    <div class="footer-container">
        <div class="row">
            <div class="col l2 offset-l1 s6">
                <a href="http://itravelhn.com/"><img class="ilogo" src="{{asset('images/logo_final_iTravel_mobile.png')}}"></a>
            </div>
            <div class="col l3 s6">
                <h5 class="white-text">Cont&aacute;ctanos:</h5>

                <p class="grey-text text-lighten-4">Ll&aacute;manos: 2231-1211</p>

                <p class="grey-text text-lighten-4">Escr&iacute;benos: info@dreamgifts.hn
                </p>

                <p class="grey-text text-lighten-4">Dirección: Paseo los proceres, Tegucigalpa - Honduras.
                </p>
            </div>
            <div class="col l5 offset-l1 s12">
                <h5 class="white-text">S&iacute;guenos:</h5>
                <ul class="container_social">
                    <li class="col s4"><a class="grey-text text-lighten-3 social" href="#!"> <img
                                src="{{ asset('images/icon-fb-.png')}}" alt="" class="circle"></a></li>
                    <li class="col s4"><a class="grey-text text-lighten-3 social" href="#!"> <img
                                src="{{ asset('images/icon-instagram.png')}}" alt="" class="circle"></a></li>
                    <li class="col s4"><a class="grey-text text-lighten-3 social" href="#!"> <img
                                src="{{ asset('images/icon-tw.png')}}" alt="" class="circle"></a></li>
                </ul>
            </div>
        </div>

    </div>
    <div class="footer-copyright">
        <div class="container">
            © 2015 Copyright Soy Servidor
            <a class="grey-text text-lighten-4 right" href="http://dreamgifts.ss.hn/">S&iacute;tio Oficial</a>
        </div>
    </div>
</footer>

    @section('script')
    <script src="{{ asset('js/jquery-1.11.3.min.js')}}"></script>
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/modernizr.js') }}"></script>
    <script src="{{ asset('js/materialize.min.js')}}"></script>
    <script src="{{ asset('js/materialize.js') }}"></script>
    <script src="{{ asset('js/maskedinput.js')}}"></script>
    <script src="{{ asset('js/behavior.js')}}"></script>
    <script>
        jQuery(document).ready(function($) {
            $('#go_make_process_pay').click(function(event) {
                    $('#total').text("TOTAL A PAGAR: " + $('#col_total').html());
                });
            });
    </script>


    @show
@show
</html>