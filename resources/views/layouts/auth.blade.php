<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="_token" content="{!! csrf_token() !!}"/>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Dreamgifts @yield('title')</title>
	@section('css')
		<link rel="stylesheet" href="{{ asset('css/normalize.min.css')}}">
		<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/backend.css') }}">
        <link rel="stylesheet" href="{{ asset('css/materialize.css')}}">
        <link rel="stylesheet" href="{{ asset('css/materialize.min.css')}}">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.1/css/materialize.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
        <link rel="shortcut icon" href="{{{ asset('images/favicon.png') }}}">
	@show
</head>
<body>
	@section ('header')	
	<div class=" navbar-fixed">
	    <nav>
	        <div class="nav-wrapper">
	          <a href="{{route('home')}}" class="brand-logo center"><img src="{{ asset('images/logo.png') }}" alt=""/></a>
	          <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
		          <ul id="nav-mobile" class="right hide-on-med-and-down">
		            <li>
		            	<a href="{{route('newCustomerPackage')}}">Crear Nuevo Paquete</a>
		            </li>
		                <li>
	                		<a href="{{route ('users')}}">Usuarios</a>
	                	</li>
	                <li>
	                	<a class="dropdown-button" href="#!" data-activates="dropdown1">
							@if (Auth::check())
								{{Auth::user()->name}}
							@endif
	                	<i class="material-icons right">arrow_drop_down</i>
	                	</a>
	                	<ul id="dropdown1" class="dropdown-content">
	                		<li>
	                			<a href="auth/logout"> Salir
	                			<i class="material-icons right">play_arrow</i>
	                			</a>
	                		</li>
	                		
	                	</ul>
	                </li>
		          </ul>
		          <ul class="side-nav" id="mobile-demo">
                     <li>
		            	<a href="{{route('newCustomerPackage')}}">Crear Nuevo Paquete</a>
		            </li>
		                <li>
	                		<a href="{{route ('users')}}">Usuarios</a>
	                	</li>
	                <li>
	                	<a class="dropdown-button" href="#!" data-activates="dropdown1">
							@if (Auth::check())
								{{Auth::user()->name}}
							@endif
	                	<i class="material-icons right">arrow_drop_down</i>
	                	</a>
	                	<ul id="dropdown1" class="dropdown-content">
	                		<li>
	                			<a href="auth/logout"> Salir
	                			<i class="material-icons right">play_arrow</i>
	                			</a>
	                		</li>
	                		
	                	</ul>
	                </li>
                  </ul>


	        </div>
	    </nav>
	</div>
	@show
	@section('content')
	
	@show
	

	@section('script')
		<script src="{{ asset('js/code.js') }}"></script>
		<script src="{{ asset('js/jquery-1.11.3.min.js') }}"></script>
		<script src="{{ asset('js/jquery.min.js') }}"></script>
		<script src="{{ asset('js/modernizr.js') }}"></script>
		<script src="{{ asset('js/materialize.min.js') }}"></script>
		<script src="{{ asset('js/materialize.js') }}"></script>
		
		<script type="text/javascript">
				$(document).ready(function(){
				$(".button-collapse").sideNav();
				$('.input-field').trigger('autoresize');
				$(".dropdown-button").dropdown();
				$('.modal-trigger').leanModal();
				$('select').material_select();
				$('select:not([multiple])').material_select();
				$('.datepicker').pickadate({
				    selectMonths: true, // Creates a dropdown to control month
				    selectYears: 15 // Creates a dropdown of 15 years to control year
				});
			  	$('#textarea1').trigger('autoresize');
			  	
			});
		</script>
	@show
</body>
</html>