<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>@yield('title','DreamGifts')</title>
	@section('css')
		<link rel="stylesheet" href="{{ asset('css/normalize.min.css')}}">
		<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/backend.css') }}">
        <link rel="stylesheet" href="{{ asset('css/materialize.css')}}">
        <link rel="stylesheet" href="{{ asset('css/materialize.min.css')}}">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.1/css/materialize.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
	@show
</head>
<body>
	@section ('header')	
	<div class=" navbar-fixed">
	    <nav>
	        <div class="nav-wrapper">
	          <a href="#" class="brand-logo center"><img src="{{ asset('images/logo.png') }}" alt=""/></a>
		          <ul id="nav-mobile" class="right hide-on-med-and-down">
		            <li>
		            	<a href="#package">Crear Nuevo Paquete</a>
		            </li>
		                @if (Auth::user()->role== 'admin')
	                	<li>
	                		<a href="{{route ('users')}}">Usuarios</a>
	                	</li>
		                @endif
	                <li>
	                	<a class="dropdown-button" href="#!" data-activates="dropdown1">
							@if (Auth::check())
								{{Auth::user()->name}}
							@endif
	                	<i class="material-icons right">arrow_drop_down</i>
	                	</a>
	                	<ul id="dropdown1" class="dropdown-content">
	                		<li>
	                			<a href="auth/logout"> Salir
	                			<i class="material-icons right">play_arrow</i>
	                			</a>
	                		</li>
	                		
	                	</ul>
	                </li>
		          </ul>

	        </div>
	    </nav>
	</div>
	@show
	@section('content')
	
	@show
	

	@section('script')
		<script src="{{ asset('js/jquery-1.11.3.min.js') }}"></script>
		<script src="{{ asset('js/jquery.min.js') }}"></script>
		<script src="{{ asset('js/modernizr.js') }}"></script>
		<script src="{{ asset('js/materialize.min.js') }}"></script>
		<script src="{{ asset('js/materialize.js') }}"></script>
		<script src="{{ asset('js/code.js') }}"></script>
		
	@show
</body>
</html>