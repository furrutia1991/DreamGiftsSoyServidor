@extends('layouts.auth')

@section('css')
	@parent
	<link rel="stylesheet" href="{{ asset('css/responsive-tables.css') }}">
	<link rel="stylesheet" href="{{ asset('css/jquery.dynatable.css') }}">
@stop

@section('content')
	<div class="row header">
		<div class="col  s12">
			 <a href="{{ route ('getOrders', $package->package_id ) }}" class="left waves-effect waves-light btn-large"><i class="material-icons right">settings_backup_restore</i>Atras</a>
		</div>
	</div>
	<div class="tittle">
		<h4 style="text-align: center">Detalle de transacción</h4>
	</div>

	@if(!$transaction)
		<div class="tittle">
			<h2>No hay transacciones para mostrar</h2>	
		</div>
	@endif

	@if($payment)
		<form action="{{route('saveStatus', [$order->order_id, $package->package_id ])}}" id="order_state" method="POST" enctype="multipart/form-data">
		{!! csrf_field() !!}
			<div class="row">
				<div class="col s12 m12 l10 offset-l2 status-color">
					<div class="tittle col s12 m6 l5 ">
						<h2 class="status-color">Estado de la orden:</h2>
					</div>
					<div class="order-select col s8 offset-s2 m5 l5">
						<select name="status">
							@foreach($status as $e)
								<option value="{{$e}}" @if($e == $order->status)selected="selected" @endif>{{$e}}</option>
							@endforeach
						</select>	
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<input type="hidden" name="id_order" value="{{$order->order_id}}"/>
				<div class="buyer-name col s12 m12 l10 status-color">
					<div class="tittle col s12 m12 l7 left">
						<h2 class="status-color">Orden #: {{$order->order_id}}</h2>
					</div>
					<div class="tittle col s12 m12 l5 right">
						<h2 class="status-color">Comprador: {{$transaction[0]->order->buyer->name}}</h2>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col s12 l10 offset-l1">
					<table class="striped responsive-table" id="my-table">
						<thead>
							<tr>
								<th  width="150" height="35">Id transacción</th>
								<th  width="150" height="35">Componente</th>
								<th  width="150" height="35">Abonos regalados</th>
								<th  width="150" height="35">Valor regalo</th>
								<th  width="150" height="35">Tipo de pago</th>
								<th  width="150" height="35">Fecha transacción</th>
							</tr>
						</thead>
						<tbody class="packages-body">
							@foreach ($transaction as $element)
							<tr>
								<td class="code">{{$element->transaction_id}}</td>
								<td class="code">{{$element->component->tittle}}</td>
								<td class="Comentario">{{$element->num_parts}}</td>
								<td class="wedding">{{$element->amount}}</td>
								@foreach($payment as $p)
									<td class="wedding">{{$p->paymentmethod->tittle}}</td>
								@endforeach
								<td class="wedding">{{$element->order->date_created}}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					<div class="row">
						<div class="left total col s12 m8 l12">
							<div class="tittle col s12 m8 l12">
								<h2>Total: ${{$order->total}}</h2>
							</div>
						</div>
					</div>
					<div class="row">
						<div>
							<button href="#modal-change-status" class="right btn-large waves-effect waves-light modal-trigger" name="action">Guardar<i class="material-icons right">send</i>
							</button>
						</div>
						<div>
							<a href="#modal-delete-transaction"class="modal-trigger waves-effect waves-light btn-large"><i class="material-icons right">delete</i>Eliminar</a>
						</div>
					</div>
				</div>
			</div>

		{{-- Confirm Modal --}}
		<div id="modal-change-status" class="modal">
	        <div class="modal-content">
	            <div class="row">
	                <div class="col l8 offset-l2">
	                    <h4>Cambio de estado de transacción</h4>
	                    <p style="text-align: center;">
	                    	¿Desea cambiar el estado de esta transacción?
	                    </p>
	                </div>
	            </div>
	            <br>
	            <div class="row">
	                <div class="col l10 offset-l2">
	                    <button class="col l4 btn waves-effect" type="submit" id="confirm-change-status"
	                            name="action">
	                        Confirmar
	                        <i class="material-icons right"><i class="mdi-toggle-check-box"></i></i>
	                    </button>
	                    <button class="col l4 offset-l1 btn waves-effect">
	                        No
	                        <i class="material-icons right"><i class="mdi-action-highlight-remove"></i></i>
	                    </button>
	                </div>
	            </div>

	        </div>
	        <div class="modal-footer">
	            <a class="modal-action modal-close waves-effect waves-green btn-flat tooltipped" id="out-modal"
	               data-position="top" data-delay="50" data-tooltip="Salir de esta pantalla">Salir</a>
	        </div>
	    </div>

	    {{-- Delete Modal --}}
		<div id="modal-delete-transaction" class="modal">
	        <div class="modal-content">
	            <div class="row">
	                <div class="col l8 offset-l2">
	                    <h4>Elminar transacción</h4>
	                    <p style="text-align: center;">
	                    	¿Esta seguro que desea eliminar esta transacción?
	                    </p>
	                </div>
	            </div>
	            <br>
	            <div class="row">
	                <div class="col l10 offset-l2">
	                    <a  href="{{route('deleteTransaction', [$order->order_id, $package->package_id])}}" class="col l4 btn waves-effect" name="action">
	                        Confirmar
	                        <i class="material-icons right"><i class="mdi-toggle-check-box"></i></i>
	                    </a>
	                    <button class="col l4 offset-l1 btn waves-effect" >
	                        No
	                        <i class=" material-icons right"><i class="mdi-action-highlight-remove"></i></i>
	                    </button>
	                </div>
	            </div>

	        </div>
	        <div class="modal-footer">
	            <a class="modal-action modal-close waves-effect waves-green btn-flat tooltipped" id="out-modal"
	               data-position="top" data-delay="50" data-tooltip="Salir de esta pantalla">Salir</a>
	        </div>
	    </div>
	</form>

	@endif
@stop
	
	@section('script')
	@parent
	<script type="text/javascript">
		    $(document).ready(function(){
		        $("#confirm-change-status").click(function () {
		            jQuery("#order_state").submit();
		        });
	    });
	    
	</script>
	@stop