@extends('layouts.auth')

@section('css')
	@parent
	<link rel="stylesheet" href="{{ asset('css/responsive-tables.css') }}">
	<link rel="stylesheet" href="{{ asset('css/jquery.dynatable.css') }}">
@stop

@section('content')
	<div class="row header">
		<div class="col  s12">
			 <a href="{{route ('package', $package->package_id)}}" class="left waves-effect waves-light btn-large"><i class="material-icons right">settings_backup_restore</i>Atras</a>
		</div>
		<input id="package_id_search" type="hidden" value="{{$package->package_id}}">
	</div>
	<div class="tittle">
		<h2 style="text-align: center">Transacciones de boda: {{$package->customer->name}}</h2>
	</div>
	<div class="row">
		<div class="col l8 offset-l2 s12">
			<label>Buscar</label>
			<input type="text" class="search-transactions" placeholder="Buscar transacciones por comprador, id o estado..." autofocus>
		</div>
		<div >
			<a href="javascript:location.reload()" class="refresh-btn left btn waves-effect waves-light">Ver todas<i class="material-icons right">perm_identity</i>
		  	</a>
		</div>
	</div>

	<div class="row">
		<div class="col s12 l10 offset-l1">
			<table class="responsive-table" id="my-table">
				<thead>
					<tr>
						<th width="100" height="48">Ver</th>
						<th width="100" height="48">Id orden</th>
						<th width="100" height="48">Comprador</th>
						<th width="100" height="48">Total</th>
						<th width="100" height="48">Estado</th>
						<th width="100" height="48">Fecha</th>
					</tr>
				</thead>
				<tbody class="transactions-body">
					@foreach($order as $element)
						<tr>
							<td class="show"><a href="{{route('getComponentOrder', [$element->order_id, $package->package_id ])}}" class="waves-effect waves-light btn-large"><i class="material-icons right">search</i></a>
							</td>
							<td class="code">
								{{$element->order_id}}
							</td>
							<td class="code">
								{{$element->buyer->name}}
							</td>
							<td class="code">
								$: {{$element->total}}
							</td>
							<td class="code">
								{{$element->status}}
							</td>
							<td class="code">
								{{$element->date_created}}
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<div class="row paginate">
		<div class="col s12 l4 offset-l4">

			{!! $order->render() !!}
		</div>
	</div>

@stop

@section('script')
	@parent
	<script>
		jQuery(document).ready(function($) {
		$('.search-transactions').keyup(function(event) {
		    if(event.keyCode == 13){
		    	if($(".search-transactions").val().length > 0){
		        var text = $(this).val() ? $(this).val() : 'blank';
		        var url = window.location.href; 
		        var package_id_search = $('#package_id_search').val();
		        console.log(url);
		        $.get(url + '/buscar/' + text, function(data) {

		            if (data) {
		                
		                if(data.redirect)
		                {
		                    window.location.href = data.redirect;
		                }
		                $('.Jdelete').css("display", "none");
		                $('.paginate').css("display", "none");
		                $('.transactions-body').html('');
		                $('.refresh-btn').css("display", "block");
		                
		                $.each(data, function(index, val) {

		                    var markup = "<tr>" +
		                        "<td width='100'><a class='waves-effect waves-light btn-large' href='orden/" + val.order_id + "/transacciones/" + package_id_search + "'>" + "<i class='material-icons right'>search</i></a></td>" +
		                        "<td width='100'>" + val.order_id + "</td>" +
		                        "<td width='100'>" + val.buyer_name + "</td>" +
		                        "<td width='100'>" + val.total + "</td>" +
		                        "<td width='100'>" + val.status + "</td>" +
		                        "<td width='100'>" + val.date_created + "</td>" +
		                    "</tr>";
		                    $('.transactions-body').append(markup);
		                })
		            }

		        })
		    }
		    };
			    
			});
		});
		
	</script>
@stop


