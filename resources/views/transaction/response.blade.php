<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="_token" content="{!! csrf_token() !!}"/>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Dreamgifts-Pago @yield('title')</title>
	<link rel="stylesheet" href="{{ asset('css/normalize.min.css')}}">
	<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/backend.css') }}">
    <link rel="stylesheet" href="{{ asset('css/materialize.css')}}">
    <link rel="stylesheet" href="{{ asset('css/materialize.min.css')}}">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.1/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{{ asset('images/favicon.png') }}}">
</head>
<body>
	<div class=" navbar-fixed">
	    <nav>
	        <div class=" navbar-fixed">
	            <nav>
	                <div class="nav-wrapper">
	                  <a href="http://dreamgifts.ss.hn/" class="brand-logo center"><img src="{{ asset('images/logo.png')}}" alt=""/></a>
	                </div>
	            </nav>

	        </div>
	    </nav>
	</div>
	<div class="tittle">
	    <h3 style="text-align: center">Confirmación de Pago</h3>

	    <h2 style="text-align: center"><?php $dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
		$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
		echo $dias[date('w')]." ".date('d')." de ".$meses[date('n')-1]. " del ".date('Y') ;  ?></h2>
	</div>
	<br>

	<div class="row">
		<div id="success" class="success_img col l8 offset-l2 section scrollspy container_confirmed_large">
		    <div class="hero">
		        <div class="z-depth-1 grey lighten-4 container_confirmed " >

		            <h4 class="center-align" id="message_confirmed">{{$res_eva}}</h4>
		            <h6 class="center-align" id="message_confirmed_1">{{$res_comp}}</h6>

		        </div>
		    </div>
		</div>
	</div>

	<div class="terms col s12 m12 l12" style="text-align: center;">
	    <a class="terms" style="cursor: pointer"><p>Terminos y condiciones</p></a>
	</div>
	<footer class="page-footer">
	    <div class="footer-container">
	        <div class="row">
	            <div class="col l2 offset-l1 s6">
	                <a href="http://itravelhn.com/"><img class="ilogo" src="{{asset('images/logo_final_iTravel_mobile.png')}}"></a>
	            </div>
	            <div class="col l3 s6">
	                <h5 class="white-text">Cont&aacute;ctanos:</h5>

	                <p class="grey-text text-lighten-4">Ll&aacute;manos: 2231-1211</p>

	                <p class="grey-text text-lighten-4">Escr&iacute;benos: info@dreamgifts.hn
	                </p>

	                <p class="grey-text text-lighten-4">Dirección: Paseo los proceres, Tegucigalpa - Honduras.
	                </p>
	            </div>
	            <div class="col l5 offset-l1 s12">
	                <h5 class="white-text">S&iacute;guenos:</h5>
	                <ul class="container_social">
	                    <li class="col s4"><a class="grey-text text-lighten-3 social" href="#!"> <img
	                                src="{{ asset('images/icon-fb-.png')}}" alt="" class="circle"></a></li>
	                    <li class="col s4"><a class="grey-text text-lighten-3 social" href="#!"> <img
	                                src="{{ asset('images/icon-instagram.png')}}" alt="" class="circle"></a></li>
	                    <li class="col s4"><a class="grey-text text-lighten-3 social" href="#!"> <img
	                                src="{{ asset('images/icon-tw.png')}}" alt="" class="circle"></a></li>
	                </ul>
	            </div>
	        </div>

	    </div>
	    <div class="footer-copyright" style="margin-left: 0!important;">
	        <div class="container">
	            © 2015 Copyright Soy Servidor
	            <a class="grey-text text-lighten-4 right" href="http://dreamgifts.ss.hn/">S&iacute;tio Oficial</a>
	        </div>
	    </div>
	</footer>
</body>
	<script src="{{ asset('js/jquery-1.11.3.min.js')}}"></script>
	<script src="{{ asset('js/jquery.min.js') }}"></script>
	<script src="{{ asset('js/modernizr.js') }}"></script>
	<script src="{{ asset('js/materialize.min.js')}}"></script>
	<script src="{{ asset('js/materialize.js') }}"></script>
	<script src="{{ asset('js/maskedinput.js')}}"></script>
	<script src="{{ asset('js/behavior.js')}}"></script>
</html>