@extends('layouts.auth')

@section('title', 'Dreamgifts')

@section('content')
	@parent
	<div class="row">
		<div class="modules col s12 m 12 l10 offset-l1">
			<div class="hoverable col s10 offset-s1 m6 l4" >
				<a href="{{ route('customers') }}">
					<img class="imgright" src="{{asset('images/4-disfruta-tu-dreamgifts.png')}}" alt="">
					<h4 class="center-align">Bodas</h4>
				</a>
			</div>
			<div class="hoverable col s10 offset-s1 m6 l4" >
				<a href="{{ route('packages') }}">
					<img class="imgright" src="{{asset('images/3-lista-de-apartados.png')}}" alt="">
					<h4 class="center-align">Ver Paquetes</h4>
				</a>
			</div>
			<!-- <div class="hoverable col s10 offset-s1 m6 l4" >
				<a href="{{route('newPackages')}}">
					<img class="imgright" src="{{asset('images/1-crea-tu-lista.png')}}" alt="">
					<h4 class="center-align">Crear Paquetes</h4>
				</a>
			</div> -->
			<div class="hoverable col s10 offset-s1 m6 l4" >
				<a href="{{ route('destinations') }}">
					<img class="imgright" src="{{asset('images/2-comparte.png')}}" alt="">
					<h4 class="center-align">Destinos</h4>
				</a>
			</div>
		</div>
	</div>		
	@stop


