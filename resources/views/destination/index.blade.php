@extends('layouts.auth')	

@section('css')
	@parent
	<link rel="stylesheet" href="{{ asset('css/responsive-tables.css') }}">
	<link rel="stylesheet" href="{{ asset('css/jquery.dynatable.css') }}">
@stop


@section('content')
	

	<div class="row header">
		<div class="col  s12">
			 <a href="{{route ('home')}}" class="left waves-effect waves-light btn-large"><i class="material-icons right">settings_backup_restore</i>Atras</a>
			 <a class="right modal-trigger waves-effect waves-light btn-large" href="#modal1"><i class="material-icons right">playlist_add</i>Nuevo Destino</a>
		</div>
	</div>
	<div class="tittle">
		<h2 style="text-align: center">Destinos</h2>
	</div>
	<div class="row">
		<div class="col l8 offset-l2 s12">
			<label>Nombre de destino</label>
			<input type="text" class="search-destination" placeholder="Buscar destino por nombre, pais o ciudad..." autofocus>
		</div>
		<div >
			<a href="javascript:location.reload()" class="refresh-btn left btn waves-effect waves-light">Ver todos<i class="material-icons right">perm_identity</i>
		  	</a>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col l10 offset-l1 s12">
			<table class="responsive">
				<thead>
					<th width="100" height="40">Nombre </th>
					<th width="100" height="40">Pais</th>
					<th width="100" height="40">Ciudad</th>
					<th width="100" height="40">No. Telefono</th>
					<th width="100" height="40" class="Jdelete">Eliminar</th>
				</thead>
				<tbody class="destination-body">
					@foreach ($destination as $element)
						<tr>
							<td><a href="{{ route('destination', $element->destination_id) }}">{{$element->name}}</a></td>
							<td>{{$element->city->country->name}}</td>
							<td>{{$element->city->name}}</td>
							<td>{{$element->phone}}</td>
							<td><a href="#modal-delete-destination{{$element->destination_id}} " class=" modal-trigger waves-effect waves-light btn-large"><i class="material-icons right">delete</i>Eliminar</a></td>
						</tr>
						{{-- Delete Modal --}}
								<div id="modal-delete-destination{{$element->destination_id}}" class="modal">
							        <div class="modal-content">
							            <div class="row">
							                <div class="col l8 offset-l2">
							                    <h4 style="text-align: center;">Elminar Destino</h4>
							                    <p style="text-align: center;">
							                    	¿Esta seguro que desea eliminar el destino {{$element->name}}?
							                    </p>
							                </div>
							            </div>
							            <br>
							            <div class="row">
							                <div class="col l10 offset-l2">
							                    <a   href="{{ route('deleteDestination', $element->destination_id) }}" class="col l4 btn waves-effect" name="action">
							                        Confirmar
							                        <i class="material-icons right"><i class="mdi-toggle-check-box"></i></i>
							                    </a>
							                    <button class="modal-close col l4 offset-l1 btn waves-effect" type="submit" id="decline-delete-component"
							                            name="action">
							                        No
							                        <i class="material-icons right"><i class="mdi-action-highlight-remove"></i></i>
							                    </button>
							                </div>
							            </div>

							        </div>
							        <div class="modal-footer">
							            <a class="modal-action modal-close waves-effect waves-green btn-flat tooltipped" id="out-modal"
							               data-position="top" data-delay="50" data-tooltip="Salir de esta pantalla">Salir</a>
							        </div>
							    </div>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<div class="row paginate">
		<div class="col s12 l4 offset-l4">
			{!! $destination->render() !!}
		</div>
	</div>	
	{{-- Destination Modal --}}
	<div id="modal1" class="modal" style="padding: 3em">
	    <div class="modal-content">
	    	<h4 style="text-align: center">Nuevo Destino</h4>
				<form action="{{ route('newDestination')}}" method="POST" enctype="multipart/form-data">
					{!! csrf_field() !!}

					<div class="row">
						<div class="col s12 l6">
							<label>
								Nombre:
							</label>
								<input type="text" name="name" required>
						</div>
						<div class="col s12 l6">
							<label>
								Descripción:
							</label>
								<input type="text" name="description" required>
						</div>
					</div>
					<div class="row">
						<div class="col s12 l6">
							<label>
								No. Telefono:
							</label>
								<input type="number" name="phone" cols="10" rows="2"></input>
						</div>
						<div class="col s12 l6">
							<label>
								Código Postal:
							</label>
								<input type="number" name="postal_code" cols="10" rows="2"></input>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12 l6">
						    <select name="city" required>
						      <option value="" disabled selected>Elige una ciudad</option>
						      	@foreach($city as $element)
									<option value="{{$element->city_id}}">{{$element->name}}</option>
								@endforeach
						    </select>
						    <label>Ciudad:</label>
						</div>
					</div>
					<button class="left btn waves-effect waves-light modal-action modal-close waves-green">Cerrar<i class="material-icons right">cancel</i>
					</button>
					<button class="right btn waves-effect waves-light" type="submit" name="action">Guardar<i class="material-icons right">send</i>
		  			</button>
				</form>	    		
	    </div>
	</div>
@stop

@section('script')
	@parent
	<script>
		jQuery(document).ready(function($) {
		$('.search-destination').keyup(function(event) {
		    if(event.keyCode == 13){
		        var text = $(this).val() ? $(this).val() : 'blank';

		        $.get('destinos/buscar/' + text, function(data) {

		            if (data) {
		                // alert("source");
		                if(data.redirect)
		                {
		                    window.location.href = data.redirect;
		                }
		                $('.Jdelete').css("display", "none");
		                $('.paginate').css("display", "none");
		                $('.destination-body').html('');
		                $('.refresh-btn').css("display", "block");
		                
		                $.each(data, function(index, val) {

		                    var markup = "<tr>" +
		                        "<td><a href='destino/" + val.destination_id + "'>" + val.name + "</a></td>" +
		                        "<td>" + val.country_name + "</td>" +
		                        "<td>" + val.city_name + "</td>" +
		                        "<td>" + val.phone + "</td>" +
		                    "</tr>";
		                    $('.destination-body').append(markup);
		                })
		            }

		        })
		    }
			    
			});
		});
		
	</script>
@stop