@extends('layouts.auth')	

@section('css')
	@parent
	<link rel="stylesheet" href="{{ asset('css/responsive-tables.css') }}">
	<link rel="stylesheet" href="{{ asset('css/jquery.dynatable.css') }}">
@stop


@section('content')

	<div class="row header">
		<div class="col  s12">
			 <a href="{{route ('destinations')}}" class="left waves-effect waves-light btn-large"><i class="material-icons right">settings_backup_restore</i>Atras</a>
		</div>
	</div>
	<div class="tittle">
		<h4 style="text-align: center">Destino</h4>
	</div>
	<br>
    <div class="row">
   		<div class="col s12 l10 offset-l1">
			<form id="destination_data" action="{{route('updateDestination', $destination->destination_id)}}" method="POST" enctype="multipart/form-data">
				{!! csrf_field() !!}

				<div class="row">
					<div class="col s12 l6">
						<label>
							Nombre:
						</label>
							<input type="text" value="{{$destination->name}}" name="name" required>
					</div>
					
					<div class="col s12 l6">
						<div class="input-field col s12">
						    <select name="city" required>
						      <option value="" disabled selected>Elige una ciudad</option>
						      	@foreach($city as $element)
									<option value="{{$element->city_id}}">{{$element->name}}</option>
								@endforeach
						    </select>
						    <label>Ciudad:</label>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col s12 l6">
						<label>
							No. Telefono:
						</label>
							<input type="text" value="{{$destination->phone}}" name="phone" cols="10" rows="2"></input>
					</div>
					<div class="col s12 l6">
						<label>
							Código Postal:
						</label>
							<input type="text" value="{{$destination->postal_code}}" name="postal_code" cols="10" rows="2"></input>
					</div>
				</div>
				
				<div class="row">
					<div class="input-field col s12">
					  <textarea name="description" id="textarea1" class="materialize-textarea" required>{{$destination->description}}</textarea>
					  <label for="textarea1">Descripción:</label>
					</div>
				</div>

				<button href="#modal-change-destination" type="submit" class="modal-trigger right btn-large waves-effect waves-light" name="action">Guardar<i class="material-icons right">send</i>
				</button>


				{{-- Confirm Modal --}}
				<div id="modal-change-destination" class="modal">
			        <div class="modal-content">
			            <div class="row">
			                <div class="col l8 offset-l2">
			                    <h4 style="text-align: center;">Editar Destino</h4>
			                    <p style="text-align: center;">
			                    	¿Desea cambiar datos de este destino?
			                    </p>
			                </div>
			            </div>
			            <br>
			            <div class="row">
			                <div class="col l10 offset-l2">
			                    <a id="confirm-change-destination" class="col l4 btn waves-effect" name="action">
			                        Confirmar
			                        <i class="material-icons right"><i class="mdi-toggle-check-box"></i></i>
			                    </a>
			                    <button class="modal-close col l4 offset-l1 btn waves-effect" type="submit" id="decline-delete-component"
			                            name="action">
			                        No
			                        <i class="material-icons right"><i class="mdi-action-highlight-remove"></i></i>
			                    </button>
			                </div>
			            </div>

			        </div>
			        <div class="modal-footer">
			            <a class="modal-action modal-close waves-effect waves-green btn-flat tooltipped" id="out-modal"
			               data-position="top" data-delay="50" data-tooltip="Salir de esta pantalla">Salir</a>
			        </div>
			    </div>


			</form>	  
		</div>  		
    </div>


@stop
	@section('script')
	@parent
	<script type="text/javascript">
		    $(document).ready(function(){
		        $("#confirm-change-destination").click(function () {
		            jQuery("#destination_data").submit();
		        });
		       
	    });
	    
	</script>
	@stop