<!DOCTYPE html>
<html lang="en">
<head>
	<title>Terminos y condiciones</title>
	<meta charset="UTF-8">
	<meta name="_token" content="{!! csrf_token() !!}"/>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	@section('css')
		<link rel="stylesheet" href="{{ asset('css/normalize.min.css')}}">
		<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/backend.css') }}">
        <link rel="stylesheet" href="{{ asset('css/materialize.css')}}">
        <link rel="stylesheet" href="{{ asset('css/materialize.min.css')}}">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.1/css/materialize.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>

	@show
</head>
<body>
	<div class="row">
		<div class="col s12 m12 l8 offset-l2">
			<br>
			<div class="row">
				<a href="http://dreamgifts.ss.hn/" target="_blank">
					<div class="col s12 m12 l12" style="background:url(http://dreamgifts.ss.hn/wp-content/uploads/2015/12/slide_principal.jpg); no-repeat; background-size: cover">
					<img src="http://dreamgifts.ss.hn/wp-content/uploads/2015/12/logo-slide_dreamgift.png">
					</div>
				</a>
			</div>
			<div class="std"><p style="text-align: justify;">&nbsp;</p>
					<p align="JUSTIFY"><strong>Políticas generales de uso </strong></p>
					<ol>
					<li>
					<p align="JUSTIFY">La Nueva Blanca empresa mercantil constituida de acuerdo a las leyes de la República de Honduras es la propietaria de la página Web <a href="//www.dreamgifts.hn">www.dreamgifts.hn</a></p>
					</li>
					<li>
					<p align="JUSTIFY">Este contrato describe los Términos y Condiciones Generales aplicables a las compras de los productos ofrecidos por La Nueva Blanca. dentro del sitio <a href="//www.dreamgifts.hn">www.dreamgifts.hn</a>. Cualquier persona que desea acceder y/o usar el sitio podrá hacerlo sujetándose a los Términos y Condiciones Generales, junto con todas las políticas y principios que rigen <a href="//www.dreamgifts.hn">www.dreamgifts.hn</a> y que son incorporados al presente por referencia.</p>
					</li>
					<li>
					<p align="JUSTIFY">Toda la facturación que se realice en esta página será a través La Nueva Blanca.</p>
					</li>
					<li>
					<p align="JUSTIFY">Cualquier persona que no acepte estos Términos y Condiciones Generales los cuales tienen un carácter obligatorio y vinculante, deberá abstenerse de utilizar el sitio y/o los productos.</p>
					</li>
					<li>
					<p align="JUSTIFY">El Usuario debe leer, entender y aceptar todas las condiciones establecidas en los Términos y Condiciones Generales y en nuestras Políticas así como en los demás documentos incorporados en los mismos por referencia, previos a su inscripción como usuario de <a href="//www.dreamgifts.hn">www.dreamgifts.hn</a></p>
					</li>
					<li>
					<p align="JUSTIFY">Los productos de venta solo están disponibles para personas naturales que tengan capacidad legal para contratar. No podrán realizar compras las personas que no tengan esa capacidad, los menores de edad o usuarios de <a href="//www.dreamgifts.hn">www.dreamgifts.hn</a> que hayan sido suspendidos temporalmente o inhabilitados definitivamente.</p>
					</li>
					<li>
					<p align="JUSTIFY">Es obligatorio completar el formulario de inscripción en todos sus campos con datos válidos para poder comprar los productos que brinda <a href="//www.dreamgifts.hn">www.dreamgifts.hn</a>. El futuro usuario deberá completarlo con su información personal de manera exacta, precisa y verdadera, asumiendo el compromiso de actualizar los datos personales conforme resulte necesario. <a href="//www.dreamgifts.hn">www.dreamgifts.hn</a>, podrá utilizar diversos medios para identificar sus usuarios pero <a href="//www.dreamgifts.hn">www.dreamgifts.hn</a>, no se responsabiliza por la certeza de los datos personales provistos por sus Usuarios. Los Usuarios garantizan y responden en cualquier caso, de la veracidad, exactitud, vigencia y autenticidad de los datos personales ingresados.</p>
					</li>
					<li>
					<p align="JUSTIFY"><a href="//www.dreamgifts.hn">www.dreamgifts.hn</a>, se reserva el derecho de solicitar algún comprobante y o dato adicional a efectos de corroborar los datos personales así como de suspender temporal o definitivamente a aquellos Usuarios cuyos datos no hayan podido ser confirmados, en estos casos de inhabilitación se dará de baja a todas las transacciones realizadas, sin que ello genere algún derecho a resarcimiento.</p>
					</li>
					<li>
					<p align="JUSTIFY">Queda prohibido al usuario de <a href="//www.dreamgifts.hn">www.dreamgifts.hn</a>utilizar esta página para transmitir noticias y/o mensajes cuyo contenido sea contrario a la seguridad del estado, a la concordia internacional, a la paz, al orden público, a la moral, a las buenas costumbres, a cualquier ley aplicable y a la decencia del lenguaje o que perjudiquen los intereses de <a href="//www.dreamgifts.hn">www.dreamgifts.hn</a> o de cualquier otro Usuario o persona, así como utilizarla para cualquier fin prohibido o ilícito.</p>
					</li>
					<li>
					<p align="JUSTIFY">El usuario acepta que deberá verificar en cada operación que realice, la situación (status) de sus transacciones antes de que sean procesadas en la página <a href="//www.dreamgifts.hn">www.dreamgifts.hn</a> en vista de que se tomará como verdadera una vez aceptada por el Usuario.</p>
					</li>
					<li>
					<p align="JUSTIFY">Cualquier controversia o conflicto entre las partes relacionado directa o indirectamente con la información contenida en los Términos y Condiciones Generales y en nuestras Políticas ya sea de naturaleza, interpretación, cumplimiento, ejecución se resolverá mediante el procedimiento de arbitraje de conformidad con el Reglamento del Centro de Conciliación y Arbitraje de la Cámara de Comercio e Industrias de Cortés de la Ciudad de San Pedro Sula, Honduras.</p>
					</li>
					<li>
					<p align="JUSTIFY">Es entendido y convenido que en caso de que alguna de las cláusulas o parte de alguna de ellas sea inválida o contenga vicio de nulidad alguno, ya sea dentro de la legislación Hondureña o en cualquier otra ley que aplique, esta no dará lugar a la nulidad de los Términos y Condiciones Generales así como en nuestras Políticas en todo, manteniéndose en vigor el resto de las disposiciones y cláusulas del mismo.</p>
					</li>
					<li>
					<p align="JUSTIFY">Se entenderán como aceptados los Términos y Condiciones Generales y nuestras Políticas de Privacidad una vez que el Usuario se haya registrado en nuestra página web <a href="//www.dreamgifts.hn">www.dreamgifts.hn</a></p>
					</li>
					</ol>
					<p align="JUSTIFY"><strong>Condiciones de Uso </strong></p>
					<ol>
					<li>
					<p align="JUSTIFY">El uso de los servicios:</p>
					</li>
					</ol>
					<p align="JUSTIFY"><a href="//www.dreamgifts.hn">www.dreamgifts.hn</a> está sujeto a los Términos y Condiciones descritos a continuación. Al hacer uso de estos servicios, el Usuario acepta estos términos y Condiciones. Por favor lea con atención y para cualquier duda o aclaración puede dirigirse a los responsables, consultando la sección de contacto.</p>
					<ol start="2">
					<li>
					<p align="JUSTIFY">Sobre el Sitio y el Acceso:</p>
					</li>
					</ol>
					<p align="JUSTIFY">El acceso al sitio <a href="//www.dreamgifts.hn">www.dreamgifts.hn</a> es restringido y de carácter personal. Por lo tanto no está permitido descargar, modificar, reproducir, duplicar, copiar, vender, revender, visitar o explotar su contenido o parte de él con fines comerciales sin el consentimiento expreso por escrito de La Nueva Blanca.</p>
					<p align="JUSTIFY">El acceso no incluye la reventa o uso comercial de este sitio ni de su contenido, ni de listas de productos o precios. Tampoco permite ningún tipo de descarga o copia de información de facturación para beneficio de un tercero; ni tampoco el uso de minería de datos, robots o herramientas de extracción de datos.&nbsp;</p>
					<p align="JUSTIFY">Está permitido incluir enlaces al sitio <a href="//www.dreamgifts.hn">www.dreamgifts.hn</a> en otras páginas siempre y cuando no estén asociadas a información falsa o de carácter ofensivo. No pueden incluirse en los enlaces imágenes, logotipos, textos, diseños o forma del sitio sin el consentimiento expreso por escrito de La Nueva Blanca.&nbsp;</p>
					<p align="JUSTIFY">Cualquier uso no autorizado del sitio o de su contenido cancela la licencia de acceso al sitio <a href="//www.dreamgifts.hn">www.dreamgifts.hn</a></p>
					<ol start="3">
					<li>
					<p align="JUSTIFY">Sobre la Información de las Cuentas:&nbsp;</p>
					</li>
					</ol>
					<p align="JUSTIFY">Usted es responsable por mantener la confidencialidad de su cuenta y de su contraseña. Acepta tomar responsabilidad por todas las actividades que se realicen bajo el uso de éstas.<br>Nos reservamos el derecho de negar, cancelar, eliminar o editar el contenido de las cuentas, así como de cancelar órdenes, sin que esté obligado a comunicar o exponer las razones de su decisión y sin que ello genere algún derecho a indemnización o resarcimiento.</p>
					<ol start="4">
					<li>
					<p align="JUSTIFY">Comunicación:&nbsp;<br>Los visitantes pueden publicar comentarios, enviar sugerencias, ideas, preguntas o cualquier otra información, mientras que el contenido no sea ilegal, obsceno, amenazante, difamatorio, invasor de la privacidad, que infrinja derechos de Propiedad Intelectual u ofensivos a terceros.&nbsp;</p>
					</li>
					</ol>
					<p align="JUSTIFY">No pueden contener virus, campañas políticas, solicitudes comerciales, cartas cadena, correos masivos, así como ningún tipo de “spam”. Nos reservamos el derecho de eliminar o editar dicho contenido. No está permitido utilizar direcciones de e-mail falsas o implicar a otras personas.&nbsp;</p>
					<p align="JUSTIFY">Al publicar o enviar información a este sitio, otorga por completo a La Nueva Blanca el derecho de uso de dicha información y posible distribución, modificación, adaptación, publicación, traducción, creación de trabajos derivados de dicho contenido, por cualquier medio de comunicación. Otorga también el derecho de utilizar el nombre con el cual este contenido fue enviado. Acepta la responsabilidad de ser autor intelectual de dicha información, de no causar daño a ninguna persona o entidad y por lo tanto acepta indemnizar a La Nueva Blanca. por cualquier reclamo resultante del contenido que proporcione. El sitio tiene el derecho mas no la obligación de monitorear y editar o eliminar cualquier comentario, por consiguiente no asume ninguna responsabilidad por ningún comentario publicado por usted o por un tercero.&nbsp;<br>El sitio&nbsp;<a href="//www.dreamgifts.hn">www.dreamgifts.hn</a>&nbsp;se comunica con los Usuarios vía e-mail o publicando noticias en la página. Cuando usted se comunica electrónicamente con nosotros, accede a recibir comunicación electrónica de nuestra parte.</p>
					<p align="JUSTIFY"><strong>Descripciones de los productos </strong></p>
					<p align="JUSTIFY">Hemos diseñado nuestra página de manera que el Usuario pueda visualizar de forma clara y acertada los productos que ofrecemos (Imagen, Estilo, Talla, Color, Marca y descripción de componentes). Sin embargo, el sitio <a href="//www.dreamgifts.hn">www.dreamgifts.hn</a>&nbsp; no garantiza que los colores que el Usuario observe en su monitor sean los correctos; porque esto dependerá de la resolución del monitor que esté utilizando.</p>
					<p align="JUSTIFY">&nbsp;</p>
					<p align="JUSTIFY"><strong>Precios</strong></p>
					<p><span style="font-family: 'Times New Roman', serif;"><span style="font-size: medium;"><span>Nuestros precios se despliegan en Moneda Dólares de los Estados Unidos de Norteamérica (Americanos) y no incluyen el Impuesto Sobre La Venta (Actualmente 15% sobre el valor de la venta, pudiendo cambiar de conformidad a las leyes aplicables), sin embargo este Impuesto será aplicado a la compra y mostrado al Usuario al momento final de la misma, en una línea que se leerá Impuesto Sobre Ventas ó Tax.</span></span></span></p>
					<p><span style="font-family: 'Times New Roman', serif;"><span style="font-size: medium;">El precio mostrado en nuestra página web <a href="//www.dreamgifts.hn">www.dreamgifts.hn</a>, es verdadero por lo tanto, este será tomado como válido para cualquier reclamo que se presente por parte de nuestros Usuarios. Es nuestra responsabilidad el actualizar los precios y no está autorizado a ningún Usuario la manipulación de datos y en casos debidamente comprobados se eliminará toda transacción pendiente, sin que esto cause perjuicio alguno a La Nueva Blanca. </span></span></p>
					<p align="JUSTIFY"><strong>Envíos y entregas </strong></p>
					<p align="JUSTIFY"><span style="font-family: 'Times New Roman', serif;"><span style="font-size: medium;">Contamos con el servicio de envío a cualquier parte del mundo. El costo de envío varía dependiendo del peso, volumen y ubicación del domicilio.</span></span></p>
					<p align="JUSTIFY"><span style="font-family: 'Times New Roman', serif;"><span style="font-size: medium;">El costo del envío se mostrara al momento de finalizar el proceso de compra.&nbsp;<br><br>Cualquier producto adquirido en <a href="//www.dreamgifts.hn">www.dreamgifts.hn</a>, se entregará a la empresa de paquetería contratada por LA Nueva Blanca<span>., en un periodo máximo de 05 (Cinco) días</span> hábiles después de haber recibido la confirmación del pago de la orden; este tiempo de entrega a la empresa de paquetería dependerá del tipo de servicio de envío escogido por el Usuario. El tiempo de entrega al domicilio especificado por el Usuario dependerá de la dirección de envío provista en la página y del tipo de servicio de paquetería escogido.</span></span></p>
					<p align="JUSTIFY"><span style="font-family: 'Times New Roman', serif;"><span style="font-size: medium;">Los envíos se realizaran a la dirección consignada por el comprador, no se realizaran entregas en lugares públicos tales como parques, estacionamientos, centros de recreación, direcciones no confirmadas, etc.</span></span></p>
					<p align="JUSTIFY"><span style="font-family: 'Times New Roman', serif;"><span style="font-size: medium;">La Nueva Blanca no se hace responsable por cobros de aranceles, ya que cada país tiene regulaciones diferentes. Nuestros tiempos de envíos son garantizados, sin embargo, debes tener en cuenta que algunas veces los procesos de aduana de cada país puede generar retrasos en la entrega de tu orden.</span></span></p>
					<p align="JUSTIFY"><span style="font-family: 'Times New Roman', serif;"><span style="font-size: medium;">En caso de que el paquete sea retenido en aduana, el Usuario correrá con los gastos de desaduanaje y movilización a la aduana, ya que estos inconvenientes no son controlables por La Nueva Blanca. </span></span></p>
					<p align="JUSTIFY"><br><br></p>
					<p align="JUSTIFY"><br><br></p>
					<p align="JUSTIFY"><span style="font-family: 'Times New Roman', serif;"><span style="font-size: medium;"><strong>Forma de Pago </strong></span></span></p>
					<p><span style="font-family: 'Times New Roman', serif;"><span style="font-size: medium;">El tipo de pago aceptado actualmente por&nbsp;<a href="//www.dreamgifts.hn">www.dreamgifts.hn</a>&nbsp;es a través de Tarjetas de Crédito y Débito considerando para dicha transacción las siguientes:</span></span></p>
					<ul>
					<li>
					<p>Visa</p>
					</li>
					<li>
					<p>Master Card</p>
					</li>
					<li>
					<p>American Express</p>
					</li>
					<li>
					<p>Diners Club</p>
					</li>
					<li>
					<p>JCB</p>
					</li>
					<li>
					<p>Discover</p>
					</li>
					</ul>
					<p><span style="font-family: 'Times New Roman', serif;"><span style="font-size: medium;">El sitio guiará a nuestros Usuarios en el proceso de pagos y les indicará los datos necesarios para completar el proceso. El uso de las Tarjetas de Crédito y Débito es responsabilidad del Usuario por lo cual La Nueva Blanca. no se hace responsable por cualquier irregularidad que se presente en el manejo de la misma.</span></span></p>
					<p><span style="font-family: 'Times New Roman', serif;"><span style="font-size: medium;">Condiciones especiales </span></span></p>
					<p><span style="font-family: 'Times New Roman', serif;"><span style="font-size: medium;">El sitio <a href="//www.dreamgifts.hn">www.dreamgifts.hn</a>&nbsp;evoluciona constantemente por lo que estos Términos y Condiciones Generales de Uso pueden cambiar sin previo aviso. Es responsabilidad de los Usuarios consultar periódicamente cualquier cambio.</span></span></p>
				</div>
			</div>
		</div>
	</body>
</html>