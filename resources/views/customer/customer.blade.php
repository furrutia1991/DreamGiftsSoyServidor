@extends('layouts.auth')

@section('content')
	<div class="row">
		<div class="row header">
			<div class="col s12">
				<a href="{{route ('customers')}}" class="waves-effect waves-light btn-large"><i class="material-icons right">settings_backup_restore</i>Atras</a>
			</div>
		</div>
		@if ($customer->first())
		<div class="tittle">
			<h3 style="text-align: center">Boda: {{ $customer->name}}</h3>
		</div>
			<div class="row">
				<div class="col s12 l8 offset-l2">
					<form id="myform" action="{{ route('updateCustomer', $customer->customer_id) }}" method="POST">
						{!! csrf_field() !!}

						<div class="row">
							<div class="col s12">
								<label>
									Nombre Boda:
									<input type="text" name="name" value="{{$customer->name}}" required>
								</label>
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<label>
									Correo eléctronico:
									<input type="text" name="email" value="{{$customer->email}}" required>
								</label>
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<label>
									Número de identidad:
									<input type="text" name="dni_number" onkeypress="return /\d/.test(String.fromCharCode(((event||window.event).which||(event||window.event).which)));" maxlength="15" value="{{$customer->dni_number}}" required>
								</label>
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<label>
									Dirección:
									<input type="text" name="address" cols="10" rows="2" value="{{$customer->address}}" required></input>
								</label>
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<label>
								Número telefonico:
									<input type="text" onkeypress="return /\d/.test(String.fromCharCode(((event||window.event).which||(event||window.event).which)));" maxlength="8" name="phone_number" value="{{$customer->phone_number}}" required>
								</label>
							</div>
						</div>
						<div>
							@if($package)
							<a href="{{route('package', $package->package_id) }}" class="left waves-effect waves-light btn-large"><i class="material-icons right">zoom_in</i>Ver Paquete</a>
							@endif
							@if(!$package)
							<a href="{{route('newPackages', $customer->customer_id)}}" class="left waves-effect waves-light btn-large"><i class="material-icons right">note_add</i>Crear Paquete</a>
							@endif
							<button type="submit" class="right btn-large waves-effect waves-light" name="action">Guardar<i class="material-icons right">send</i>
							</button>
						</div>
					</form>
				</div>
			</div>

		@endif

	</div>
@stop
