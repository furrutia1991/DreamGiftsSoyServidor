@extends('layouts.auth')	

@section('css')
	@parent
	<link rel="stylesheet" href="{{ asset('css/responsive-tables.css') }}">
	<link rel="stylesheet" href="{{ asset('css/jquery.dynatable.css') }}">
@stop


@section('content')
	

	<div class="row header">
		<div class="col  s12">
			 <a href="{{route ('home')}}" class="left waves-effect waves-light btn-large"><i class="material-icons right">settings_backup_restore</i>Atras</a>
			 <a class="right modal-trigger waves-effect waves-light btn-large" href="#modal1"><i class="material-icons right">playlist_add</i>Nuevo Cliente</a>
		</div>
	</div>
	<div class="tittle">
		<h2 style="text-align: center">Bodas</h2>
	</div>
	<br>
	<div class="row">
		<div class="col l8 offset-l2 s10 offset-s1">
			<label>Nombre/Número de identidad de cliente</label>
			<input type="text" class="search-customers" placeholder="Buscar cliete por identidad o nombre..." autofocus>
		</div>
		<div >
			<a href="javascript:location.reload()" class="refresh-btn left btn waves-effect waves-light">Ver todos<i class="material-icons right">perm_identity</i>
		  	</a>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col l10 offset-l1 s12">
			<table class="responsive-table">
				<thead>
					<th width="100" height="40">Nombre Boda</th>
					<th width="100" height="40">Correo Electronico</th>
					<th width="100" height="40">Número de telefono</th>
					<th width="100" height="40">Dirección</th>
					<th width="100" height="40">Número de identidad</th>
					<th class="Jdelete" width="100" height="40">Eliminar</th>
				</thead>
				<tbody class="customers-body">
					@foreach ($customers as $element)
						<tr>
							<td><a href="{{ route('customer', $element->customer_id) }}">{{$element->name}}</a></td>
							<td>{{$element->email}}</td>
							<td>{{$element->phone_number}}</td>
							<td>{{$element->address}}</td>
							<td>{{$element->dni_number}}</td>
							<td><a href="#modal-delete-wedding_{{$element->customer_id}}" class="modal-trigger waves-effect waves-light btn-large"><i class="delete material-icons right">delete</i>Eliminar</a></td>
						</tr>
						{{-- Delete Modal --}}
								<div id="modal-delete-wedding_{{$element->customer_id}}" class="modal">
							        <div class="modal-content">
							            <div class="row">
							                <div class="col l8 offset-l2">
							                    <h4 style="text-align: center;">Elminar Boda</h4>
							                    <p style="text-align: center;">
							                    	¿Esta seguro que desea eliminar la boda {{$element->name}}?
							                    	
							                    </p>
							                </div>
							            </div>
							            <br>
							            <div class="row">
							                <div class="col l10 offset-l2">
							                    <a   href="{{ route('deleteCustomer', $element->customer_id) }}" class="col l4 btn waves-effect" name="action">
							                        Confirmar
							                        <i class="material-icons right"><i class="mdi-toggle-check-box"></i></i>
							                    </a>
							                    <button class="modal-close col l4 offset-l1 btn waves-effect" type="submit" id="decline-delete-component"
							                            name="action">
							                        No
							                        <i class="material-icons right"><i class="mdi-action-highlight-remove"></i></i>
							                    </button>
							                </div>
							            </div>

							        </div>
							        <div class="modal-footer">
							            <a class="modal-action modal-close waves-effect waves-green btn-flat tooltipped" id="out-modal"
							               data-position="top" data-delay="50" data-tooltip="Salir de esta pantalla">Salir</a>
							        </div>
							    </div>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<div class="row paginate">
		<div class="col s12 l4 offset-l4">

			{!! $customers->render() !!}
		</div>
	</div>	

	{{-- Customer Modal --}}
	<div id="modal1" class="modal" style="padding: 3em">
	    	<h4 style="text-align: center">Nuevo Cliente</h4>
	    <div class="modal-content">
				<form action="{{ route('newCustomer')}}" method="POST" enctype="multipart/form-data">
					{!! csrf_field() !!}

					<div class="row">
						<div class="col s12 l6">
							<label>
								Nombre completo:
							</label>
								<input type="text" name="name" required>
						</div>
						<div class="col s12 l6">
							<label>
								Correo eléctronico:
							</label>
								<input type="text" name="email" required>
						</div>
					</div>
					<div class="row">
						<div class="col s12 l6">
							<label>
								Número de identidad:
							</label>
								<input type="text" name="dni_number" onkeypress="return /\d/.test(String.fromCharCode(((event||window.event).which||(event||window.event).which)));" maxlength="13" required>
						</div>
						<div class="col s12 l6">
							<label>
								Dirección:
							</label>
								<input type="text" name="address" cols="10" rows="2"></input>
						</div>
					</div>
					<div class="row">
						<div class="col s12 l6">
							<label>
							Número teléfonico:
							</label>
								<input type="text" onkeypress="return /\d/.test(String.fromCharCode(((event||window.event).which||(event||window.event).which)));" maxlength="8" name="phone_number" >
						</div>
					</div>
					<button class="left btn waves-effect waves-light modal-action modal-close waves-green">Cerrar<i class="material-icons right">cancel</i>
					</button>
					<button class="right btn waves-effect waves-light" type="submit" name="action">Guardar<i class="material-icons right">send</i>
		  			</button>
				</form>	    		
	    </div>
	</div>
@stop
@section('script')
	@parent
	<script>
		jQuery(document).ready(function($) {
		$('.search-customers').keyup(function(event) {
		    if(event.keyCode == 13){
		        var text = $(this).val() ? $(this).val() : 'blank';

		        $.get('clientes/buscar/' + text, function(data) {

		            if (data) {
		                
		                if(data.redirect)
		                {
		                    window.location.href = data.redirect;
		                }
		                $('.Jdelete').css("display", "none");
						$('.paginate').css("display", "none");
		                $('.customers-body').html('');
		                $('.refresh-btn').css("display", "block");
		                
		                $.each(data, function(index, val) {

		                    var markup = "<tr>" +
		                        "<td><a href='cliente/" + val.customer_id + "'>" + val.name + "</a></td>" +
		                        "<td>" + val.email + "</td>" +
		                        "<td>" + val.phone_number + "</td>" +
		                        "<td>" + val.address + "</td>" +
		                        "<td>" + val.dni_number + "</td>" +
		                    "</tr>";
		                    $('.customers-body').append(markup);
		                })
		            }

		        })
		    }
			    
			});
		});
		
	</script>
@stop
