@extends('layouts.auth')

@section('css')
	@parent
	<link rel="stylesheet" href="{{ asset('css/responsive-tables.css') }}">
	<link rel="stylesheet" href="{{ asset('css/jquery.dynatable.css') }}">
@stop

@section('content')
<div class="row header">
	<div class="col  s12">
		 <a href="{{route('packages')}}" class="left waves-effect waves-light btn-large"><i class="material-icons right">settings_backup_restore</i>Atras</a>
	</div>
</div>
<div class="tittle">
	<h4 style="text-align: center">Nuevo Cliente</h4>
</div>
<div class="row">
	<div class="col s12 l8 offset-l2">
			   
		<form action="{{ route('newCustomerToNewPackage')}}" method="POST" enctype="multipart/form-data">
			{!! csrf_field() !!}

			<div class="row">
				<div class="col s12 l6">
					<label>
						Nombre completo:
					</label>
						<input type="text" name="name" required>
				</div>
				<div class="col s12 l6">
					<label>
						Correo eléctronico:
					</label>
						<input type="text" name="email" required>
				</div>
			</div>
			<div class="row">
				<div class="col s12 l6">
					<label>
						Número de identidad:
					</label>
						<input type="text" name="dni_number" onkeypress="return /\d/.test(String.fromCharCode(((event||window.event).which||(event||window.event).which)));" maxlength="13" required>
				</div>
				<div class="col s12 l6">
					<label>
						Dirección:
					</label>
						<input type="text" name="address" cols="10" rows="2"></input>
				</div>
			</div>
			<div class="row">
				<div class="col s12 l6">
					<label>
					Número teléfonico:
					</label>
						<input type="text" onkeypress="return /\d/.test(String.fromCharCode(((event||window.event).which||(event||window.event).which)));" maxlength="8" name="phone_number" >
				</div>
			</div>
			<button class="right btn-large waves-effect waves-light" type="submit" name="action">Guardar<i class="material-icons right">send</i>
			</button>
		</form>	    
	</div>
</div>
	
@stop