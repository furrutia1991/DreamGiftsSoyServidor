@extends('layouts.auth')

@section('title', 'Ingresar al sistema')

@section('content')


	<div class="row">
    	<form class="register_form col s12 l6 offset-l3" action="register" method="POST">
    		{!! csrf_field() !!}
		      <div class="row">
		        <div class="input-field col s12">
		          <input placeholder="Nombre completo" id="first_name" type="text" name="name" class="validate" required>
		          <label for="first_name">Nombre Completo: </label>
		        </div>
		      </div>
		      <div class="row">
		        <div class="input-field col s12">
		          <input id="email" type="email" name="email" placeholder='yo@ejemplo.com' class="validate" required>
		          <label for="email">Email</label>
		        </div>
		        @if (count($errors) > 0 && $errors->first('email'))
						<div class="col s12">
							<span class="error">{{ $errors->first('email') }}</span>
						</div>
				@endif
		      </div>
		      <div class="row">
		        <div class="input-field col s12">
		          <input id="password" type="password" name="password" placeholder='contraseña' class="validate" required>
		          <label for="password">Contraseña: </label>
		        </div>
		      </div>
		      
		      <div class="row">
		        <div class="input-field col s12">
		          <input id="password" type="password" name="password_confirmation" placeholder='repita su contraseña' class="validate" required>
		          <label for="password">Repetir contraseña: </label>
		        </div>
		        @if (count($errors) > 0 && $errors->first('password'))
						<div class="col s12 error">
							<span class="error">{{ $errors->first('password') }}</span>
						</div>
				@endif
		      </div>

		      <div class="row">
                    <div class="col m12">
                        <p class="right-align">
                            <button class="btn btn-large waves-effect waves-light" type="submit" name="action">Ingresar
							 <i class="material-icons right">send</i>
                            </button>
                        </p>
                    </div>
            	</div>

		      
    	</form>
  </div>



	<!-- <div class="row valign-wrapper register">

		<div class="small-12 medium-8 medium-offset-2 columns panel">

			<form action="register" method="POST">
				{!! csrf_field() !!}

				<div class="row">
					<div class="small-12 columns">
						<label for=""> Nombre Completo: 
							<input type="text" name="name" placeholder='nombre completo' required>
						</label>
					</div>
				</div>
				<div class="row">
					<div class="small-12 columns">
						<label for=""> Email: 
							<input type="email" name="email" placeholder='me@example.com' required>
						</label>
					</div>
					@if (count($errors) > 0 && $errors->first('email'))
						<div class="small-12 columns">
							<span class="error">{{ $errors->first('email') }}</span>
						</div>
					@endif
				</div>
				<div class="row">
					<div class="small-12 columns">
						<label for=""> Contraseña: 
							<input type="password" name="password" placeholder='contraseña' required>
						</label>
					</div>
				</div>
				<div class="row">
					<div class="small-12 columns">
						<label for=""> Repetir contraseña: 
							<input type="password" name="password_confirmation" placeholder='contraseña' required>
						</label>
					</div>
					@if (count($errors) > 0 && $errors->first('password'))
						<div class="small-12 columns error">
							<span class="error">{{ $errors->first('password') }}</span>
						</div>
					@endif
				</div>
				<div class="row">
					<div class="small-12 columns">
						<div class="clearfix">
							<button type="submit" class="button right">Ingresar</button>
						</div>	
					</div>
				</div>
			</form>
		</div>
	</div> -->
@stop