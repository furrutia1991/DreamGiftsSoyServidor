@extends('layouts.auth')

@section('title', 'Ingresar al sistema')

@section('content')
	<div class="row">
    	<form class="register_form col s12 m8 offset-m2 l6 offset-l3 " action="login" method="POST">
    		{!! csrf_field() !!}
    		<h2 class="center-align">Registro</h2>
		      <div class="row">
		        <div class="input-field col s12">
		        	<i class="material-icons prefix">account_circle</i>
			        <input id="icon_prefix email" type="email" class="validate" name="email" placeholder='yo@ejemplo.com' required>
			        <label for="email">Email: </label>
		        </div>
		      </div>

		      <div class="row">
		        <div class="input-field col s12">
					<i class="material-icons prefix">vpn_key</i>
			        <input id="password" type="password" class="validate" name="password" placeholder='contraseña' required>
			        <label for="password">Contraseña: </label>
		        </div>
		      </div>

 				<div class="divider"></div>
                <div class="row">
                    <div class="col m12">
                        <p class="right-align">

                            <button class="btn btn-large waves-effect waves-light" type="submit" name="action">Ingresar
							 <i class="material-icons right">send</i>
                            </button>
                        </p>
                    </div>
            	</div>

            	<a href="register">Solicitar cuenta al administrador</a>

				
		      	@if ($errors->any())
					<span class="error">{{$errors->first()}}</span>
				@endif
    	</form>
  </div>
@stop